/*
 * Tri-D Chess applimport gui.chess.FieldView;
 * 
 * import java.awt.image.BufferedImage; import java.util.ArrayList;
 * 
 * import util.ArrayUtils; This program is free software; you can redistribute
 * it and/or modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.model;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import de.rowlo.tridchess.gui.FieldView;
import de.rowlo.tridchess.util.ArrayUtils;

/**
 * Figure description.
 * 
 * @author Robert Wloch
 * 
 */
public class Figure {
   /** Type of the Figure. */
   public static final int             PAWN               = 1;
   /** Type of the Figure. */
   public static final int             CASTLE             = 2;
   /** Type of the Figure. */
   public static final int             KNIGHT             = 3;
   /** Type of the Figure. */
   public static final int             BISHOP             = 4;
   /** Type of the Figure. */
   public static final int             QUEEN              = 5;
   /** Type of the Figure. */
   public static final int             KING               = 6;

   /** The Field the Figure stands on. */
   protected Field                     field;
   /** The Board the Figure belongs to. */
   protected Board                     board;
   /** The name of the Figure. */
   protected String                    name;
   /** The type of the Figure. */
   protected int                       type;
   /** Flag indicating if the Figure has not moved yet. */
   protected boolean                   untouched;
   /** Flag indicating the party the Figure belongs to. */
   protected boolean                   isWhite;
   /** Flag indication if the figure is still in the game. */
   protected boolean                   inGame;
   /** Flag used for kings indicating if they are check mate. */
   protected boolean                   mate;
   /** The BufferedImage of the Figure. */
   private BufferedImage               image;

   private ArrayList<Field>            moveList           = new ArrayList<Field>();
   private ArrayList<Field>            ghostMoveList      = new ArrayList<Field>();
   private ArrayList<Field>            attackList         = new ArrayList<Field>();
   private ArrayList<Field>            attackersList      = new ArrayList<Field>();
   private ArrayList<Field>            blockList          = new ArrayList<Field>();
   private ArrayList<Field>            coverList          = new ArrayList<Field>();
   // path lists
   // direct check
   private ArrayList<ArrayList<Field>> checkPaths         = new ArrayList<ArrayList<Field>>();
   // could set check if one own figure wasn't in the way
   private ArrayList<ArrayList<Field>> checkThreadPaths   = new ArrayList<ArrayList<Field>>();
   // will set check, if one opposing chained figure wasn't in the way
   private ArrayList<ArrayList<Field>> chainedThreadPaths = new ArrayList<ArrayList<Field>>();

   private boolean                     init;

   /**
    * @param type
    *           figure type
    * @param n
    *           String (image file name); not null
    * @param f
    *           Field; if null figure is not in game
    * @param b
    *           Board; not null
    */
   public Figure( boolean white, int type, String n, Field f, Board b ) {
      this.init = true;
      this.isWhite = white;
      this.type = type;
      setName( n );
      setField( f );
      setBoard( b );
      this.untouched = ( f != null ) ? true : false;
      this.inGame = ( f != null ) ? true : false;
      this.init = false;
      this.mate = false;
   }

   /**
    * @return type
    */
   public int getType() {
      return type;
   }

   /**
    * @return Board
    */
   public Board getBoard() {
      return board;
   }

   /**
    * @param board
    *           Board
    */
   public void setBoard( Board board ) {
      this.board = board;
   }

   /**
    * @return Name
    */
   public String getName() {
      return name;
   }

   /**
    * @param name
    *           String
    */
   public void setName( String name ) {
      this.name = name;
   }

   /**
    * @return field
    */
   public Field getField() {
      return field;
   }

   /**
    * Sets field where the figure stands and updates old and new fields' figure
    * attribute.<br>
    * Calls back board's moveLog method to report the move.
    * 
    * @param field
    *           Field
    */
   public void setField( Field field ) {
      if ( this.field != null ) {
         this.field.setFigure( null );
      }
      Field oldField = this.field;
      this.field = field;
      untouched = false;
      if ( field != null ) {
         field.setFigure( oldField, this, !init );
      }
   }

   /**
    * @return the image
    */
   public BufferedImage getImage() {
      return image;
   }

   /**
    * @param image
    *           BufferedImage
    */
   public void setImage( BufferedImage image ) {
      this.image = image;
   }

   /**
    * @return true, if figure was not moved so far
    */
   public boolean isUntouched() {
      return untouched;
   }

   /**
    * @return inGame
    */
   public boolean isInGame() {
      return inGame && ( getField() != null );
   }

   /**
    * @param inGame
    *           boolean
    */
   public void setInGame( boolean inGame ) {
      this.inGame = inGame;
      setField( null );
   }

   @Override
   public String toString() {
      StringBuffer sb = new StringBuffer();

      sb = sb.append( "Figure " );
      String pos = "out of game";
      if ( getField() != null ) {
         pos = getField().getName();
      }
      sb = sb.append( pos ).append( " is a" );
      if ( board.getWhiteFiguresList().contains( this ) ) {
         sb = sb.append( " white" );
      }
      else {
         sb = sb.append( " black" );
      }
      switch ( getType() ) {
         case Figure.PAWN:
            sb = sb.append( " pawn" );
            break;
         case Figure.CASTLE:
            sb = sb.append( " castle" );
            break;
         case Figure.KNIGHT:
            sb = sb.append( " knight" );
            break;
         case Figure.BISHOP:
            sb = sb.append( " bishop" );
            break;
         case Figure.QUEEN:
            sb = sb.append( " queen" );
            break;
         case Figure.KING:
            sb = sb.append( " king" );
            break;
         default:
            sb = sb.append( " UNKNOWN" );
            break;
      }

      return sb.toString();
   }

   /**
    * @return isWhite
    */
   public boolean isWhite() {
      return isWhite;
   }

   /**
    * @param isWhite
    *           boolean
    */
   public void setWhite( boolean isWhite ) {
      this.isWhite = isWhite;
   }

   /**
    * @return the name of the Figure.
    */
   public String getFigureName() {
      StringBuffer sb = new StringBuffer();
      sb = sb.append( ( isWhite ) ? "white" : "black" );
      switch ( type ) {
         case PAWN:
            sb = sb.append( " pawn" );
            break;
         case CASTLE:
            sb = sb.append( " castle" );
            break;
         case KNIGHT:
            sb = sb.append( " knight" );
            break;
         case BISHOP:
            sb = sb.append( " bishop" );
            break;
         case QUEEN:
            sb = sb.append( " queen" );
            break;
         case KING:
            sb = sb.append( " king" );
            break;
         default:
            sb = sb.append( " unknown" );
      }
      if ( isCheck() ) {
         sb = sb.append( " in check" );
      }
      if ( isMate() ) {
         sb = sb.append( " mate" );
      }
      return sb.toString();
   }

   /**
    * Applies only to kings.
    * 
    * @return true if king is check
    */
   public boolean isCheck() {
      if ( type == KING && getAttackersList().size() > 0 ) return true;
      return false;
   }

   /**
    * Keep only fields in move and attack list, that are in the counter check
    * fields list.<br>
    * Kings keep their move fields without the ones also present in the covered
    * fields.
    * 
    * @param counterCheckFields
    * @param coveredFields
    */
   @SuppressWarnings("unchecked")
   public void applyCheckCounterMeasures( ArrayList<Field> counterCheckFields,
         ArrayList<Field> coveredFields ) {
      if ( type == KING ) {
         setMoveList( ArrayUtils.joinExclude( getMoveList(), coveredFields ) );
      }
      else {
         ArrayList<Field> allMoves = ArrayUtils.copy( getMoveList() );
         getMoveList().clear();
         int count = allMoves.size();
         for ( int i = 0; i < count; i++ ) {
            Field f = allMoves.get( i );
            if ( counterCheckFields.contains( f ) ) {
               getMoveList().add( f );
            }
         }
      }
      if ( counterCheckFields != null ) {
         ArrayList<Field> allAttacks = ArrayUtils.copy( getAttackList() );
         getAttackList().clear();
         int count = allAttacks.size();
         for ( int i = 0; i < count; i++ ) {
            Field f = allAttacks.get( i );
            if ( counterCheckFields.contains( f ) ) {
               getAttackList().add( f );
            }
         }
      }
   }

   /**
    * Applies only to kings.
    * 
    * @return true, if king is check mate
    */
   public boolean isMate() {
      if ( type == KING ) { return mate; }
      return false;
   }

   /**
    * @param mate
    *           boolean
    */
   public void setMate( boolean mate ) {
      if ( type == KING ) {
         this.mate = mate;
      }
      else {
         this.mate = false;
      }
   }

   /**
    * @return the attackList
    */
   public ArrayList<Field> getAttackList() {
      return attackList;
   }

   /**
    * @return the blockList
    */
   public ArrayList<Field> getBlockList() {
      return blockList;
   }

   /**
    * @return the coverList
    */
   public ArrayList<Field> getCoverList() {
      return coverList;
   }

   /**
    * @return the moveList
    */
   public ArrayList<Field> getMoveList() {
      return moveList;
   }

   /**
    * @param list
    *           the new moveList; not null
    */
   public void setMoveList( ArrayList<Field> list ) {
      if ( list != null ) {
         moveList = list;
      }
   }

   /**
    * This list is a collection of fields this figure could reach, if an
    * opposing figure wasn't in its way.
    * 
    * @return the ghostMoveList
    */
   public ArrayList<Field> getGhostMoveList() {
      return ghostMoveList;
   }

   /**
    * @param list
    *           the new ghostMoveList; not null
    */
   public void setGhostMoveList( ArrayList<Field> list ) {
      if ( list != null ) {
         ghostMoveList = list;
      }
   }

   /**
    * @return the attackersList
    */
   public ArrayList<Field> getAttackersList() {
      return attackersList;
   }

   /**
    * @return the chainedThreadPaths
    */
   public ArrayList<ArrayList<Field>> getChainedThreadPaths() {
      return chainedThreadPaths;
   }

   /**
    * @return the checkPaths
    */
   public ArrayList<ArrayList<Field>> getCheckPaths() {
      return checkPaths;
   }

   /**
    * @return the checkThreadPaths
    */
   public ArrayList<ArrayList<Field>> getCheckThreadPaths() {
      return checkThreadPaths;
   }

   /**
    * Clears all information about attackers and Figures being attacked or
    * covered as well as information about possible check threads from or to the
    * Figure.
    */
   public void clearStateLists() {
      getMoveList().clear();
      getGhostMoveList().clear();
      getAttackList().clear();
      getBlockList().clear();
      getCoverList().clear();
      getAttackersList().clear();

      getCheckPaths().clear();
      getCheckThreadPaths().clear();
      getChainedThreadPaths().clear();

      setMate( false );
   }

   /**
    * Highlights the fields concerning the next possible move of this figure.
    */
   public void highlightFields() {
      int count = getMoveList().size();
      for ( int i = 0; i < count; i++ ) {
         getMoveList().get( i ).getView().setShade( FieldView.ALLOWED_SHADE );
      }
      count = getBlockList().size();
      for ( int i = 0; i < count; i++ ) {
         getBlockList().get( i ).getView().setShade( FieldView.FORBIDDEN_SHADE );
      }
      count = getCoverList().size();
      for ( int i = 0; i < count; i++ ) {
         getCoverList().get( i ).getView().setShade( FieldView.GUARDED_SHADE );
      }
      count = getAttackersList().size();
      for ( int i = 0; i < count; i++ ) {
         getAttackersList().get( i ).getView().setShade(
               FieldView.ATTACKER_SHADE );
      }
      // attack last to have it override attacker
      count = getAttackList().size();
      for ( int i = 0; i < count; i++ ) {
         getAttackList().get( i ).getView().setShade( FieldView.ATTACK_SHADE );
      }
   }

}
