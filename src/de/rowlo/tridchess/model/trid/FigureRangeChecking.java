/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.model.trid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import de.rowlo.tridchess.model.Field;
import de.rowlo.tridchess.model.Figure;
import de.rowlo.tridchess.util.ArrayUtils;
import de.rowlo.tridchess.util.BoardUtils;

/**
 * Checks the movement range for all figures and provides counter check
 * checking.
 * 
 * @author Robert Wloch
 * 
 */
public class FigureRangeChecking {
   // debug levels
   private static final int   DL_TRACE_STOP   = 1;
   private static final int   DL_METHOD_ENTER = 2;
   private static final int   DL_RECURSE      = 4;
   private static final int   DL_IGNORE_Z     = 8;

   /** Indicates checking for plain moves. */
   protected static final int MOVE_RANGE      = 0;
   /**
    * Indicates checking for possible check thread after move has to stop due to
    * an own figure. Checking assumes the own figure wasn't there and if the
    * investigated figure could put enemy to check if it had free way.
    */
   protected static final int CHECK_THREAD    = 1;
   /**
    * Indicates checking for possible check thread after move has to stop due to
    * an enemy's figure. Checking assumes the opposing figure wasn't there and
    * if the investigated figure could put enemy to check if it had free way.
    */
   protected static final int CHAIN_THREAD    = 2;

   // debug level bit field
   private int                DL              = 0;

   /** Value for X_AXIS. */
   public static final int    X_AXIS          = 0;
   /** Value for Y_AXIS. */
   public static final int    Y_AXIS          = 1;

   private TriDBoard          board;

   /**
    * Construction with TriDBoard.
    * 
    * @param board
    */
   public FigureRangeChecking( TriDBoard board ) {
      this.board = board;
      // this.DL = DL_TRACE_STOP | DL_METHOD_ENTER | DL_RECURSE | DL_IGNORE_Z;
   }

   /**
    * For all figures finds the fields they can move onto, figures they can
    * attack, and figures they cover. Also collects fields where regular
    * movement is blocked by other figures.<br>
    * Marks kings being check or mate.
    */
   public void refreshFiguresStates() {
      // clear all figures' states
      ArrayList<Figure> figs = board.getWhiteFiguresList();
      int count = figs.size();
      for ( int i = 0; i < count; i++ ) {
         figs.get( i ).clearStateLists();
      }
      figs = board.getBlackFiguresList();
      count = figs.size();
      for ( int i = 0; i < count; i++ ) {
         figs.get( i ).clearStateLists();
      }

      // calculate range for all figures
      figs = board.getWhiteFiguresList();
      count = figs.size();
      for ( int i = 0; i < count; i++ ) {
         checkRangeForFigure( figs.get( i ) );
      }
      figs = board.getBlackFiguresList();
      count = figs.size();
      for ( int i = 0; i < count; i++ ) {
         Figure figure = figs.get( i );
         checkRangeForFigure( figure );
      }

      // check if a king is check
      counterCheckCalculations( board.getWhiteKing(), board
            .getWhiteFiguresList(), board.getBlackFiguresList() );
      counterCheckCalculations( board.getBlackKing(), board
            .getBlackFiguresList(), board.getWhiteFiguresList() );
   }

   /**
    * Returns the Figure that is in the path preventing a direct attack.
    * 
    * @param path
    * @return Figure
    */
   private Figure getThreadCause( ArrayList<Field> path ) {
      int count = path.size();
      for ( int i = 1; i < count - 1; i++ ) {
         Figure fig = path.get( i ).getFigure();
         if ( fig != null ) { return fig; }
      }
      System.err
            .println( "ERROR: checking path for thread cause, but no figure found:\n"
                  + path );
      return null;
   }

   /**
    * Perform move range calculations for party put in check to remove check
    * status. This caluclations must recognize check mate.
    * 
    * @param king
    *           attacked king Figure
    * @param attacked
    *           all figures in play of party in check
    * @param attackers
    *           all figures in play of attacking party
    */
   @SuppressWarnings("unchecked")
   protected void counterCheckCalculations( Figure king,
         ArrayList<Figure> attacked, ArrayList<Figure> attackers ) {
      // list of fields attacked party can move to to remove check
      ArrayList<Field> counterCheckFields = new ArrayList<Field>();
      // list of fields attacker can move to
      ArrayList<Field> coveredFields = new ArrayList<Field>();
      // common path of all attackers of the king
      ArrayList<Field> commonCheckPath = null;
      // attackers' figures that just don't give check, because another own
      // figure is in the way
      HashMap<Figure, ArrayList<Field>> threads = new HashMap<Figure, ArrayList<Field>>();
      // attacked party's figures that can't move because that would put their
      // king check
      HashMap<Figure, ArrayList<Field>> chaineds = new HashMap<Figure, ArrayList<Field>>();

      // find threads
      int count = attackers.size();
      for ( int i = 0; i < count; i++ ) {
         Figure f = attackers.get( i );
         ArrayList<ArrayList<Field>> paths = f.getCheckThreadPaths();
         int max = paths.size();
         for ( int j = 0; j < max; j++ ) {
            ArrayList<Field> path = paths.get( j );
            threads.put( getThreadCause( path ), path );
         }
         paths = f.getChainedThreadPaths();
         max = paths.size();
         for ( int j = 0; j < max; j++ ) {
            ArrayList<Field> path = paths.get( j );
            chaineds.put( getThreadCause( path ), path );
         }
         // keep track of all covered fields
         coveredFields = ArrayUtils.joinUnique( coveredFields, f.getMoveList() );
      }
      // restrict movement of king onto fields reachable by opposing party
      king.applyCheckCounterMeasures( null, coveredFields );

      if ( king.isCheck() ) {
         ArrayList<Field> kingAttackers = ArrayUtils.removeDuplicates( king
               .getAttackersList() );
         count = kingAttackers.size();
         for ( int i = 0; i < count; i++ ) {
            Figure f = kingAttackers.get( i ).getFigure();
            // common check path
            ArrayList<ArrayList<Field>> checkPaths = f.getCheckPaths();
            int paths = checkPaths.size();
            for ( int j = 0; j < paths; j++ ) {
               ArrayList<Field> path = checkPaths.get( j );
               if ( commonCheckPath == null ) {
                  commonCheckPath = ArrayUtils.copy( path );
               }
               else {
                  commonCheckPath = ArrayUtils
                        .joinMatch( commonCheckPath, path );
               }
            }
         }

         // 3 ways to put check: 1 figure with 1 path, 1 figure with n paths, n
         // figures with n paths
         count = kingAttackers.size();
         // only one attacker: check if it can be counter attacked
         if ( count == 1 ) {
            Figure f = kingAttackers.get( 0 ).getFigure();
            // check if the only king attacker is covered by another one of
            // attacker's figures
            boolean attackerIsCovered = false;
            for ( Figure protecter : attackers ) {
               if ( protecter.getCoverList().contains( f.getField() ) ) {
                  attackerIsCovered = true;
               }
            }
            // can attacker be counter attacked?
            if ( f.getAttackersList().size() > 0 ) {
               if ( f.getAttackersList().size() == 1
                     && f.getAttackersList().contains( king.getField() )
                     && attackerIsCovered ) {
                  if ( !coveredFields.contains( f.getField() ) ) {
                     coveredFields.add( f.getField() );
                  }
                  coveredFields = ArrayUtils.joinUnique( coveredFields, f
                        .getGhostMoveList() );
                  // restrict movement of king onto fields reachable by opposing
                  // party
                  king.applyCheckCounterMeasures( null, coveredFields );
               }
               else {
                  counterCheckFields.add( f.getField() );
               }
            }
         }
         // in all other case check if common path fields can be moved onto
         count = attacked.size();
         for ( int i = 0; i < count; i++ ) {
            Figure fig = attacked.get( i );
            ArrayList<Field> fields = fig.getMoveList();
            int max = fields.size();
            for ( int j = 0; j < max; j++ ) {
               Field f = fields.get( j );
               if ( commonCheckPath.contains( f )
                     && fig.getType() != Figure.KING ) {
                  counterCheckFields.add( f );
               }
            }
         }
      } // if: king.isCheck()

      // apply free move restrictions
      if ( king.isCheck() ) {
         count = attacked.size();
         for ( int i = 0; i < count; i++ ) {
            attacked.get( i ).applyCheckCounterMeasures( counterCheckFields,
                  coveredFields );
         }
         // is check removable or king movable?
         if ( counterCheckFields.size() < 1 && king.getMoveList().size() < 1 ) {
            king.setMate( true );
         }
      }
      else {
         // king is not in check, restrict movement of chained figures
         Iterator<Figure> it = chaineds.keySet().iterator();
         while ( it.hasNext() ) {
            Figure fig = it.next();
            ArrayList<Field> path = chaineds.get( fig );
            fig.applyCheckCounterMeasures( path, coveredFields );
         }
      }

   }

   /**
    * For a figure finds the fields it can move onto, figures it can attack, and
    * figures it covers. Also collects fields where regular movement is blocked
    * by other figures.<br>
    * Marks kings being check or mate.
    */
   protected void checkRangeForFigure( Figure fig ) {
      if ( fig == null ) return;
      Field f = fig.getField();
      if ( f == null ) {
         System.err.println( "ERROR: Figure's field is null: " + fig );
         return;
      }

      switch ( fig.getType() ) {
         case Figure.PAWN:
            checkPawnRange( fig, f );
            break;
         case Figure.CASTLE:
            checkCastleRange( fig, f );
            break;
         case Figure.KNIGHT:
            checkKnightRange( fig, f );
            break;
         case Figure.BISHOP:
            checkBishopRange( fig, f );
            break;
         case Figure.QUEEN:
            checkKingQueenRange( fig, f, 10, 6 );
            break;
         case Figure.KING:
            checkKingQueenRange( fig, f, 1, 1 );
            break;
         default:
            break;
      }
   }

   /**
    * Creates a new path list and prepends the contents of old, if old is not
    * null. The last element is always f.
    * 
    * @param old
    *           may be null
    * @param f
    *           not null
    * @return new list with added field
    */
   protected ArrayList<Field> pathList( ArrayList<Field> old, Field f ) {
      ArrayList<Field> list = new ArrayList<Field>();
      if ( old != null ) {
         int count = old.size();
         for ( int i = 0; i < count; i++ ) {
            list.add( old.get( i ) );
         }
      }
      list.add( f );
      return list;
   }

   /**
    * Starts field range calculations from current field on for pawns.
    * 
    * @param fig
    *           Figure
    * @param f
    *           current Field
    */
   public void checkPawnRange( Figure fig, Field f ) {
      if ( fig.getType() != Figure.PAWN ) return;
      dEnter( fig );
      // black pawn can only move "south", white ones only "north"
      int signY = ( board.getBlackFiguresList().contains( fig ) ) ? -1 : 1;
      int[] b = f.getView().getBoardPos();
      int maxStep = ( fig.isUntouched() ) ? 2 : 1;

      // regular pawn moves
      checkStraight( fig, pathList( null, f ), maxStep, signY, b,
            FigureRangeChecking.Y_AXIS, true, MOVE_RANGE );
      checkUpDownStraightStart( fig, pathList( null, f ), maxStep, signY, b,
            FigureRangeChecking.Y_AXIS, true, MOVE_RANGE );

      // special attack pawn moves
      checkDiagonal( fig, pathList( null, f ), 1, -1, signY, b, true,
            MOVE_RANGE ); // left
      checkDiagonal( fig, pathList( null, f ), 1, 1, signY, b, true, MOVE_RANGE ); // right
      checkUpDownDiagonalStart( fig, pathList( null, f ), 1, -1, signY, b,
            true, MOVE_RANGE ); // left
      checkUpDownDiagonalStart( fig, pathList( null, f ), 1, 1, signY, b, true,
            MOVE_RANGE ); // right
   }

   /**
    * Starts field range calculations from current field on for castles.
    * 
    * @param fig
    *           Figure
    * @param f
    *           current Field
    */
   public void checkCastleRange( Figure fig, Field f ) {
      if ( fig.getType() != Figure.CASTLE ) return;
      dEnter( fig );
      int[] b = f.getView().getBoardPos();

      // regular castle moves
      ArrayList<Field> pathList = pathList( null, f );
      checkStraight( fig, pathList, 10, 1, b, FigureRangeChecking.Y_AXIS,
            false, MOVE_RANGE );
      checkStraight( fig, pathList, 10, -1, b, FigureRangeChecking.Y_AXIS,
            false, MOVE_RANGE );
      checkUpDownStraightStart( fig, pathList, 10, 1, b,
            FigureRangeChecking.Y_AXIS, false, MOVE_RANGE );
      checkUpDownStraightStart( fig, pathList, 10, -1, b,
            FigureRangeChecking.Y_AXIS, false, MOVE_RANGE );

      checkStraight( fig, pathList, 6, 1, b, FigureRangeChecking.X_AXIS, false,
            MOVE_RANGE );
      checkStraight( fig, pathList, 6, -1, b, FigureRangeChecking.X_AXIS,
            false, MOVE_RANGE );
      checkUpDownStraightStart( fig, pathList, 6, 1, b,
            FigureRangeChecking.X_AXIS, false, MOVE_RANGE );
      checkUpDownStraightStart( fig, pathList, 6, -1, b,
            FigureRangeChecking.X_AXIS, false, MOVE_RANGE );
   }

   /**
    * Starts field range calculations from current field on for knights.
    * 
    * @param fig
    *           Figure
    * @param f
    *           current Field
    */
   public void checkKnightRange( Figure fig, Field f ) {
      if ( fig.getType() != Figure.KNIGHT ) return;
      dEnter( fig );
      int[] b = f.getView().getBoardPos();

      // regular knight moves
      checkJumps( fig, pathList( null, f ), b, MOVE_RANGE );
   }

   /**
    * Starts field range calculations from current field on for bishops.
    * 
    * @param fig
    *           Figure
    * @param f
    *           current Field
    */
   public void checkBishopRange( Figure fig, Field f ) {
      if ( fig.getType() != Figure.BISHOP ) return;
      dEnter( fig );
      int[] b = f.getView().getBoardPos();

      // regular bishop moves
      checkDiagonal( fig, pathList( null, f ), 6, -1, 1, b, false, MOVE_RANGE ); // north
      // west
      checkDiagonal( fig, pathList( null, f ), 6, 1, 1, b, false, MOVE_RANGE ); // north
      // east
      checkUpDownDiagonalStart( fig, pathList( null, f ), 6, -1, 1, b, false,
            MOVE_RANGE ); // north west
      checkUpDownDiagonalStart( fig, pathList( null, f ), 6, 1, 1, b, false,
            MOVE_RANGE ); // north east

      checkDiagonal( fig, pathList( null, f ), 6, -1, -1, b, false, MOVE_RANGE ); // south
      // west
      checkDiagonal( fig, pathList( null, f ), 6, 1, -1, b, false, MOVE_RANGE ); // south
      // east
      checkUpDownDiagonalStart( fig, pathList( null, f ), 6, -1, -1, b, false,
            MOVE_RANGE ); // south west
      checkUpDownDiagonalStart( fig, pathList( null, f ), 6, 1, -1, b, false,
            MOVE_RANGE ); // south east
   }

   /**
    * Starts field range calculations from current field on for queens and
    * kings.
    * 
    * @param fig
    *           Figure
    * @param f
    *           current Field
    * @param x
    *           board width
    * @param y
    *           board height
    */
   public void checkKingQueenRange( Figure fig, Field f, int x, int y ) {
      if ( fig.getType() != Figure.QUEEN && fig.getType() != Figure.KING ) return;
      dEnter( fig );
      int[] b = f.getView().getBoardPos();

      // regular king and queen moves
      checkDiagonal( fig, pathList( null, f ), y, -1, 1, b, false, MOVE_RANGE ); // north
      // west
      checkDiagonal( fig, pathList( null, f ), y, 1, 1, b, false, MOVE_RANGE ); // north
      // east
      checkUpDownDiagonalStart( fig, pathList( null, f ), y, -1, 1, b, false,
            MOVE_RANGE ); // north west
      checkUpDownDiagonalStart( fig, pathList( null, f ), y, 1, 1, b, false,
            MOVE_RANGE ); // north east

      checkDiagonal( fig, pathList( null, f ), y, -1, -1, b, false, MOVE_RANGE ); // south
      // west
      checkDiagonal( fig, pathList( null, f ), y, 1, -1, b, false, MOVE_RANGE ); // south
      // east
      checkUpDownDiagonalStart( fig, pathList( null, f ), y, -1, -1, b, false,
            MOVE_RANGE ); // south west
      checkUpDownDiagonalStart( fig, pathList( null, f ), y, 1, -1, b, false,
            MOVE_RANGE ); // south east

      checkStraight( fig, pathList( null, f ), x, 1, b,
            FigureRangeChecking.Y_AXIS, false, MOVE_RANGE );
      checkStraight( fig, pathList( null, f ), x, -1, b,
            FigureRangeChecking.Y_AXIS, false, MOVE_RANGE );
      checkUpDownStraightStart( fig, pathList( null, f ), x, 1, b,
            FigureRangeChecking.Y_AXIS, false, MOVE_RANGE );
      checkUpDownStraightStart( fig, pathList( null, f ), x, -1, b,
            FigureRangeChecking.Y_AXIS, false, MOVE_RANGE );

      checkStraight( fig, pathList( null, f ), y, 1, b,
            FigureRangeChecking.X_AXIS, false, MOVE_RANGE );
      checkStraight( fig, pathList( null, f ), y, -1, b,
            FigureRangeChecking.X_AXIS, false, MOVE_RANGE );
      checkUpDownStraightStart( fig, pathList( null, f ), y, 1, b,
            FigureRangeChecking.X_AXIS, false, MOVE_RANGE );
      checkUpDownStraightStart( fig, pathList( null, f ), y, -1, b,
            FigureRangeChecking.X_AXIS, false, MOVE_RANGE );
   }

   /**
    * Sets Figure's and Field's properties for possible attack move.
    * 
    * @param fig
    *           movable Figure
    * @param target
    *           Field
    * @param path
    *           Field path from Figure's Field to target Field
    * @param checkType
    *           MOVE_RANGE, CHECK_THREAD, or CHAIN_THREAD
    */
   protected void attack( Figure fig, Field target, ArrayList<Field> path,
         int checkType ) {
      Figure t = target.getFigure();
      if ( checkType == MOVE_RANGE ) {
         fig.getAttackList().add( target );
         t.getAttackersList().add( fig.getField() );
      }
      if ( t.getType() == Figure.KING ) {
         // king under attack
         switch ( checkType ) {
            case CHECK_THREAD: // indirect check condition through "chained"
               // own figure on path
               fig.getCheckThreadPaths().add( path );
               break;
            case CHAIN_THREAD: // indirect check condition through chained
               // opposing figure on path
               fig.getChainedThreadPaths().add( path );
               break;
            default: // direct check condition
               fig.getCheckPaths().add( path );
         }
      }
   }

   /**
    * Checks next diagonal field relative to current board position if it's free
    * to move onto it. If signX is 1 field is "east", if it's -1 field is
    * "west". If signY is 1 forward is "north", if it's -1 it's "south".<br>
    * The limit is the number of fields that will be checked at most if no
    * figure is in the way. It's determined by the range of a figure.<br>
    * Normally a field is allowed to be moved on, if it's not occupied. However,
    * some figures can only move diagonally for attacks (pawns). To "turn
    * around" the checking, attackOnly should be true.<br>
    * The checking will be done for all z level of the current field.
    * 
    * @param fig
    *           Figure
    * @param path
    *           path of Fields to the current check position
    * @param limit
    *           maximum number of fields to check further
    * @param signX
    *           movement "west": -1, "east": 1
    * @param signY
    *           movement "south" (black pawns): -1, "north" (white pawns): 1
    * @param b
    *           current 3D board position
    * @param attackOnly
    *           allows move only, if target field is occupied by a figure
    * @param checkType
    *           MOVE_RANGE, CHECK_THREAD, or CHAIN_THREAD
    */
   protected void checkDiagonal( Figure fig, ArrayList<Field> path, int limit,
         int signX, int signY, int[] b, boolean attackOnly, int checkType ) {
      boolean isWhite = fig.isWhite();
      dEnter( limit, "checkDiagonal", b, checkType );
      if ( limit < 1 ) {
         dStop( limit, checkType );
         return;
      }
      int[] p3d = BoardUtils.make3DPointArray( b[0], b[1], b[2] );
      // if current position is non existing field, abort recursion here
      if ( !board.getFieldsByName().containsKey( board.get3DName( p3d ) ) ) {
         dStop( limit, p3d, checkType );
         return;
      }
      // do step
      p3d[1] = p3d[1] + signY * 1;
      p3d[0] = p3d[0] + signX * 1;
      String targetName = board.get3DName( p3d );
      if ( board.getFieldsByName().containsKey( targetName ) ) {
         Field t = board.getFieldsByName().get( targetName );
         if ( t.getFigure() != null ) {
            if ( isWhite != t.getFigure().isWhite() ) {
               attack( fig, t, pathList( path, t ), checkType );
               // recurse if it was move checking
               if ( checkType == MOVE_RANGE ) {
                  checkDiagonal( fig, pathList( path, t ), limit - 1, signX,
                        signY, p3d, attackOnly, CHAIN_THREAD );
                  checkUpward( fig, pathList( path, t ), 5, limit, signX,
                        signY, p3d, attackOnly, CHAIN_THREAD );
                  checkDownward( fig, pathList( path, t ), 0, limit, signX,
                        signY, p3d, attackOnly, CHAIN_THREAD );
               }
            }
            else {
               if ( checkType == MOVE_RANGE ) {
                  fig.getCoverList().add( t );
                  checkDiagonal( fig, pathList( path, t ), limit - 1, signX,
                        signY, p3d, attackOnly, CHECK_THREAD );
                  checkUpward( fig, pathList( path, t ), 5, limit, signX,
                        signY, p3d, attackOnly, CHECK_THREAD );
                  checkDownward( fig, pathList( path, t ), 0, limit, signX,
                        signY, p3d, attackOnly, CHECK_THREAD );
               }
            }
            dStop( limit, t.getFigure(), checkType );
         }
         else {
            if ( !attackOnly && checkType == MOVE_RANGE ) {
               fig.getMoveList().add( t );
            }
            else if ( !attackOnly && checkType == CHAIN_THREAD ) {
               fig.getGhostMoveList().add( t );
            }
            dRecurse( limit );
            checkDiagonal( fig, pathList( path, t ), limit - 1, signX, signY,
                  p3d, attackOnly, checkType );
            checkUpward( fig, pathList( path, t ), 5, limit, signX, signY, p3d,
                  attackOnly, checkType );
            checkDownward( fig, pathList( path, t ), 0, limit, signX, signY,
                  p3d, attackOnly, checkType );
         }
      }
      else {
         dStop( limit, p3d, checkType );
      }
   }

   /**
    * Checks all other z levels away from current board position if they are
    * free to switch the level at the start of a move.<br>
    * If signX is 1 field is "east", if it's -1 field is "west". If signY is 1
    * forward is "north", if it's -1 it's "south".<br>
    * The limit is the number of fields that will be checked at most if no
    * figure is in the way. It's determined by the range of a figure.<br>
    * Normally a field is allowed to be moved on, if it's not occupied. However,
    * some figures can only move diagonally for attacks (pawns). To "turn
    * around" the checking, attackOnly should be true.<br>
    * The checking will be done for all z level of the current field.
    * 
    * @param fig
    *           Figure
    * @param path
    *           path of Fields to the current check position
    * @param limit
    *           maximum number of fields to check further
    * @param signX
    *           movement "west": -1, "east": 1
    * @param signY
    *           movement "south" (black pawns): -1, "north" (white pawns): 1
    * @param b
    *           current 3D board position
    * @param attackOnly
    *           allows move only, if target field is occupied by a figure
    * @param checkType
    *           MOVE_RANGE, CHECK_THREAD, or CHAIN_THREAD
    */
   public void checkUpDownDiagonalStart( Figure fig, ArrayList<Field> path,
         int limit, int signX, int signY, int[] b, boolean attackOnly,
         int checkType ) {
      // check current field's z levels
      for ( int z = b[2] + 1; z <= 5; z++ ) {
         int[] p3d = BoardUtils.make3DPointArray( b[0], b[1], z );
         String targetName = board.get3DName( p3d );
         if ( board.getFieldsByName().containsKey( targetName ) ) {
            Field t = board.getFieldsByName().get( targetName );
            if ( t.getFigure() != null ) {
               fig.getBlockList().add( t );
               if ( checkType == MOVE_RANGE ) {
                  if ( t.getFigure().isWhite() == fig.isWhite() ) {
                     checkDiagonal( fig, pathList( path, t ), limit, signX,
                           signY, p3d, attackOnly, CHECK_THREAD );
                  }
                  else {
                     checkDiagonal( fig, pathList( path, t ), limit, signX,
                           signY, p3d, attackOnly, CHAIN_THREAD );
                  }
               }
               dStop( limit, t.getFigure(), checkType );
               break;
            }
            else {
               // if z level is free, check straight move from here on
               dRecurse( limit );
               checkDiagonal( fig, pathList( path, t ), limit, signX, signY,
                     p3d, attackOnly, checkType );
            }
         }
         else {
            dIgnoreZ( limit, z );
         }
      } // for: z level upwards
      for ( int z = b[2] - 1; z >= 0; z-- ) {
         int[] p3d = BoardUtils.make3DPointArray( b[0], b[1], z );
         String targetName = board.get3DName( p3d );
         if ( board.getFieldsByName().containsKey( targetName ) ) {
            Field t = board.getFieldsByName().get( targetName );
            if ( t.getFigure() != null ) {
               fig.getBlockList().add( t );
               if ( checkType == MOVE_RANGE ) {
                  if ( t.getFigure().isWhite() == fig.isWhite() ) {
                     checkDiagonal( fig, pathList( path, t ), limit, signX,
                           signY, p3d, attackOnly, CHECK_THREAD );
                  }
                  else {
                     checkDiagonal( fig, pathList( path, t ), limit, signX,
                           signY, p3d, attackOnly, CHAIN_THREAD );
                  }
               }
               dStop( limit, t.getFigure(), checkType );
               break;
            }
            else {
               // if z level is free, check straight move from here on
               dRecurse( limit );
               checkDiagonal( fig, pathList( path, t ), limit, signX, signY,
                     p3d, attackOnly, checkType );
            }
         }
         else {
            dIgnoreZ( limit, z );
         }
      } // for: z level downwards
   }

   /**
    * Checks all upward fields in proper order from current board position on if
    * they're free to move onto. If a free field is found, the search continues
    * with diagonal checking.<br>
    * If signX is 1 field is "east", if it's -1 field is "west". If signY is 1
    * forward is "north", if it's -1 it's "south".<br>
    * The limit is the number of rows that will be checked at most if no figure
    * is in the way. It's determined by the range of a figure.<br>
    * Normally a field is allowed to be moved on, if it's not occupied. However,
    * some figures can only move diagonally for attacks (pawns). To "turn
    * around" the checking, attackOnly should be true.<br>
    * 
    * @param fig
    *           Figure
    * @param path
    *           path of Fields to the current check position
    * @param maxZ
    *           highest z level ID
    * @param limit
    *           maximum number of fields to check further
    * @param signX
    *           movement "west": -1, "east": 1
    * @param signY
    *           movement "south" (black pawns): -1, "north" (white pawns): 1
    * @param b
    *           current 3D board position
    * @param attackOnly
    *           allows move only, if target field is occupied by a figure
    * @param checkType
    *           MOVE_RANGE, CHECK_THREAD, or CHAIN_THREAD
    */
   protected void checkUpward( Figure fig, ArrayList<Field> path, int maxZ,
         int limit, int signX, int signY, int[] b, boolean attackOnly,
         int checkType ) {
      boolean isWhite = fig.isWhite();
      dEnter( limit, "checkUpward diagonal", b, checkType );
      if ( limit < 1 ) {
         dStop( limit, checkType );
         return;
      }
      int[] p3d = BoardUtils.make3DPointArray( b[0], b[1], b[2] );
      // if current position is non existing field, abort recursion here
      if ( !board.getFieldsByName().containsKey( board.get3DName( p3d ) ) ) {
         dStop( limit, p3d, checkType );
         return;
      }
      for ( int z = p3d[2]; z <= maxZ; z++ ) {
         p3d[2] += 1;
         String targetName = board.get3DName( p3d );
         if ( board.getFieldsByName().containsKey( targetName ) ) {
            Field t = board.getFieldsByName().get( targetName );
            if ( t.getFigure() != null ) {
               if ( isWhite != t.getFigure().isWhite() ) {
                  attack( fig, t, pathList( path, t ), checkType );
                  // recurse if it was move checking
                  if ( checkType == MOVE_RANGE ) {
                     checkDiagonal( fig, pathList( path, t ), limit - 1, signX,
                           signY, p3d, attackOnly, CHAIN_THREAD );
                  }
               }
               else {
                  if ( checkType == MOVE_RANGE ) {
                     fig.getCoverList().add( t );
                     checkDiagonal( fig, pathList( path, t ), limit - 1, signX,
                           signY, p3d, attackOnly, CHECK_THREAD );
                  }
               }
               dStop( limit, t.getFigure(), checkType );
               break;
            }
            else {
               if ( !attackOnly && checkType == MOVE_RANGE ) {
                  fig.getMoveList().add( t );
               }
               else if ( !attackOnly && checkType == CHAIN_THREAD ) {
                  fig.getGhostMoveList().add( t );
               }
               dRecurse( limit );
               checkDiagonal( fig, pathList( path, t ), limit - 1, signX,
                     signY, p3d, attackOnly, checkType );
            }
         }
         else {
            if ( p3d[2] % 2 == 1 ) {
               dIgnoreZ( limit, z );
            }
            else {
               dStop( limit, p3d, checkType );
               break; // field with the requested position does not exist
            }
         }
      }
   }

   /**
    * Checks all downward fields in proper order from current board position on
    * if they're free to move onto. If a free field is found, the search
    * continues with diagonal checking.<br>
    * If signX is 1 field is "east", if it's -1 field is "west". If signY is 1
    * forward is "north", if it's -1 it's "south".<br>
    * The limit is the number of rows that will be checked at most if no figure
    * is in the way. It's determined by the range of a figure.<br>
    * Normally a field is allowed to be moved on, if it's not occupied. However,
    * some figures can only move diagonally for attacks (pawns). To "turn
    * around" the checking, attackOnly should be true.<br>
    * 
    * @param fig
    *           Figure
    * @param path
    *           path of Fields to the current check position
    * @param minZ
    *           lowest z level ID
    * @param limit
    *           maximum number of fields to check further
    * @param signX
    *           movement "west": -1, "east": 1
    * @param signY
    *           movement "south" (black pawns): -1, "north" (white pawns): 1
    * @param b
    *           current 3D board position
    * @param attackOnly
    *           allows move only, if target field is occupied by a figure
    * @param checkType
    *           MOVE_RANGE, CHECK_THREAD, or CHAIN_THREAD
    */
   protected void checkDownward( Figure fig, ArrayList<Field> path, int minZ,
         int limit, int signX, int signY, int[] b, boolean attackOnly,
         int checkType ) {
      dEnter( limit, "checkDownward diagonal", b, checkType );
      boolean isWhite = fig.isWhite();
      if ( limit < 1 ) {
         dStop( limit, checkType );
         return;
      }
      int[] p3d = BoardUtils.make3DPointArray( b[0], b[1], b[2] );
      // if current position is non existing field, abort recursion here
      if ( !board.getFieldsByName().containsKey( board.get3DName( p3d ) ) ) {
         dStop( limit, p3d, checkType );
         return;
      }
      for ( int z = p3d[2]; z >= minZ; z-- ) {
         p3d[2] -= 1;
         String targetName = board.get3DName( p3d );
         if ( board.getFieldsByName().containsKey( targetName ) ) {
            Field t = board.getFieldsByName().get( targetName );
            if ( t.getFigure() != null ) {
               if ( isWhite != t.getFigure().isWhite() ) {
                  attack( fig, t, pathList( path, t ), checkType );
                  // recurse if it was move checking
                  if ( checkType == MOVE_RANGE ) {
                     checkDiagonal( fig, pathList( path, t ), limit - 1, signX,
                           signY, p3d, attackOnly, CHAIN_THREAD );
                  }
               }
               else {
                  if ( checkType == MOVE_RANGE ) {
                     fig.getCoverList().add( t );
                     checkDiagonal( fig, pathList( path, t ), limit - 1, signX,
                           signY, p3d, attackOnly, CHECK_THREAD );
                  }
               }
               dStop( limit, t.getFigure(), checkType );
               break;
            }
            else {
               if ( !attackOnly && checkType == MOVE_RANGE ) {
                  fig.getMoveList().add( t );
               }
               else if ( !attackOnly && checkType == CHAIN_THREAD ) {
                  fig.getGhostMoveList().add( t );
               }
               dRecurse( limit );
               checkDiagonal( fig, pathList( path, t ), limit - 1, signX,
                     signY, p3d, attackOnly, checkType );
            }
         }
         else {
            if ( p3d[2] % 2 == 1 ) {
               dIgnoreZ( limit, z );
            }
            else {
               dStop( limit, p3d, checkType );
               break; // field with the requested position does not exist
            }
         }
      }
   }

   /**
    * Checks next field relative to current board position if it's free to move
    * onto it.<br>
    * For xy = 0:<br>
    * If sign is 1 it's "east", if it's -1 it's "west".<br>
    * For xy = 1:<br>
    * If sign is 1 it's "north", if it's -1 it's "south".<br>
    * The limit is the number of columns that will be checked at most if no
    * figure is in the way. It's determined by the range of a figure.<br>
    * The checking will be done for all z levels of the current field.
    * 
    * @param fig
    *           Figure
    * @param path
    *           path of Fields to the current check position
    * @param limit
    *           maximum number of fields to check further
    * @param sign
    *           movement direction
    * @param b
    *           current 3D board position
    * @param xy
    *           movement axis
    * @param mustNotAttack
    *           true for pawns since they can only attack diagonally
    * @param checkType
    *           MOVE_RANGE, CHECK_THREAD, or CHAIN_THREAD
    */
   public void checkStraight( Figure fig, ArrayList<Field> path, int limit,
         int sign, int[] b, int xy, boolean mustNotAttack, int checkType ) {
      dEnter( limit, "checkStraight", b, checkType );
      boolean isWhite = fig.isWhite();
      if ( xy < X_AXIS || xy > Y_AXIS ) {
         System.err.println( "Invalid axis value for xy parameter: " + xy );
         return;
      }
      if ( limit < 1 ) {
         dStop( limit, checkType );
         return;
      }
      int[] p3d = BoardUtils.make3DPointArray( b[0], b[1], b[2] );
      // if current position is non existing field, abort recursion here
      if ( !board.getFieldsByName().containsKey( board.get3DName( p3d ) ) ) {
         dStop( limit, p3d, checkType );
         return;
      }
      // check next field and its z levels
      p3d[xy] = p3d[xy] + sign * 1;
      String targetName = board.get3DName( p3d );
      if ( board.getFieldsByName().containsKey( targetName ) ) {
         Field t = board.getFieldsByName().get( targetName );
         ArrayList<Field> pathList = pathList( path, t );
         if ( t.getFigure() != null ) {
            if ( isWhite != t.getFigure().isWhite() && !mustNotAttack ) {
               attack( fig, t, pathList, checkType );
               // recurse if it was move checking
               if ( checkType == MOVE_RANGE ) {
                  checkStraight( fig, pathList, limit - 1, sign, p3d, xy,
                        mustNotAttack, CHAIN_THREAD );
                  checkUpward( fig, pathList, 5, limit, sign, p3d, xy,
                        mustNotAttack, CHAIN_THREAD );
                  checkDownward( fig, pathList, 0, limit, sign, p3d, xy,
                        mustNotAttack, CHAIN_THREAD );
               }
            }
            else {
               if ( !mustNotAttack && checkType == MOVE_RANGE ) {
                  fig.getCoverList().add( t );
                  checkStraight( fig, pathList, limit - 1, sign, p3d, xy,
                        mustNotAttack, CHECK_THREAD );
                  checkUpward( fig, pathList, 5, limit, sign, p3d, xy,
                        mustNotAttack, CHECK_THREAD );
                  checkDownward( fig, pathList, 0, limit, sign, p3d, xy,
                        mustNotAttack, CHECK_THREAD );
               }
            }
            dStop( limit, t.getFigure(), checkType );
         }
         else {
            if ( checkType == MOVE_RANGE ) {
               fig.getMoveList().add( t );
            }
            else if ( checkType == CHAIN_THREAD ) {
               fig.getGhostMoveList().add( t );
            }
            dRecurse( limit );
            checkStraight( fig, pathList, limit - 1, sign, p3d, xy,
                  mustNotAttack, checkType );
            checkUpward( fig, pathList, 5, limit, sign, p3d, xy, mustNotAttack,
                  checkType );
            checkDownward( fig, pathList, 0, limit, sign, p3d, xy,
                  mustNotAttack, checkType );
         }
      }
      else {
         dStop( limit, p3d, checkType );
      }
   }

   /**
    * Checks all other z levels away from current board position if they are
    * free to switch the level at the start of a move.<br>
    * For xy = 0:<br>
    * If sign is 1 it's "east", if it's -1 it's "west".<br>
    * For xy = 1:<br>
    * If sign is 1 it's "north", if it's -1 it's "south".<br>
    * The limit is the number of columns that will be checked at most if no
    * figure is in the way. It's determined by the range of a figure.<br>
    * The checking will be done for all z levels of the current field except the
    * one of the position itself.<br>
    * 
    * @param fig
    *           Figure
    * @param path
    *           path of Fields to the current check position
    * @param limit
    *           maximum number of fields to check further
    * @param sign
    *           movement direction
    * @param b
    *           current 3D board position
    * @param xy
    *           movement axis
    * @param mustNotAttack
    *           true for pawns since they can only attack diagonally
    * @param checkType
    *           MOVE_RANGE, CHECK_THREAD, or CHAIN_THREAD
    */
   public void checkUpDownStraightStart( Figure fig, ArrayList<Field> path,
         int limit, int sign, int[] b, int xy, boolean mustNotAttack,
         int checkType ) {
      // check current field's z levels
      for ( int z = b[2] + 1; z <= 5; z++ ) {
         int[] p3d = BoardUtils.make3DPointArray( b[0], b[1], z );
         String targetName = board.get3DName( p3d );
         if ( board.getFieldsByName().containsKey( targetName ) ) {
            Field t = board.getFieldsByName().get( targetName );
            if ( t.getFigure() != null ) {
               fig.getBlockList().add( t );
               if ( checkType == MOVE_RANGE ) {
                  if ( t.getFigure().isWhite() == fig.isWhite() ) {
                     checkStraight( fig, pathList( path, t ), limit, sign, p3d,
                           xy, mustNotAttack, CHECK_THREAD );
                  }
                  else {
                     checkStraight( fig, pathList( path, t ), limit, sign, p3d,
                           xy, mustNotAttack, CHAIN_THREAD );
                  }
               }
               dStop( limit, t.getFigure(), checkType );
               break;
            }
            else {
               // if z level is free, check straight move from here on
               dRecurse( limit );
               checkStraight( fig, pathList( path, t ), limit, sign, p3d, xy,
                     mustNotAttack, checkType );
            }
         }
         else {
            dIgnoreZ( limit, z );
         }
      } // for: z level upwards
      for ( int z = b[2] - 1; z >= 0; z-- ) {
         int[] p3d = BoardUtils.make3DPointArray( b[0], b[1], z );
         String targetName = board.get3DName( p3d );
         if ( board.getFieldsByName().containsKey( targetName ) ) {
            Field t = board.getFieldsByName().get( targetName );
            if ( t.getFigure() != null ) {
               fig.getBlockList().add( t );
               if ( checkType == MOVE_RANGE ) {
                  if ( t.getFigure().isWhite() == fig.isWhite() ) {
                     checkStraight( fig, pathList( path, t ), limit, sign, p3d,
                           xy, mustNotAttack, CHECK_THREAD );
                  }
                  else {
                     checkStraight( fig, pathList( path, t ), limit, sign, p3d,
                           xy, mustNotAttack, CHAIN_THREAD );
                  }
               }
               dStop( limit, t.getFigure(), checkType );
               break;
            }
            else {
               // if z level is free, check straight move from here on
               dRecurse( limit );
               checkStraight( fig, pathList( path, t ), limit, sign, p3d, xy,
                     mustNotAttack, checkType );
            }
         }
         else {
            dIgnoreZ( limit, z );
         }
      } // for: z level downwards
   }

   /**
    * Checks all upward fields in proper order from current board position on if
    * they're free to move onto. If a free field is found, the search continues
    * with forward checking.<br>
    * For xy = 0:<br>
    * If sign is 1 it's "east", if it's -1 it's "west".<br>
    * For xy = 1:<br>
    * If sign is 1 it's "north", if it's -1 it's "south".<br>
    * The limit is the number of rows that will be checked at most if no figure
    * is in the way. It's determined by the range of a figure.<br>
    * 
    * @param fig
    *           Figure
    * @param path
    *           path of Fields to the current check position
    * @param maxZ
    *           highest z level ID
    * @param limit
    *           maximum number of fields to check further
    * @param sign
    *           movement direction
    * @param b
    *           current 3D board position
    * @param xy
    *           movement axis
    * @param mustNotAttack
    *           true for pawns since they can only attack diagonally
    * @param checkType
    *           MOVE_RANGE, CHECK_THREAD, or CHAIN_THREAD
    */
   public void checkUpward( Figure fig, ArrayList<Field> path, int maxZ,
         int limit, int sign, int[] b, int xy, boolean mustNotAttack,
         int checkType ) {
      dEnter( limit, "checkUpward straight", b, checkType );
      boolean isWhite = fig.isWhite();
      if ( limit < 1 ) {
         dStop( limit, checkType );
         return;
      }
      int[] p3d = BoardUtils.make3DPointArray( b[0], b[1], b[2] );
      // if current position is non existing field, abort recursion here
      if ( !board.getFieldsByName().containsKey( board.get3DName( p3d ) ) ) {
         dStop( limit, p3d, checkType );
         return;
      }
      for ( int z = p3d[2]; z <= maxZ; z++ ) {
         p3d[2] += 1;
         String targetName = board.get3DName( p3d );
         if ( board.getFieldsByName().containsKey( targetName ) ) {
            Field t = board.getFieldsByName().get( targetName );
            if ( t.getFigure() != null ) {
               if ( isWhite != t.getFigure().isWhite() && !mustNotAttack ) {
                  attack( fig, t, pathList( path, t ), checkType );
                  // recurse if it was move checking
                  if ( checkType == MOVE_RANGE ) {
                     checkStraight( fig, pathList( path, t ), limit - 1, sign,
                           p3d, xy, mustNotAttack, CHAIN_THREAD );
                  }
               }
               else {
                  if ( !mustNotAttack && checkType == MOVE_RANGE ) {
                     fig.getCoverList().add( t );
                     checkStraight( fig, pathList( path, t ), limit - 1, sign,
                           p3d, xy, mustNotAttack, CHECK_THREAD );
                  }
               }
               dStop( limit, t.getFigure(), checkType );
               break;
            }
            else {
               if ( checkType == MOVE_RANGE ) {
                  fig.getMoveList().add( t );
               }
               else if ( checkType == CHAIN_THREAD ) {
                  fig.getGhostMoveList().add( t );
               }
               dRecurse( limit );
               checkStraight( fig, pathList( path, t ), limit - 1, sign, p3d,
                     xy, mustNotAttack, checkType );
            }
         }
         else {
            if ( p3d[2] % 2 == 1 ) {
               dIgnoreZ( limit, z );
            }
            else {
               dStop( limit, p3d, checkType );
               break; // field with the requested position does not exist
            }
         }
      }
   }

   /**
    * Checks all downward fields in proper order from current board position on
    * if they're free to move onto. If a free field is found, the search
    * continues with forward checking.<br>
    * For xy = 0:<br>
    * If sign is 1 it's "east", if it's -1 it's "west".<br>
    * For xy = 1:<br>
    * If sign is 1 it's "north", if it's -1 it's "south".<br>
    * The limit is the number of rows that will be checked at most if no figure
    * is in the way. It's determined by the range of a figure.<br>
    * 
    * @param fig
    *           Figure
    * @param path
    *           path of Fields to the current check position
    * @param minZ
    *           lowest z level ID
    * @param limit
    *           maximum number of fields to check further
    * @param sign
    *           movement direction
    * @param b
    *           current 3D board position
    * @param xy
    *           movement axis
    * @param mustNotAttack
    *           true for pawns since they can only attack diagonally
    * @param checkType
    *           MOVE_RANGE, CHECK_THREAD, or CHAIN_THREAD
    */
   public void checkDownward( Figure fig, ArrayList<Field> path, int minZ,
         int limit, int sign, int[] b, int xy, boolean mustNotAttack,
         int checkType ) {
      dEnter( limit, "checkDownward straight", b, checkType );
      boolean isWhite = fig.isWhite();
      if ( limit < 1 ) {
         dStop( limit, checkType );
         return;
      }
      int[] p3d = BoardUtils.make3DPointArray( b[0], b[1], b[2] );
      // if current position is non existing field, abort recursion here
      if ( !board.getFieldsByName().containsKey( board.get3DName( p3d ) ) ) {
         dStop( limit, p3d, checkType );
         return;
      }
      for ( int z = p3d[2]; z >= minZ; z-- ) {
         p3d[2] -= 1;
         String targetName = board.get3DName( p3d );
         if ( board.getFieldsByName().containsKey( targetName ) ) {
            Field t = board.getFieldsByName().get( targetName );
            if ( t.getFigure() != null ) {
               if ( isWhite != t.getFigure().isWhite() && !mustNotAttack ) {
                  attack( fig, t, pathList( path, t ), checkType );
                  // recurse if it was move checking
                  if ( checkType == MOVE_RANGE ) {
                     checkStraight( fig, pathList( path, t ), limit - 1, sign,
                           p3d, xy, mustNotAttack, CHAIN_THREAD );
                  }
               }
               else {
                  if ( !mustNotAttack && checkType == MOVE_RANGE ) {
                     fig.getCoverList().add( t );
                     checkStraight( fig, pathList( path, t ), limit - 1, sign,
                           p3d, xy, mustNotAttack, CHECK_THREAD );
                  }
               }
               dStop( limit, t.getFigure(), checkType );
               break;
            }
            else {
               if ( checkType == MOVE_RANGE ) {
                  fig.getMoveList().add( t );
               }
               else if ( checkType == CHAIN_THREAD ) {
                  fig.getGhostMoveList().add( t );
               }
               dRecurse( limit );
               checkStraight( fig, pathList( path, t ), limit - 1, sign, p3d,
                     xy, mustNotAttack, checkType );
            }
         }
         else {
            if ( p3d[2] % 2 == 1 ) {
               dIgnoreZ( limit, z );
            }
            else {
               dStop( limit, p3d, checkType );
               break; // field with the requested position does not exist
            }
         }
      }
   }

   /**
    * Checks all jump destination fields on all z levels relative to current
    * board position if they're free to move onto.<br>
    * 
    * @param fig
    *           Figure
    * @param path
    *           path of Fields to the current check position
    * @param b
    *           current 3D board position
    * @param checkType
    *           MOVE_RANGE, CHECK_THREAD, or CHAIN_THREAD
    */
   public void checkJumps( Figure fig, ArrayList<Field> path, int[] b,
         int checkType ) {
      dEnter( 1, "checkJumps", b, checkType );
      boolean isWhite = fig.isWhite();
      // if current position is non existing field, abort recursion here
      if ( !board.getFieldsByName().containsKey( board.get3DName( b ) ) ) {
         dStop( 1, b, checkType );
         return;
      }
      int[] p3d = new int[3];
      // iterate over x and y axis signs
      for ( int sx = -1; sx < 2; sx += 2 ) {
         for ( int sy = -1; sy < 2; sy += 2 ) {
            // iteration for 2 steps forward and one to the side or vice versa
            for ( int j = 0; j < 2; j++ ) {
               p3d[0] = b[0] + sx * ( 1 + j );
               p3d[1] = b[1] + sy * ( 2 - j );
               // iteration of all z levels
               for ( int z = 0; z <= 5; z++ ) {
                  p3d[2] = z;
                  String targetName = board.get3DName( p3d );
                  if ( board.getFieldsByName().containsKey( targetName ) ) {
                     Field t = board.getFieldsByName().get( targetName );
                     if ( t.getFigure() != null ) {
                        if ( isWhite != t.getFigure().isWhite() ) {
                           fig.getAttackList().add( t );
                           t.getFigure().getAttackersList()
                                 .add( fig.getField() );
                           // knights always attack directly
                           if ( t.getFigure().getType() == Figure.KING ) {
                              fig.getCheckPaths().add( pathList( path, t ) );
                           }
                        }
                        else {
                           fig.getCoverList().add( t );
                        }
                        dStop( 1, t.getFigure(), checkType );
                     } // if: field with figure
                     else {
                        fig.getMoveList().add( t );
                        dStop( 1, checkType );
                     } // else: field without figure
                  } // if: field is available
                  else {
                     dIgnoreZ( 1, z );
                  } // else: field is not present
               } // for: z levels
            } // for: j
         } // for: sign y
      } // for: sign x
   }

   private String dCheck( int checkType ) {
      String check = "";
      switch ( checkType ) {
         case CHECK_THREAD:
            check = ", (check thread by covered own figure)";
            break;
         case CHAIN_THREAD:
            check = ", (check thread by chained opposing figure)";
            break;
         default:
      }
      return check;
   }

   private void dEnter( Figure fig ) {
      if ( ( DL & DL_METHOD_ENTER ) == DL_METHOD_ENTER ) {
         System.out.println( "\nChecking movability for figure: " + fig
               + "\n0123456789" );
      }
   }

   private void dEnter( int i, String descr, int[] b, int checkType ) {
      if ( ( DL & DL_METHOD_ENTER ) == DL_METHOD_ENTER ) {
         System.out.println( indent( i ) + descr + ": b="
               + BoardUtils.arrayToString( b ) + "(" + board.get3DName( b )
               + ")" + dCheck( checkType ) );
      }
   }

   private void dStop( int i, Figure f, int checkType ) {
      int[] b = f.getField().getView().getBoardPos();
      if ( ( DL & DL_TRACE_STOP ) == DL_TRACE_STOP ) {
         System.out.println( indent( i ) + "stop at a " + f.getFigureName()
               + " @ b=" + BoardUtils.arrayToString( b ) + "("
               + board.get3DName( b ) + ")" + dCheck( checkType ) );
      }
   }

   private void dStop( int i, int[] b, int checkType ) {
      if ( ( DL & DL_TRACE_STOP ) == DL_TRACE_STOP ) {
         System.out.println( indent( i ) + "stop at unavailable field b="
               + BoardUtils.arrayToString( b ) + "(" + board.get3DName( b )
               + ")" + dCheck( checkType ) );
      }
   }

   private void dStop( int i, int checkType ) {
      if ( ( DL & DL_TRACE_STOP ) == DL_TRACE_STOP ) {
         System.out.println( indent( i ) + "stop at range limit"
               + dCheck( checkType ) );
      }
   }

   private void dRecurse( int i ) {
      if ( ( DL & DL_RECURSE ) == DL_RECURSE ) {
         System.out.println( indent( i ) + "recursing" );
      }
   }

   private void dIgnoreZ( int i, int z ) {
      if ( ( DL & DL_IGNORE_Z ) == DL_IGNORE_Z ) {
         System.out.println( indent( i )
               + "ignoring unpresent attack board @ z=" + z );
      }
   }

   private String indent( int i ) {
      StringBuffer sb = new StringBuffer();
      for ( int j = i; j > 0; j-- ) {
         sb = sb.append( ' ' );
      }
      return sb.toString();
   }
}
