/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.model.trid;

import java.util.ArrayList;

import de.rowlo.tridchess.model.Board;
import de.rowlo.tridchess.model.Field;
import de.rowlo.tridchess.model.Figure;

/**
 * <p>
 * This is a representation of a Move as Properties entry. These entries do not
 * contain any information about the figures being moved.
 * </p>
 * <p>
 * Consider these Properties entries:<br>
 * <ul>
 * <li>move.1=figure-a5.2-c6.4</li>
 * <li>move.35=pin-QP1-KP2-false</li>
 * </ul>
 * One cannot tell which attack board or figure is being moved just from this
 * information. However, when saving a game the initial start formation is saved
 * and can be used to replay the move sequence. Step by step the incomplete move
 * information above can be filled up with the specific attack board and figure.
 * </p>
 * <p>
 * The PlainMove can create a Move from itself considering the current board
 * formation. Thus it serves as a temporary abstract move information container.
 * </p>
 * 
 * @author Robert Wloch
 * 
 */
public class PlainMove {
   /** Figure move type. */
   public static boolean FIGURE_MOVE = true;
   /** Pin move type. */
   public static boolean PIN_MOVE    = false;
   private boolean       moveType;
   private String        sourceName;
   private String        targetName;
   private boolean       modify;

   /**
    * Creates a plain figure move.
    * 
    * @param source
    *           3D name of the source field
    * @param target
    *           3D name of the target field
    */
   public PlainMove( String source, String target ) {
      setMoveType( PlainMove.FIGURE_MOVE );
      setModify( false );
      setSourceName( source );
      setTargetName( target );
   }

   /**
    * Creates a plain pin move.
    * 
    * @param source
    *           name of the source pin
    * @param target
    *           name of the target pin
    * @param modify
    *           true, if attack board is turned by 180 degrees
    */
   public PlainMove( String source, String target, boolean modify ) {
      setMoveType( PlainMove.PIN_MOVE );
      setModify( modify );
      setSourceName( source );
      setTargetName( target );
   }

   /**
    * Attempts to convert this PlainMove to a Move.
    * 
    * @return null, if the fields/pins are not usable by this PlainMove
    */
   public Move toMove( Board b ) {
      Move m = null;
      if ( isMoveType() == PlainMove.FIGURE_MOVE ) {
         m = toFigureMove( b );
      }
      else {
         m = toPinMove( b );
      }
      return m;
   }

   private Move toFigureMove( Board b ) {
      Move m = null;
      Field sourceField = b.getFieldsByName().get( getSourceName() );
      Field targetField = b.getFieldsByName().get( getTargetName() );
      if ( sourceField == null ) { return m; }
      Figure figure = sourceField.getFigure();
      if ( figure == null ) { return m; }
      Figure attacked = targetField.getFigure();
      m = new Move( figure, targetField, attacked );
      return m;
   }

   private Move toPinMove( Board b ) {
      Move m = null;
      ArrayList<Pin> list = b.getPinsList();
      Pin sourcePin = null;
      Pin targetPin = null;
      for ( Pin p : list ) {
         if ( p.getName().equals( getSourceName() ) ) {
            sourcePin = p;
         }
         else if ( p.getName().equals( getTargetName() ) ) {
            targetPin = p;
         }
      }
      if ( sourcePin != null && targetPin != null && sourcePin.canMove()
            && !targetPin.getView().isGhost() ) {
         m = new Move( sourcePin, targetPin, isModify() );
      }
      return m;
   }

   /**
    * @return the moveType
    */
   public boolean isMoveType() {
      return moveType;
   }

   /**
    * @param moveType
    *           the moveType to set
    */
   private void setMoveType( boolean moveType ) {
      this.moveType = moveType;
   }

   /**
    * @return the sourceName
    */
   public String getSourceName() {
      return sourceName;
   }

   /**
    * @param sourceName
    *           the sourceName to set
    */
   private void setSourceName( String sourceName ) {
      this.sourceName = sourceName;
   }

   /**
    * @return the targetName
    */
   public String getTargetName() {
      return targetName;
   }

   /**
    * @param targetName
    *           the targetName to set
    */
   private void setTargetName( String targetName ) {
      this.targetName = targetName;
   }

   /**
    * @return the modify
    */
   public boolean isModify() {
      return modify;
   }

   /**
    * @param modify
    *           the modify to set
    */
   private void setModify( boolean modify ) {
      this.modify = modify;
   }

   @Override
   public String toString() {
      StringBuffer sb = new StringBuffer();
      sb = sb.append( ( isMoveType() == PlainMove.FIGURE_MOVE ) ? "figure"
            : "pin" );
      sb = sb.append( " move(" + getSourceName() + ", " + getTargetName() );
      sb = sb.append( ( isMoveType() == PlainMove.FIGURE_MOVE ) ? ""
            : ", turned" );
      sb = sb.append( ")" );
      return sb.toString();
   }

}
