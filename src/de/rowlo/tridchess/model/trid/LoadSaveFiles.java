/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.model.trid;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import de.rowlo.tridchess.model.Board;
import de.rowlo.tridchess.util.StringUtils;

/**
 * Saves and loads Tri-D chess games.
 * 
 * @author Robert Wloch
 */
public class LoadSaveFiles {
   private Game game = null;

   /**
    * Creates a new game saver/loader.
    * 
    * @param game
    *           the Game for loading into and saving from.
    */
   public LoadSaveFiles( Game game ) {
      setGame( game );
   }

   /**
    * Saves the current game.
    */
   public void saveGame() {
      if ( !getGame().isRunning() ) return;
      // choose file dialog first
      String path = "save/";
      File sf = showSaveDialog( path, ".tridmatch",
            "Tri-D chess matches (*.tridmatch)" );
      if ( sf == null ) { return; }
      String filePath = sf.getAbsolutePath();
      if ( !filePath.endsWith( ".tridmatch" ) ) filePath = filePath
            + ".tridmatch";
      sf.getParentFile().mkdirs();
      String basePath = sf.getParent();
      if ( !basePath.endsWith( File.separator ) ) {
         basePath = basePath + File.separator;
      }

      // save start formation
      String formationFilePath = saveFormation( false, getGame()
            .getStartFormationAsProperties(), basePath );
      if ( formationFilePath == null ) {
         System.err
               .println( "Game not saved because saving the formation failed!" );
         return;
      }

      // save moves
      String moveLogFilePath = saveMoveLog( false, basePath );
      if ( moveLogFilePath == null ) {
         System.err
               .println( "Game not saved because saving the move log failed!" );
         File f = new File( formationFilePath );
         if ( f.exists() ) {
            f.delete();
         }
         return;
      }

      // zip and save
      String[] fileNames = new String[] { formationFilePath, moveLogFilePath };
      byte[] buf = new byte[2048];

      try {
         File f = new File( path );
         f.mkdirs();
         // Create the ZIP file
         ZipOutputStream out = new ZipOutputStream( new FileOutputStream(
               filePath ) );

         // Compress the files
         for ( int i = 0; i < fileNames.length; i++ ) {
            FileInputStream in = new FileInputStream( fileNames[i] );

            // Add ZIP entry to output stream.
            String entryName = StringUtils.skipMatch( fileNames[i], basePath );
            ZipEntry zipEntry = new ZipEntry( entryName );
            out.putNextEntry( zipEntry );

            // Transfer bytes from the file to the ZIP file
            int len;
            while ( ( len = in.read( buf ) ) > 0 ) {
               out.write( buf, 0, len );
            }

            // Complete the entry
            out.closeEntry();
            in.close();
         }

         // Complete the ZIP file
         out.close();
      }
      catch ( IOException e ) {
         File f = new File( filePath );
         if ( f.exists() ) {
            f.delete();
         }
      }

      // delete created files
      File f = new File( formationFilePath );
      if ( f.exists() ) {
         f.delete();
      }
      f = new File( moveLogFilePath );
      if ( f.exists() ) {
         f.delete();
      }
   }

   /**
    * Loads a game.
    */
   public void loadGame() {
      // choose file dialog first
      String path = "save/";
      File sf = showLoadDialog( path, ".tridmatch",
            "Tri-D chess matches (*.tridmatch)" );
      if ( sf == null ) { return; }
      String filePath = sf.getAbsolutePath();
      if ( !filePath.endsWith( ".tridmatch" ) ) filePath = filePath
            + ".tridmatch";
      String basePath = sf.getParent();
      if ( !basePath.endsWith( File.separator ) ) {
         basePath = basePath + File.separator;
      }

      // unzip and load
      String formationFilePath = null;
      String moveLogFilePath = null;

      try {
         // Open the ZIP file
         ZipInputStream in = new ZipInputStream( new FileInputStream( filePath ) );

         // Get the first entry
         ZipEntry entry = in.getNextEntry();
         while ( entry != null ) {
            String entryName = entry.getName();
            if ( entryName.endsWith( ".formation" ) ) {
               formationFilePath = basePath + entryName;
            }
            else if ( entryName.endsWith( ".movelog" ) ) {
               moveLogFilePath = basePath + entryName;
            }
            else {
               // ignore entry
               entry = in.getNextEntry();
               continue;
            }

            // Open the output file
            String outFilename = basePath + entryName;
            OutputStream out = new FileOutputStream( outFilename );

            // Transfer bytes from the ZIP file to the output file
            byte[] buf = new byte[2048];
            int len;
            while ( ( len = in.read( buf ) ) > 0 ) {
               out.write( buf, 0, len );
            }

            // Close the streams
            out.close();
            // ask for next ZipEntry
            entry = in.getNextEntry();
         }
         in.close();
      }
      catch ( IOException e ) {
         deleteFile( formationFilePath );
         deleteFile( moveLogFilePath );
      }

      // load formation
      Properties properties = loadProperties( formationFilePath );
      Map<String, Object> m = loadFormation( properties );
      // start the game without free move mode
      if ( m != null ) {
         getGame().startGame( m );
         if ( getGame().isFreeMoveMode() ) {
            getGame().toggleFreeMoveMode();
         }
      }

      // load move log
      loadMoveLog( moveLogFilePath );

      // delete created files
      deleteFile( formationFilePath );
      deleteFile( moveLogFilePath );
   }

   private void deleteFile( String filePath ) {
      File f = new File( filePath );
      if ( f.exists() ) {
         f.delete();
      }
   }

   /**
    * Saves the list of game Moves done so far to a file displaying a dialog to
    * choose a file name.
    */
   public String saveMoveLog() {
      return saveMoveLog( true, "save/" );
   }

   /**
    * Saves the list of game Moves done so far to a file with a default name
    * containing date and time. There will be no dialog for choosing a file
    * name.
    * 
    * @param displayDialog
    *           true to show file chooser dialog
    * @param path
    *           the base path where to save the file
    */
   public String saveMoveLog( boolean displayDialog, String path ) {
      Properties properties = getGame().currentMoveLogAsProperties();
      // don't save if no Properties available
      if ( properties == null ) return null;

      // save moves
      // leave this as default initialization for file names
      SimpleDateFormat dfm = (SimpleDateFormat) SimpleDateFormat
            .getDateTimeInstance();
      dfm.applyPattern( "yyyy-MM-dd_HH-mm" );
      String dateTime = dfm.format( new Date() );
      String filePath = path + "TriDMatch_" + dateTime + ".movelog";

      try {
         File f = new File( path );
         f.mkdirs();
         // file chooser dialog
         if ( displayDialog ) {
            File sf = showSaveDialog( path, ".movelog",
                  "chess board move logs (*.movelog)" );
            if ( sf == null ) { return null; }
            filePath = sf.getAbsolutePath();
            if ( !filePath.endsWith( ".movelog" ) ) filePath = filePath
                  + ".movelog";
            sf.getParentFile().mkdirs();
         }
         else {
            f = new File( filePath );
            filePath = f.getAbsolutePath();
         }
         // write file
         BufferedOutputStream bos = new BufferedOutputStream(
               new FileOutputStream( filePath ) );
         properties.store( bos, null );
      }
      catch ( FileNotFoundException e ) {
         e.printStackTrace();
         System.err.println( "ERROR: Cannot save formation: " + filePath );
      }
      catch ( IOException e ) {
         e.printStackTrace();
         System.err.println( "ERROR: Cannot save formation: " + filePath );
      }
      catch ( NullPointerException e ) {
         e.printStackTrace();
         System.err.println( "ERROR: Cannot save formation: " + filePath );
      }
      return filePath;
   }

   /**
    * <p>
    * Loads a move log from a properties file.
    * </p>
    * <p>
    * It is required that the current board's formation is the base of the move
    * sequence being loaded!
    * </p>
    * 
    * @param filePath
    *           path to the properties file
    */
   public void loadMoveLog( String filePath ) {
      Properties properties = loadProperties( filePath );
      Map<Integer, PlainMove> m = loadMoveLog( properties );
      Replayer replayer = new Replayer( m, getGame().getBoard() );
      replayer.start();
   }

   /**
    * Loads a move log from a Properties object.
    * 
    * @param properties
    *           move log as Properties
    */
   public Map<Integer, PlainMove> loadMoveLog( Properties properties ) {
      Map<Integer, PlainMove> moveSequence = new TreeMap<Integer, PlainMove>();
      // extract the sequence from the properties file
      if ( properties != null ) {
         Iterator<Object> it = properties.keySet().iterator();
         while ( it.hasNext() ) {
            String key = (String) it.next();
            String value = (String) properties.get( key );
            if ( !fillMoveSequence( key, value, moveSequence ) ) {
               System.err.println( "ERROR: Couldn't read move info: " + key
                     + "=" + value );
               return null;
            }
         }
      }
      return moveSequence;
   }

   private boolean fillMoveSequence( String moveKey, String moveValue,
         Map<Integer, PlainMove> moveSequence ) {
      // get move number
      Integer moveNumber = getMoveNumber( moveKey );
      if ( moveNumber == null ) { return false; }
      if ( moveSequence.containsKey( moveNumber ) ) {
         System.err
               .println( "ERROR: Move sequence contains move number already: "
                     + moveSequence.get( moveNumber ) + " (new: " + moveKey
                     + "=" + moveValue + ")" );
         return false;
      }
      // get plain move information
      PlainMove plainMove = getPlainMove( moveValue );
      if ( plainMove == null ) { return false; }
      moveSequence.put( moveNumber, plainMove );
      return true;
   }

   private Integer getMoveNumber( String moveKey ) {
      Integer number = null;
      try {
         number = Integer.parseInt( moveKey.substring( 5 ) );
      }
      catch ( NumberFormatException nfe ) {
         number = null;
      }
      return number;
   }

   private PlainMove getPlainMove( String moveValue ) {
      PlainMove plainMove = null;
      if ( moveValue.startsWith( "figure" ) ) {
         plainMove = toPlainFigureMove( moveValue.substring( 7 ) );
      }
      else if ( moveValue.startsWith( "pin" ) ) {
         plainMove = toPlainPinMove( moveValue.substring( 4 ) );
      }
      return plainMove;
   }

   private PlainMove toPlainFigureMove( String moveValue ) {
      PlainMove plainMove = null;
      String[] parts = moveValue.split( "-" );
      if ( parts.length == 2 ) {
         String source3dName = parts[0].replace( '.', ':' );
         String target3dName = parts[1].replace( '.', ':' );
         plainMove = new PlainMove( source3dName, target3dName );
      }
      return plainMove;
   }

   private PlainMove toPlainPinMove( String moveValue ) {
      PlainMove plainMove = null;
      String[] parts = moveValue.split( "-" );
      if ( parts.length == 3 ) {
         String sourceName = parts[0];
         String targetName = parts[1];
         boolean turn = Boolean.parseBoolean( parts[2] );
         plainMove = new PlainMove( sourceName, targetName, turn );
      }
      return plainMove;
   }

   /**
    * Saves the current formation to a file displaying a dialog to choose a file
    * name.
    */
   public String saveFormation() {
      Properties properties = getGame().currentFormationAsProperties();
      return saveFormation( true, properties, "save/" );
   }

   /**
    * Saves a formation to a file with a default name containing date and time.
    * There will be no dialog for choosing a file name.
    * 
    * @param displayDialog
    *           true to show file chooser dialog
    * @param properties
    *           formation to be saved as Properties object
    * @param path
    *           the base path where to save the file
    */
   public String saveFormation( boolean displayDialog, Properties properties,
         String path ) {
      // leave this as default initialization for file names
      SimpleDateFormat dfm = (SimpleDateFormat) SimpleDateFormat
            .getDateTimeInstance();
      dfm.applyPattern( "yyyy-MM-dd_HH-mm" );
      String dateTime = dfm.format( new Date() );
      String filePath = path + "TriDStart_" + dateTime + ".formation";

      // don't save if no Properties available
      if ( properties == null ) return null;

      // save figures
      try {
         File f = new File( path );
         f.mkdirs();
         // show dialog if desired
         if ( displayDialog ) {
            File sf = showSaveDialog( path, ".formation",
                  "chess board formations (*.formation)" );
            if ( sf == null ) { return null; }
            filePath = sf.getAbsolutePath();
            if ( !filePath.endsWith( ".formation" ) ) filePath = filePath
                  + ".formation";
            sf.getParentFile().mkdirs();
         }
         else {
            f = new File( filePath );
            filePath = f.getAbsolutePath();
         }
         // write file
         BufferedOutputStream bos = new BufferedOutputStream(
               new FileOutputStream( filePath ) );
         properties.store( bos, null );
      }
      catch ( FileNotFoundException e ) {
         e.printStackTrace();
         System.err.println( "ERROR: Cannot save formation: " + filePath );
      }
      catch ( IOException e ) {
         e.printStackTrace();
         System.err.println( "ERROR: Cannot save formation: " + filePath );
      }
      catch ( NullPointerException e ) {
         e.printStackTrace();
         System.err.println( "ERROR: Cannot save formation: " + filePath );
      }
      return filePath;
   }

   /**
    * Loads a formation from a properties file. Shows a dialog to choose the
    * file.
    */
   public void loadFormation() {
      String filePath = "de/rowlo/tridchess/config/TriDStart.formation";
      String path = "save/";

      // file chooser dialog
      File sf = showLoadDialog( path, ".formation",
            "chess board formations (*.formation)" );
      if ( sf == null ) { return; }
      filePath = sf.getAbsolutePath();
      if ( !filePath.endsWith( ".formation" ) ) filePath = filePath
            + ".formation";

      Map<String, Object> m = loadFormation( filePath );
      // start the game in free move mode
      if ( m != null ) {
         getGame().startGame( m );
         if ( !getGame().isFreeMoveMode() ) {
            getGame().toggleFreeMoveMode();
         }
      }
   }

   /**
    * Loads a formation from a Properties files.
    * 
    * @param filePath
    *           path to formation properties file
    */
   public Map<String, Object> loadFormation( String filePath ) {
      Properties properties = loadProperties( filePath );
      return loadFormation( properties );
   }

   /**
    * Loads a formation from a Properties object.
    * 
    * @param properties
    *           formation as Properties
    */
   public Map<String, Object> loadFormation( Properties properties ) {
      Map<String, Object> figuresByFieldNames = new HashMap<String, Object>();
      // extract the mapping from the properties file
      if ( properties != null ) {
         List<String> list = Arrays.asList( Board.figureImageFilenames );
         Iterator<Object> it = properties.keySet().iterator();
         while ( it.hasNext() ) {
            String key = (String) it.next();
            String value = (String) properties.get( key );
            if ( key.startsWith( "attackboard" ) ) {
               figuresByFieldNames.put( key, value );
            }
            else {
               value = value + ".png";
               int id = list.indexOf( value );
               if ( id >= 0 ) {
                  if ( !key.startsWith( "out" ) ) {
                     key = key.replace( '.', ':' );
                  }
                  figuresByFieldNames.put( key, id );
               }
               else {
                  System.err
                        .println( "Start position properties file contains errors! Using defaults." );
                  System.out.println( value + " is not in list:\n" + list );
                  figuresByFieldNames.clear();
                  break;
               }
            }
         }
      }
      return figuresByFieldNames;
   }

   /**
    * Loads a properties file.
    * 
    * @param fileName
    *           path with name of the properties file
    */
   public Properties loadProperties( String fileName ) {
      Properties properties = new Properties();
      URL url = ClassLoader.getSystemResource( fileName );
      if ( url == null ) {
         File f = new File( fileName );
         try {
            url = f.toURI().toURL();
         }
         catch ( MalformedURLException e ) {
            System.err.println( "ERROR: Cannot create URL from URI: "
                  + f.toURI() );
            return null;
         }
      }

      try {
         properties.load( url.openStream() );
      }
      catch ( FileNotFoundException e ) {
         System.out.println( "No formation configuration found for file: "
               + fileName + ". Using defaults." );
         e.printStackTrace();
         properties = null;
      }
      catch ( IOException e ) {
         System.out.println( "No formation configuration found for file: "
               + fileName + ". Using defaults." );
         e.printStackTrace();
         properties = null;
      }
      catch ( NullPointerException e ) {
         System.out.println( "No formation configuration found for file: "
               + fileName + ". Using defaults." );
         e.printStackTrace();
         properties = null;
      }
      return properties;
   }

   /**
    * Displays a file chooser dialog for saving files.
    * 
    * @param path
    *           default path the dialog will display
    * @return the file chosen or null, if aborted
    */
   protected File showSaveDialog( String path, String fileExtension,
         String extensionDescription ) {
      return showFileDialog( path, fileExtension, extensionDescription, false );
   }

   /**
    * Displays a file chooser dialog for loading files.
    * 
    * @param path
    *           default path the dialog will display
    * @return the file chosen or null, if aborted
    */
   protected File showLoadDialog( String path, String fileExtension,
         String extensionDescription ) {
      return showFileDialog( path, fileExtension, extensionDescription, true );
   }

   /**
    * Displays a file chooser dialog for saving files.
    * 
    * @param path
    *           default path the dialog will display
    * @return the file chosen or null, if aborted
    */
   protected File showFileDialog( String path, String fileExtension,
         String extensionDescription, boolean loading ) {
      JFileChooser fc = new JFileChooser( path );
      fc.setAcceptAllFileFilterUsed( false );
      FileFilter filter = new DefaultFileFilter( fileExtension,
            extensionDescription );
      fc.setFileFilter( filter );
      int result = ( loading ) ? fc.showOpenDialog( getGame().getBoard()
            .getView() ) : fc.showSaveDialog( getGame().getBoard().getView() );
      if ( result == JFileChooser.APPROVE_OPTION ) {
         return fc.getSelectedFile();
      }
      else {
         return null;
      }
   }

   /**
    * @return the game
    */
   public Game getGame() {
      return game;
   }

   /**
    * @param game
    *           the game to set
    * @throws NullPointerException
    *            if argument game is null.
    */
   protected void setGame( Game game ) throws NullPointerException {
      if ( game == null ) { throw new NullPointerException(
            "The argument game must not be null!" ); }
      this.game = game;
   }

   /**
    * A simple FileFilter.
    * 
    * @author robert.wloch
    * 
    */
   public class DefaultFileFilter extends FileFilter {
      private String fileExtension;
      private String extensionDescription;

      /**
       * Default construction.
       * 
       * @param extension
       * @param description
       */
      public DefaultFileFilter( String extension, String description ) {
         this.fileExtension = extension;
         this.extensionDescription = description;
      }

      @Override
      public boolean accept( File f ) {
         if ( f != null ) {
            if ( f.isDirectory() || f.getName().endsWith( fileExtension ) ) return true;
         }
         return false;
      }

      @Override
      public String getDescription() {
         return extensionDescription;
      }
   };
}
