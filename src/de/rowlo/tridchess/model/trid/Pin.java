/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.model.trid;

import java.awt.Point;
import java.util.ArrayList;

import de.rowlo.tridchess.gui.FieldView;
import de.rowlo.tridchess.gui.trid.PinView;
import de.rowlo.tridchess.model.Board;
import de.rowlo.tridchess.model.Field;
import de.rowlo.tridchess.model.Figure;
import de.rowlo.tridchess.util.BoardUtils;

/**
 * Pin description. This actually encompasses a whole attack board.
 * 
 * @author Robert Wloch
 * 
 */
public class Pin {
   /** The Pin's view. */
   protected PinView           view;
   /** The attack board's fields. */
   protected Field[][]         fields;
   /** The chess Board the Pin belongs to. */
   protected Board             board;
   /** The name of the Pin. */
   protected String            name;
   /** A list of pin names this attack board currently can be moved to. */
   protected ArrayList<String> targets = new ArrayList<String>();
   /** The source Pin's name of an attack board move. */
   protected String            source;
   /** Flag indicating the ownership. */
   protected boolean           ownerIsWhite;

   /**
    * @param n
    *           String; not null
    * @param v
    *           PinView; not null
    * @param af
    *           FieldView[2][2]; all 4 fields not null
    * @param b
    *           Board; not null
    */
   public Pin( String n, PinView v, Field[][] af, Board b ) {
      setName( n );
      setView( v );
      setBoard( b );
      setFields( af );
      if ( v.getBoardPos()[2] < 2 ) {
         ownerIsWhite = true;
      }
      else {
         ownerIsWhite = false;
      }
   }

   /**
    * @return the source Pin's name of an attack board move.
    */
   public String getSource() {
      return source;
   }

   /**
    * Moves the pin to the target pin.
    * 
    * @param target
    *           Pin
    * @param turn180
    *           true, if attack board is to be rotated by 180 degrees
    */
   public void movePin( Pin target, boolean turn180 ) {
      if ( target == null ) {
         System.err.println( "ERROR: Attempt to move pin to a null target!" );
         return;
      }
      if ( !target.getView().isGhost() ) {
         System.err.println( "ERROR: Target pin is occupied already: " // TODO: Why does it go here?
               + target.getName() );
         return;
      }

      target.getView().setGhost( false );
      getView().setGhost( true );

      target.setFields( getFields(), turn180 );
      setFields( null );

      target.setWhiteOwner( isWhiteOwner() );
   }

   /**
    * @param dest
    *           desired destination Pin's name
    * @return true, if the attack board can be moved to target Pin
    */
   public boolean canTarget( String dest ) {
      if ( targets != null && targets.contains( dest ) ) return true;
      return false;
   }

   /**
    * @param b
    */
   protected void createTargets( int[] b ) {
      targets.clear();
      if ( b == null || b.length != 3 ) return;

      // left/right neighbour
      int[] t1 = new int[3];
      t1[0] = ( b[0] > 4 ) ? b[0] - 4 : b[0] + 4;
      t1[1] = b[1];
      t1[2] = b[2];
      targets.add( BoardUtils.arrayToString( t1 ) );

      // front/back neighbour
      int[] t2 = new int[3];
      t2[0] = b[0];
      t2[1] = ( b[1] - b[2] == 0 ) ? b[1] + 4 : b[1] - 4;
      t2[2] = b[2];
      targets.add( BoardUtils.arrayToString( t2 ) );

      // diagonal neighbour
      int[] t3 = new int[3];
      t3[0] = ( b[0] > 4 ) ? b[0] - 4 : b[0] + 4;
      t3[1] = ( b[1] - b[2] == 0 ) ? b[1] + 4 : b[1] - 4;
      t3[2] = b[2];
      targets.add( BoardUtils.arrayToString( t3 ) );

      if ( b[2] > 0 ) {
         // lower neighbour
         int[] t4 = new int[3];
         t4[0] = b[0];
         ;
         t4[1] = b[1] - 2;
         t4[2] = b[2] - 2;
         targets.add( BoardUtils.arrayToString( t4 ) );
      }
      if ( b[2] < 4 ) {
         // upper neighbour
         int[] t5 = new int[3];
         t5[0] = b[0];
         ;
         t5[1] = b[1] + 2;
         t5[2] = b[2] + 2;
         targets.add( BoardUtils.arrayToString( t5 ) );
      }
   }

   /**
    * @return targets
    */
   public ArrayList<String> getTargets() {
      return targets;
   }

   /**
    * @param targets
    *           Festzulegender targets
    */
   public void setTargets( ArrayList<String> targets ) {
      this.targets = targets;
   }

   /**
    * @return Board
    */
   public Board getBoard() {
      return board;
   }

   /**
    * @param board
    *           Board
    */
   public void setBoard( Board board ) {
      this.board = board;
   }

   /**
    * @return PinView
    */
   public PinView getView() {
      return view;
   }

   /**
    * @param pinView
    *           PinView
    */
   public void setView( PinView pinView ) {
      this.view = pinView;
      source = BoardUtils.arrayToString( view.getBoardPos() );
      createTargets( view.getBoardPos() );
   }

   /**
    * @return Name
    */
   public String getName() {
      return name;
   }

   /**
    * @param name
    *           String
    */
   public void setName( String name ) {
      this.name = name;
   }

   /**
    * @return Field[2][2]
    */
   public Field[][] getFields() {
      return fields;
   }

   /**
    * @param fields
    *           Field[2][2]
    */
   public void setFields( Field[][] fields ) {
      setFields( fields, false );
   }

   /**
    * @param fields
    *           Field[2][2]
    * @param turn180
    *           true to turn attack board by 180 degrees
    */
   public void setFields( Field[][] fields, boolean turn180 ) {
      this.fields = fields;
      // update board positions of pin's fields
      if ( fields != null && view != null ) {
         for ( int y = 0; y < 2; y++ ) {
            for ( int x = 0; x < 2; x++ ) {
               int tx = x;
               int ty = y;
               Field f = fields[tx][ty];
               if ( f == null ) continue;

               FieldView fv = f.getView();

               // need 180 degree turn?
               if ( turn180 ) {
                  tx = 1 - tx;
                  ty = 1 - ty;
               }

               // update board positions
               int[] fb = fv.getBoardPos();
               fb[0] = view.getBoardPos()[0] - 1 + tx;
               fb[1] = view.getBoardPos()[1] + 1 - ty;
               fb[2] = view.getBoardPos()[2] + 1;
               fv.setBoardPos( fb );

               // update grid positions
               Point fg = fv.getGridPos();
               fg = new Point( view.getGridPos() );
               if ( view.getGridPos().x == 2 ) { // queens' side
                  fg.setLocation( fg.x - 2 + tx, fg.y + ty );
               }
               else { // kings' side
                  fg.setLocation( fg.x + 1 + tx, fg.y + ty );
               }
               fv.setGridPos( fg );

               // update field name
               // board.getFieldsByName().remove( f.get3DName() );
               f.setName( board.getView().getColNames()[fg.x]
                     + board.getView().getRowNames()[fg.y] );
               // board.getFieldsByName().put( f.get3DName(), f );
            } // for: x
         } // for: y
         getBoard().recreateFieldsByName();
      }
   }

   /**
    * Returns permanent owner of the attack board.
    * 
    * @return ownerIsWhite
    */
   public boolean isWhiteOwner() {
      return ownerIsWhite;
   }

   /**
    * @param ownerIsWhite
    *           boolean
    */
   public void setWhiteOwner( boolean ownerIsWhite ) {
      this.ownerIsWhite = ownerIsWhite;
   }

   /**
    * Returns true, if the attack board's fields are occupied by at most one
    * pawn.
    * 
    * @return true, if Pin is movable
    */
   public boolean canMove() {
      if ( fields == null ) return false;
      int figureValue = 0;
      for ( int y = 0; y < 2; y++ ) {
         for ( int x = 0; x < 2; x++ ) {
            Figure f = fields[x][y].getFigure();
            if ( f == null ) continue;
            figureValue += f.getType();
         }
      }
      if ( figureValue <= Figure.PAWN ) { return true; }
      return false;
   }

   /**
    * Returns the current owner of the attack board. Attack boards can be
    * temporarily captured by opposing party, if occupied with an opposing pawn
    * only.
    * 
    * @return true, if white party is current owner
    */
   public boolean isWhiteCurrentOwner() {
      // if board can move, determine current owner
      if ( canMove() && fields != null ) {
         // can move means, only one pawn at most is on the attack board
         for ( int y = 0; y < 2; y++ ) {
            for ( int x = 0; x < 2; x++ ) {
               Figure f = fields[x][y].getFigure();
               if ( f != null && f.getType() == Figure.PAWN ) { return f
                     .isWhite(); }
            }
         }
      }
      return isWhiteOwner();
   }

   @Override
   public String toString() {
      StringBuffer sb = new StringBuffer();

      sb = sb.append( "Pin: " );
      sb = sb.append( getName() ).append( " (" );
      sb = sb.append( BoardUtils.arrayToString( getView().getBoardPos() ) )
            .append( ")" );

      return sb.toString();
   }
}
