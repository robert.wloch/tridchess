/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.model.trid;

import de.rowlo.tridchess.model.Board;
import de.rowlo.tridchess.model.Field;
import de.rowlo.tridchess.model.Figure;

/**
 * Description of Figure or Pin moves in a Tri-D chess match.
 * 
 * @author Robert Wloch
 * 
 */
public class Move {
   private boolean moveCarriedOut = false;
   private boolean putCheck       = false;
   private boolean putMate        = false;
   private String  moveString     = null;

   private boolean figureMove     = true;
   private Board   board          = null;
   private Figure  figure         = null;
   private Field   sourceField    = null;
   private Field   targetField    = null;
   private Figure  attacked       = null;
   private Pin     sourcePin      = null;
   private Pin     targetPin      = null;
   private boolean turned         = false;

   /**
    * Creates a figure move object.
    * 
    * @param figure
    *           the Figure that is moved
    * @param target
    *           the Field the move will end at
    * @param attacked
    *           the Figure being attacked, or null
    */
   public Move( Figure figure, Field target, Figure attacked ) {
      setMoveCarriedOut( false );
      setPutCheck( false );
      setPutMate( false );
      setFigureMove( true );
      setFigure( figure );
      setSourceField( figure.getField() );
      setTargetField( target );
      setAttacked( attacked );
      setBoard( figure.getBoard() );
   }

   /**
    * Creates an attack board move object.
    * 
    * @param source
    *           the source Pin
    * @param target
    *           the destination Pin
    * @param turned
    *           true, if the attack board is also turned 180°
    */
   public Move( Pin source, Pin target, boolean turned ) {
      setMoveCarriedOut( false );
      setPutCheck( false );
      setPutMate( false );
      setFigureMove( false );
      setSourcePin( source );
      setTargetPin( target );
      setTurned( turned );
      setBoard( source.getBoard() );
   }

   /**
    * Carries out the move on the Board.
    */
   public void carryOutMove() {
      // prevent repeating this move
      if ( isMoveCarriedOut() ) return;
      if ( isFigureMove() ) {
         // Figure specific move actions
         getFigure().setField( getTargetField() );
      }
      else {
         // Pin specific move actions
         getSourcePin().movePin( getTargetPin(), isTurned() );
      }
      // update Figure ranges
      getBoard().refreshFiguresStates();
      // log the move in the game's log
      getBoard().getGame().logMove( this );
      // switch turn to other player
      getBoard().getGame().setWhitePlayingParty(
            !getBoard().getGame().isWhitePlayingParty() );
      // mark this move as carried out
      setMoveCarriedOut( true );
   }

   @Override
   public String toString() {
      if ( moveString != null ) { return moveString; }
      StringBuffer sb = new StringBuffer();
      boolean whiteHasTurn = true;
      // general move info for figure or pin
      if ( isFigureMove() ) {
         sb = sb.append( getSourceField().get3DName() );
         if ( getAttacked() != null ) {
            sb = sb.append( " x " );
         }
         else {
            sb = sb.append( "   " );
         }
         sb = sb.append( getTargetField().get3DName() );
         whiteHasTurn = getFigure().isWhite();
      }
      else {
         sb = sb.append( getSourcePin().getName() );
         if ( isTurned() ) {
            sb = sb.append( " ° " );
         }
         else {
            sb = sb.append( "   " );
         }
         if (null != getTargetPin()) {
            sb = sb.append( getTargetPin().getName() );
            whiteHasTurn = getTargetPin().isWhiteCurrentOwner();
         }
      }
      // if opponent is check or mate add symbols
      Figure king = ( whiteHasTurn ) ? getBoard().getBlackKing() : getBoard()
            .getWhiteKing();
      if ( king.isCheck() ) {
         sb = sb.append( " !" );
      }
      if ( king.isMate() ) {
         sb = sb.append( "#" );
      }
      if ( isMoveCarriedOut() ) {
         moveString = sb.toString();
      }

      return sb.toString();
   }

   /**
    * @return an String representation of this Move for Properties files.
    */
   public String toPropertyString() {
      StringBuffer sb = new StringBuffer();
      if ( isFigureMove() ) {
         sb = sb.append( "figure-" );
         sb = sb.append( getSourceField().get3DName().replace( ':', '.' ) );
         sb = sb.append( "-" );
         sb = sb.append( getTargetField().get3DName().replace( ':', '.' ) );
      }
      else {
         sb = sb.append( "pin-" );
         sb = sb.append( getSourcePin().getName() );
         sb = sb.append( "-" );
         sb = sb.append( getTargetPin().getName() );
         sb = sb.append( "-" );
         sb = sb.append( isTurned() );
      }
      return sb.toString();
   }

   /**
    * @return the moveCarriedOut
    */
   public boolean isMoveCarriedOut() {
      return moveCarriedOut;
   }

   /**
    * @param moveCarriedOut
    *           the moveCarriedOut to set
    */
   private void setMoveCarriedOut( boolean moveCarriedOut ) {
      this.moveCarriedOut = moveCarriedOut;
      // force moveString to be set
      if ( isMoveCarriedOut() ) {
         toString();
      }
   }

   /**
    * @return the putCheck
    */
   public boolean isPutCheck() {
      return putCheck;
   }

   /**
    * @param putCheck
    *           the putCheck to set
    */
   private void setPutCheck( boolean putCheck ) {
      this.putCheck = putCheck;
   }

   /**
    * @return the putMate
    */
   public boolean isPutMate() {
      return putMate;
   }

   /**
    * @param putMate
    *           the putMate to set
    */
   private void setPutMate( boolean putMate ) {
      this.putMate = putMate;
   }

   /**
    * @return the board
    */
   public Board getBoard() {
      return board;
   }

   /**
    * @param board
    *           the board to set; NOT null!
    * @throws NullPointerException
    *            if board is null.
    */
   private void setBoard( Board board ) throws NullPointerException {
      if ( board == null ) { throw new NullPointerException(
            "Argument board must not be null!" ); }
      this.board = board;
   }

   /**
    * @return the figureMove
    */
   public boolean isFigureMove() {
      return figureMove;
   }

   /**
    * @param figureMove
    *           the figureMove to set
    */
   private void setFigureMove( boolean figureMove ) {
      this.figureMove = figureMove;
   }

   /**
    * @return the figure
    */
   public Figure getFigure() {
      return figure;
   }

   /**
    * @param figure
    *           the figure to set
    */
   private void setFigure( Figure figure ) {
      this.figure = figure;
   }

   /**
    * @return the sourceField
    */
   public Field getSourceField() {
      return sourceField;
   }

   /**
    * @param sourceField
    *           the sourceField to set
    */
   private void setSourceField( Field sourceField ) {
      this.sourceField = sourceField;
   }

   /**
    * @return the targetField
    */
   public Field getTargetField() {
      return targetField;
   }

   /**
    * @param targetField
    *           the targetField to set
    */
   private void setTargetField( Field targetField ) {
      this.targetField = targetField;
   }

   /**
    * @return the attacked
    */
   public Figure getAttacked() {
      return attacked;
   }

   /**
    * @param attacked
    *           the attacked to set
    */
   private void setAttacked( Figure attacked ) {
      this.attacked = attacked;
   }

   /**
    * @return the sourcePin
    */
   public Pin getSourcePin() {
      return sourcePin;
   }

   /**
    * @param sourcePin
    *           the sourcePin to set
    */
   private void setSourcePin( Pin sourcePin ) {
      this.sourcePin = sourcePin;
   }

   /**
    * @return the targetPin
    */
   public Pin getTargetPin() {
      return targetPin;
   }

   /**
    * @param targetPin
    *           the targetPin to set
    */
   private void setTargetPin( Pin targetPin ) {
      this.targetPin = targetPin;
   }

   /**
    * @return the turned
    */
   public boolean isTurned() {
      return turned;
   }

   /**
    * @param turned
    *           the turned to set
    */
   private void setTurned( boolean turned ) {
      this.turned = turned;
   }
}
