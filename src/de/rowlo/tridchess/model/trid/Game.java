/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.model.trid;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import de.rowlo.tridchess.model.Board;
import de.rowlo.tridchess.model.Field;
import de.rowlo.tridchess.model.Figure;

/**
 * Description of a chess game.
 * 
 * @author robert.wloch
 * 
 */
public class Game {
   private Board         board                      = null;

   private boolean       running                    = false;
   private Properties    startFormationAsProperties = null;
   private boolean       isWhitePlayingParty;
   private boolean       freeMoveMode;
   private LoadSaveFiles loader                     = null;

   /**
    * Creates a chess game on a board.
    */
   public Game() {
      setBoard( new TriDBoard( this ) );
   }

   /**
    * Starts a game on the board. Causes all figures to be reset to their
    * initial start position.
    */
   public void startGame() {
      startGame( null );
   }

   /**
    * Starts a game on the board. Causes all figures to be reset to their
    * initial start position.
    * 
    * @param figuresStartMap
    *           null, to use default start formation, otherwise map of
    *           formation.
    */
   public void startGame( Map<String, Object> figuresStartMap ) {
      running = false;
      createFigures( figuresStartMap );
      getBoard().getLogView().getMoveLog().clear();
      getBoard().getLogView().redraw();

      if ( getBoard().getBlackKing() != null
            && getBoard().getWhiteKing() != null ) {
         setFreeMoveMode( false );
         setWhitePlayingParty( true );
         getBoard().refreshFiguresStates();
         running = true;
         startFormationAsProperties = currentFormationAsProperties();
      }
      getBoard().getView().redraw();
   }

   /**
    * Creates all black and white party's figures at their initial start
    * positions. This will also move attack boards to their start positions
    * first.
    */
   protected void createFigures() {
      createFigures( null );
   }

   /**
    * Creates all black and white party's figures at the start positions
    * provided by the Map. This may also move attack boards to other positions
    * first.
    * 
    * @param figuresStartMap
    *           null, to use default start formation, otherwise map of
    *           formation.
    */
   protected void createFigures( Map<String, Object> figuresStartMap ) {
      getBoard().clearEverything();
      getBoard().getAttackedFiguresView().redraw();
      Map<String, Object> figuresByStartFieldNames = figuresStartMap;
      // create/load default start formation
      if ( figuresByStartFieldNames == null
            || figuresByStartFieldNames.isEmpty() ) {
         figuresByStartFieldNames = getBoard().createBoardFigures();
      }
      if ( figuresByStartFieldNames != null ) {
         // move pins to initial start positions
         Iterator<String> it = figuresByStartFieldNames.keySet().iterator();
         ArrayList<Pin> setPins = new ArrayList<Pin>();
         int counter = 0;
         while ( it.hasNext() ) {
            String name = (String) it.next();
            if ( !name.startsWith( "attackboard" ) ) continue;
            String[] parts = name.split( "\\." );

            if ( parts.length != 3
                  || !( "white".equals( parts[1] ) || "black".equals( parts[1] ) ) ) {
               System.err
                     .println( "Warning: Start formation config might be broken at key: "
                           + name );
               continue;
            }
            String value = (String) figuresByStartFieldNames.get( name );
            placePin( value, ( "white".equals( parts[1] ) ) ? true : false,
                  setPins );
            counter++;
         }
         if ( counter != 4 ) {
            System.err
                  .println( "Warning: Start formation config doesn't set 4 attack boards." );
         }

         // set figures
         it = figuresByStartFieldNames.keySet().iterator();
         while ( it.hasNext() ) {
            String name = (String) it.next();
            if ( name.startsWith( "attackboard" ) ) continue;
            int figure = ( (Integer) figuresByStartFieldNames.get( name ) )
                  .intValue();
            Field f = getBoard().getFieldsByName().get( name );
            if ( f != null ) {
               if ( figure > Board.KING_WHITE ) {
                  Figure fig = new Figure( false,
                        Board.getFigureTypes()[figure], Board
                              .getFigureImageFilenames()[figure], f, getBoard() );
                  fig.setImage( getBoard().getView().getImage( fig.getName() ) );
                  getBoard().getBlackFiguresList().add( fig );
                  if ( figure == Board.KING_BLACK ) {
                     getBoard().setBlackKing( fig );
                  }
               }
               else {
                  Figure fig = new Figure( true,
                        Board.getFigureTypes()[figure], Board
                              .getFigureImageFilenames()[figure], f, getBoard() );
                  fig.setImage( getBoard().getView().getImage( fig.getName() ) );
                  getBoard().getWhiteFiguresList().add( fig );
                  if ( figure == Board.KING_WHITE ) {
                     getBoard().setWhiteKing( fig );
                  }
               }
            }
            else {
               // no field found for name -> figure is out of play
               if ( name.startsWith( "out.black" ) ) {
                  Figure fig = new Figure( false,
                        Board.getFigureTypes()[figure], Board
                              .getFigureImageFilenames()[figure], null,
                        getBoard() );
                  fig.setImage( getBoard().getView().getImage( fig.getName() ) );
                  getBoard().placeFigureOutOfGame( fig );
               }
               else if ( name.startsWith( "out.white" ) ) {
                  Figure fig = new Figure( true,
                        Board.getFigureTypes()[figure], Board
                              .getFigureImageFilenames()[figure], null,
                        getBoard() );
                  fig.setImage( getBoard().getView().getImage( fig.getName() ) );
                  getBoard().placeFigureOutOfGame( fig );
               }
            }
         } // while: figures
      }
   }

   /**
    * Places an attack board to a pin
    * 
    * @param name
    *           name of the pin
    * @param white
    *           true, if white is the owner
    * @param setPins
    *           list of active pins
    */
   protected void placePin( String name, boolean white, ArrayList<Pin> setPins ) {
      int count = getBoard().getPinsList().size();
      Pin target = null;
      // try to find a pin that's already at the right position
      for ( int i = 0; i < count; i++ ) {
         Pin p = getBoard().getPinsList().get( i );
         if ( p.getName().equals( name ) ) {
            target = p;
            if ( !p.getView().isGhost() && p.isWhiteOwner() == white ) {
               setPins.add( p );
               return;
            }
         }
      }
      // not target pin found? ERROR and abort
      if ( target == null ) {
         System.err.println( "ERROR: No target pin found!" );
         return;
      }
      // the attack board is not at it's place yet
      // get next none ghost pin that was not set already and move it
      Pin source = null;
      for ( int i = 0; i < count; i++ ) {
         Pin p = getBoard().getPinsList().get( i );
         if ( p != target && !p.getView().isGhost() && !setPins.contains( p ) ) {
            source = p;
            break;
         }
      }
      // no free source pin found? ERROR and abort
      if ( source == null ) {
         System.err
               .println( "ERROR: No free pin found to move to target position!" );
         return;
      }
      source.movePin( target, false );
      setPins.add( target );
   }

   /**
    * Logs a move of a figure or an attack board in the match log.
    * 
    * @param move
    *           the Move to log
    */
   public void logMove( Move move ) {
      if ( !isFreeMoveMode() ) {
         getBoard().getLogView().addLogEntry( move );
      }
      else {
         System.out.println( move );
      }
   }

   /**
    * Creates a Properties object from the list of moves
    */
   protected Properties currentMoveLogAsProperties() {
      // don't save, if a game is not running
      if ( !isRunning() ) return null;
      Properties properties = new Properties();

      int i = 1;
      for ( Move m : getBoard().getLogView().getMoveLog() ) {
         String name = "move." + i;
         String value = m.toPropertyString();
         properties.put( name, value );
         i++;
      }
      return properties;
   }

   /**
    * @return the current Formation as a Properties object
    */
   public Properties currentFormationAsProperties() {
      // don't save, if a game is not running
      if ( !isRunning() ) return null;
      Properties properties = new Properties();

      // save attack boards
      int whitePinCounter = 0;
      int blackPinCounter = 0;
      int count = getBoard().getPinsList().size();
      for ( int i = 0; i < count; i++ ) {
         Pin p = getBoard().getPinsList().get( i );
         if ( p.getView().isGhost() ) continue;
         if ( p.isWhiteOwner() ) {
            whitePinCounter++;
            properties
                  .put( "attackboard.white." + whitePinCounter, p.getName() );
         }
         else {
            blackPinCounter++;
            properties
                  .put( "attackboard.black." + blackPinCounter, p.getName() );
         }
      }

      // save black and white figures out of play
      count = getBoard().getBlackAttackedFiguresList().size();
      for ( int i = 0; i < count; i++ ) {
         Figure fig = getBoard().getBlackAttackedFiguresList().get( i );
         String value = fig.getName().substring( 0, fig.getName().length() - 4 );
         properties.put( "out.black." + i, value );
      }
      count = getBoard().getWhiteAttackedFiguresList().size();
      for ( int i = 0; i < count; i++ ) {
         Figure fig = getBoard().getWhiteAttackedFiguresList().get( i );
         String value = fig.getName().substring( 0, fig.getName().length() - 4 );
         properties.put( "out.white." + i, value );
      }

      // save black and white figure in play
      count = getBoard().getBlackFiguresList().size();
      for ( int i = 0; i < count; i++ ) {
         Figure fig = getBoard().getBlackFiguresList().get( i );
         String value = fig.getName().substring( 0, fig.getName().length() - 4 );
         properties.put( fig.getField().get3DName().replace( ':', '.' ), value );
      }
      count = getBoard().getWhiteFiguresList().size();
      for ( int i = 0; i < count; i++ ) {
         Figure fig = getBoard().getWhiteFiguresList().get( i );
         String value = fig.getName().substring( 0, fig.getName().length() - 4 );
         properties.put( fig.getField().get3DName().replace( ':', '.' ), value );
      }
      return properties;
   }

   /**
    * Saves the current game. Does nothing if no game is running.
    */
   public void saveGame() {
      getLoader().saveGame();
   }

   /**
    * Loads a game. Will automatically start the Game and replay all moves.
    */
   public void loadGame() {
      getLoader().loadGame();
   }

   /**
    * Saves the current formation. Does nothing if no game is running.
    */
   public void saveFormation() {
      getLoader().saveFormation();
   }

   /**
    * Loads a formation. Will automatically start a Game with free move mode.
    */
   public void loadFormation() {
      getLoader().loadFormation();
   }

   /**
    * @return isWhitePlayingParty
    */
   public boolean isWhitePlayingParty() {
      return isWhitePlayingParty;
   }

   /**
    * @param isWhitePlayingParty
    *           boolean
    */
   public void setWhitePlayingParty( boolean isWhitePlayingParty ) {
      if ( isFreeMoveMode() ) {
         getBoard().getView().setHintText( "move any figure" );
      }
      else {
         this.isWhitePlayingParty = isWhitePlayingParty;
         if ( isWhitePlayingParty() ) {
            getBoard().getView().setHintText( "next move: white" );
         }
         else {
            getBoard().getView().setHintText( "next move: black" );
         }
      }
   }

   /**
    * Toggle free move mode. While in free move mode, any party can move as many
    * figures as wished.
    */
   public void toggleFreeMoveMode() {
      this.freeMoveMode = !this.freeMoveMode;
      if ( isFreeMoveMode() ) {
         getBoard().getView().setHintText( "move any figure" );
      }
      else {
         if ( isWhitePlayingParty() ) {
            getBoard().getView().setHintText( "next move: white" );
         }
         else {
            getBoard().getView().setHintText( "next move: black" );
         }
      }
      getBoard().getView().redraw();
   }

   /**
    * @return freeMoveMode
    */
   public boolean isFreeMoveMode() {
      return freeMoveMode;
   }

   /**
    * @param freeMoveMode
    *           boolean
    */
   public void setFreeMoveMode( boolean freeMoveMode ) {
      this.freeMoveMode = freeMoveMode;
   }

   /**
    * @return the running
    */
   public boolean isRunning() {
      return running;
   }

   /**
    * @param running
    *           boolean
    */
   protected void setRunning( boolean running ) {
      this.running = running;
   }

   /**
    * @return the board
    */
   public Board getBoard() {
      return board;
   }

   /**
    * @param board
    *           the board to set
    */
   protected void setBoard( Board board ) {
      this.board = board;
   }

   /**
    * @return the loader
    */
   public LoadSaveFiles getLoader() {
      if ( loader == null ) {
         loader = createLoader();
      }
      return loader;
   }

   /**
    * @return a loader for this game.
    */
   private LoadSaveFiles createLoader() {
      return new LoadSaveFiles( this );
   }

   /**
    * @return the startFormationAsProperties
    */
   public Properties getStartFormationAsProperties() {
      return startFormationAsProperties;
   }

}
