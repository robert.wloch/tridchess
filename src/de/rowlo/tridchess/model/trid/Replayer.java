/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.model.trid;

import java.util.Map;

import de.rowlo.tridchess.gui.ChessUi;
import de.rowlo.tridchess.model.Board;

/**
 * This Thread replays a move sequence on the board's current formation.
 * 
 * @author Robert Wloch
 * 
 */
public class Replayer extends Thread {
   private Map<Integer, PlainMove> sequence;
   private long                    delay = 0;
   private Board                   board;

   /**
    * @param m
    *           Move sequence
    */
   public Replayer( Map<Integer, PlainMove> m, Board b ) {
      sequence = m;
      board = b;
      try {
         delay = Long.parseLong( b.getView().getChessUi().getUiPropertyString(
               ChessUi.REPLAY_DELAY ) );
      }
      catch ( NumberFormatException nfe ) {
         delay = 500;
      }
   }

   @Override
   public void run() {
      if ( sequence != null ) {
         // replay the move sequence
         for ( Integer i : sequence.keySet() ) {
            PlainMove pm = sequence.get( i );
            if ( pm == null ) {
               System.err
                     .println( "ERROR: Iterating over move sequence with null move!" );
               return;
            }
            Move move = pm.toMove( board );
            if ( move == null ) {
               System.err
                     .println( "ERROR: Couldn't create Move from PlainMove: "
                           + pm );
               return;
            }
            move.carryOutMove();
            System.out.println( "Replayed move: " + move );
            move.getBoard().getView().redraw();

            if ( delay > 0 ) {
               try {
                  Thread.sleep( delay );
               }
               catch ( InterruptedException e ) {
               }
            }
         }
      }
   }
}
