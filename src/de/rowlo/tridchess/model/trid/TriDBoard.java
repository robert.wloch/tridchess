/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.model.trid;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.Map;

import javax.swing.JFrame;

import de.rowlo.tridchess.event.BoardMouseListener;
import de.rowlo.tridchess.gui.ChessUi;
import de.rowlo.tridchess.gui.FieldView;
import de.rowlo.tridchess.gui.trid.HelpScreen;
import de.rowlo.tridchess.gui.trid.PinView;
import de.rowlo.tridchess.gui.trid.TriDBoardView;
import de.rowlo.tridchess.model.Board;
import de.rowlo.tridchess.model.Field;
import de.rowlo.tridchess.model.Figure;
import de.rowlo.tridchess.util.BoardUtils;

/**
 * Tri-D chess board definition.
 * 
 * @author Robert Wloch
 * 
 */
public class TriDBoard extends Board {
   /** Default names. */
   public static final String[] columnNamesTriD = { "z", "a", "", "a", "b", "c", "d", "", "d", "e" };
   /** Default names. */
   public static final String[] rowNamesTriD = { "9", "8", "7", "6", "5", "4", "7", "6", "5", "4", "3", "2", "5", "4",
         "3", "2", "1", "0" };
   /** Default names. */
   public static final String[] columnNames = { "z", "a", "b", "c", "d", "e" };
   /** Default names. */
   public static final String[] rowNames = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

   private FigureRangeChecking rangeCheck;

   /**
    * Creates a Tri-D chess board.
    */
   public TriDBoard(Game game) {
      setGame(game);
      this.rangeCheck = new FigureRangeChecking(this);
      setView(new TriDBoardView(columnNamesTriD, rowNamesTriD, this));
      getView().setChessUi(new ChessUi());
      getView().setFieldWidth(35);
      getView().setFieldHeight(35);

      loadImages();
      createFields(); // sets size of the board's view
      // creation of log view and out of play figure list views
      // must occur after createFields()
      createLogView(new Dimension(180, getView().getSize().height), getView().getChessUi());
      createFigureListViews(new Dimension(300, getView().getSize().height), getView().getChessUi());
   }

   @Override
   public void generalInit() {
      super.generalInit();
      BoardMouseListener l = new BoardMouseListener(this);
      getAttackedFiguresView().addMouseMotionListener(l);
      getAttackedFiguresView().addMouseListener(l);
   }

   @Override
   protected void createFields() {
      Point ref = new Point(20, 20);
      Dimension size = new Dimension(getView().getFieldWidth(), getView().getFieldHeight());
      Dimension pinSize = new Dimension(getView().getFieldWidth(), getView().getFieldHeight() * 2);
      Dimension d = new Dimension(getView().getFieldWidth() * getView().getColNames().length + 40,
            getView().getFieldHeight() * getView().getRowNames().length + 40);
      getView().setPreferredSize(d);
      getView().setMinimumSize(d);
      getView().setSize(d);
      createGridFields(getView().getColNames().length, getView().getRowNames().length);

      // black's queen's level attack board
      int ox = 0;
      int oy = 0;
      int by = 9;
      int bx = 0;
      int[] b = BoardUtils.make3DPointArray(1, 8, 4); // position on board
      // (bottom left based)
      Point g = new Point(ox + 2, oy); // position on grid (top left based)
      PinView pv = new PinView(b, g, ref);
      pv.setUi(getView().getChessUi());
      pv.setSize(pinSize);
      Field[][] fields = new Field[2][2];
      for (int y = 0; y < 2; y++) {
         for (int x = 0; x < 2; x++) {
            boolean white = ((y * 10 + x) % 2 == 0);
            if (y % 2 == 1)
               white = !white;
            b = BoardUtils.make3DPointArray(bx + x, by - y, 5); // position on
            // board
            // (bottom
            // left based)
            g = new Point(x + ox, y + oy); // position on grid (top left
            // based)
            FieldView fv = new FieldView(b, g, ref, white);
            fv.setUi(getView().getChessUi());
            fv.setSize(size);
            Field f = new Field(getView().getColNames()[x + ox] + getView().getRowNames()[y + oy], fv, this);
            fields[x][y] = f;
            setGridField(x + ox, y + oy, f);
            getFieldsList().add(f);
            getFieldsByName().put(f.get3DName(), f);
         }
      }
      Pin p = new Pin("QP6", pv, fields, this);
      getPinsList().add(p);

      // black's king's level attack board
      ox = 8;
      oy = 0;
      bx = 4;
      by = 9;
      b = BoardUtils.make3DPointArray(5, 8, 4); // position on board (bottom
      // left based)
      g = new Point(ox - 1, oy); // position on grid (top left based)
      pv = new PinView(b, g, ref);
      pv.setUi(getView().getChessUi());
      pv.setSize(pinSize);
      fields = new Field[2][2];
      for (int y = 0; y < 2; y++) {
         for (int x = 0; x < 2; x++) {
            boolean white = ((y * 10 + x) % 2 == 0);
            if (y % 2 == 1)
               white = !white;
            b = BoardUtils.make3DPointArray(bx + x, by - y, 5); // position on
            // board
            // (bottom
            // left based)
            g = new Point(x + ox, y + oy); // position on grid (top left
            // based)
            FieldView fv = new FieldView(b, g, ref, white);
            fv.setUi(getView().getChessUi());
            fv.setSize(size);
            Field f = new Field(getView().getColNames()[x + ox] + getView().getRowNames()[y + oy], fv, this);
            fields[x][y] = f;
            setGridField(x + ox, y + oy, f);
            getFieldsList().add(f);
            getFieldsByName().put(f.get3DName(), f);
         }
      }
      p = new Pin("KP6", pv, fields, this);
      getPinsList().add(p);

      // white's queen's level attack board
      ox = 0;
      oy = 16;
      bx = 0;
      by = 1;
      b = BoardUtils.make3DPointArray(1, 0, 0); // position on board (bottom
      // left based)
      g = new Point(ox + 2, oy); // position on grid (top left based)
      pv = new PinView(b, g, ref);
      pv.setUi(getView().getChessUi());
      pv.setSize(pinSize);
      fields = new Field[2][2];
      for (int y = 0; y < 2; y++) {
         for (int x = 0; x < 2; x++) {
            boolean white = ((y * 10 + x) % 2 == 0);
            if (y % 2 == 1)
               white = !white;
            b = BoardUtils.make3DPointArray(bx + x, by - y, 1); // position on
            // board
            // (bottom
            // left based)
            g = new Point(x + ox, y + oy); // position on grid (top left
            // based)
            FieldView fv = new FieldView(b, g, ref, white);
            fv.setUi(getView().getChessUi());
            fv.setSize(size);
            Field f = new Field(getView().getColNames()[x + ox] + getView().getRowNames()[y + oy], fv, this);
            fields[x][y] = f;
            setGridField(x + ox, y + oy, f);
            getFieldsList().add(f);
            getFieldsByName().put(f.get3DName(), f);
         }
      }
      p = new Pin("QP1", pv, fields, this);
      getPinsList().add(p);

      // white's king's level attack board
      ox = 8;
      oy = 16;
      bx = 4;
      by = 1;
      b = BoardUtils.make3DPointArray(5, 0, 0); // position on board (bottom
      // left based)
      g = new Point(ox - 1, oy); // position on grid (top left based)
      pv = new PinView(b, g, ref);
      pv.setUi(getView().getChessUi());
      pv.setSize(pinSize);
      fields = new Field[2][2];
      for (int y = 0; y < 2; y++) {
         for (int x = 0; x < 2; x++) {
            boolean white = ((y * 10 + x) % 2 == 0);
            if (y % 2 == 1)
               white = !white;
            b = BoardUtils.make3DPointArray(bx + x, by - y, 1); // position on
            // board
            // (bottom
            // left based)
            g = new Point(x + ox, y + oy); // position on grid (top left
            // based)
            FieldView fv = new FieldView(b, g, ref, white);
            fv.setUi(getView().getChessUi());
            fv.setSize(size);
            Field f = new Field(getView().getColNames()[x + ox] + getView().getRowNames()[y + oy], fv, this);
            fields[x][y] = f;
            setGridField(x + ox, y + oy, f);
            getFieldsList().add(f);
            getFieldsByName().put(f.get3DName(), f);
         }
      }
      p = new Pin("KP1", pv, fields, this);
      getPinsList().add(p);

      // create 8 ghost pins without fields
      b = BoardUtils.make3DPointArray(1, 4, 4);
      getPinsList().add(createGhostPin("QP5", 2, 4, b, pinSize, ref));
      b = BoardUtils.make3DPointArray(1, 6, 2);
      getPinsList().add(createGhostPin("QP4", 2, 6, b, pinSize, ref));
      b = BoardUtils.make3DPointArray(1, 2, 2);
      getPinsList().add(createGhostPin("QP3", 2, 10, b, pinSize, ref));
      b = BoardUtils.make3DPointArray(1, 4, 0);
      getPinsList().add(createGhostPin("QP2", 2, 12, b, pinSize, ref));
      b = BoardUtils.make3DPointArray(5, 4, 4);
      getPinsList().add(createGhostPin("KP5", 7, 4, b, pinSize, ref));
      b = BoardUtils.make3DPointArray(5, 6, 2);
      getPinsList().add(createGhostPin("KP4", 7, 6, b, pinSize, ref));
      b = BoardUtils.make3DPointArray(5, 2, 2);
      getPinsList().add(createGhostPin("KP3", 7, 10, b, pinSize, ref));
      b = BoardUtils.make3DPointArray(5, 4, 0);
      getPinsList().add(createGhostPin("KP2", 7, 12, b, pinSize, ref));

      // black board
      ox = 3;
      oy = 1;
      bx = 1;
      by = 8;
      for (int y = 0; y < 4; y++) {
         for (int x = 0; x < 4; x++) {
            boolean white = ((y * 10 + x) % 2 == 0);
            if (y % 2 == 1)
               white = !white;
            b = BoardUtils.make3DPointArray(bx + x, by - y, 4); // position on
            // board
            // (bottom
            // left based)
            g = new Point(x + ox, y + oy); // position on grid (top left
            // based)
            FieldView fv = new FieldView(b, g, ref, white);
            fv.setUi(getView().getChessUi());
            fv.setSize(size);
            Field f = new Field(getView().getColNames()[x + ox] + getView().getRowNames()[y + oy], fv, this);
            setGridField(x + ox, y + oy, f);
            getFieldsList().add(f);
            getFieldsByName().put(f.get3DName(), f);
         }
      }

      // neutral board
      ox = 3;
      oy = 7;
      bx = 1;
      by = 6;
      for (int y = 0; y < 4; y++) {
         for (int x = 0; x < 4; x++) {
            boolean white = ((y * 10 + x) % 2 == 0);
            if (y % 2 == 1)
               white = !white;
            b = BoardUtils.make3DPointArray(bx + x, by - y, 2); // position on
            // board
            // (bottom
            // left based)
            g = new Point(x + ox, y + oy); // position on grid (top left
            // based)
            FieldView fv = new FieldView(b, g, ref, white);
            fv.setUi(getView().getChessUi());
            fv.setSize(size);
            Field f = new Field(getView().getColNames()[x + ox] + getView().getRowNames()[y + oy], fv, this);
            setGridField(x + ox, y + oy, f);
            getFieldsList().add(f);
            getFieldsByName().put(f.get3DName(), f);
         }
      }

      // white board
      ox = 3;
      oy = 13;
      bx = 1;
      by = 4;
      for (int y = 0; y < 4; y++) {
         for (int x = 0; x < 4; x++) {
            boolean white = ((y * 10 + x) % 2 == 0);
            if (y % 2 == 1)
               white = !white;
            b = BoardUtils.make3DPointArray(bx + x, by - y, 0); // position on
            // board
            // (bottom
            // left based)
            g = new Point(x + ox, y + oy); // position on grid (top left
            // based)
            FieldView fv = new FieldView(b, g, ref, white);
            fv.setUi(getView().getChessUi());
            fv.setSize(size);
            Field f = new Field(getView().getColNames()[x + ox] + getView().getRowNames()[y + oy], fv, this);
            setGridField(x + ox, y + oy, f);
            getFieldsList().add(f);
            getFieldsByName().put(f.get3DName(), f);
         }
      }

      // set new fields to field list
      getView().setGridFields(getGridFields());
   }

   /**
    * Creates an invisible Pin that is only highlighted if some other Pin is
    * selected.
    * 
    * @param gx
    *           x position on 2D view grid
    * @param gy
    *           y position on 2D view grid
    * @param b
    *           int[3] (x,y,z) position on board
    * @param pinSize
    *           size of pin in pixels, usually fieldWidth:fieldHeight*2
    * @param ref
    *           top left view position of view grid
    * @return Pin
    */
   protected Pin createGhostPin(String name, int gx, int gy, int[] b, Dimension pinSize, Point ref) {
      if (name == null)
         name = "";
      Point g = new Point(gx, gy); // position on grid (top left based)
      PinView pv = new PinView(b, g, ref, true);
      pv.setUi(getView().getChessUi());
      pv.setSize(pinSize);
      return new Pin(name, pv, null, this);
   }

   @Override
   public Map<String, Object> createBoardFigures() {
      Map<String, Object> figuresByFieldNames = getGame().getLoader()
            .loadFormation("de/rowlo/tridchess/config/TriDStart.formation");
      // load defaults, if map is empty
      if (figuresByFieldNames.isEmpty()) {
         figuresByFieldNames.put("attackboard.white.1", "QP1");
         figuresByFieldNames.put("attackboard.white.2", "KP1");
         figuresByFieldNames.put("attackboard.black.1", "QP6");
         figuresByFieldNames.put("attackboard.black.2", "KP6");
         figuresByFieldNames.put("z9:5", Integer.valueOf(Board.CASTLE_BLACK));
         figuresByFieldNames.put("a9:5", Integer.valueOf(Board.QUEEN_BLACK));
         figuresByFieldNames.put("a8:4", Integer.valueOf(Board.KNIGHT_BLACK));
         figuresByFieldNames.put("b8:4", Integer.valueOf(Board.BISHOP_BLACK));
         figuresByFieldNames.put("c8:4", Integer.valueOf(Board.BISHOP_BLACK));
         figuresByFieldNames.put("d8:4", Integer.valueOf(Board.KNIGHT_BLACK));
         figuresByFieldNames.put("d9:5", Integer.valueOf(Board.KING_BLACK));
         figuresByFieldNames.put("e9:5", Integer.valueOf(Board.CASTLE_BLACK));
         figuresByFieldNames.put("z8:5", Integer.valueOf(Board.PAWN_BLACK));
         figuresByFieldNames.put("a8:5", Integer.valueOf(Board.PAWN_BLACK));
         figuresByFieldNames.put("a7:4", Integer.valueOf(Board.PAWN_BLACK));
         figuresByFieldNames.put("b7:4", Integer.valueOf(Board.PAWN_BLACK));
         figuresByFieldNames.put("c7:4", Integer.valueOf(Board.PAWN_BLACK));
         figuresByFieldNames.put("d7:4", Integer.valueOf(Board.PAWN_BLACK));
         figuresByFieldNames.put("d8:5", Integer.valueOf(Board.PAWN_BLACK));
         figuresByFieldNames.put("e8:5", Integer.valueOf(Board.PAWN_BLACK));
         figuresByFieldNames.put("z0:1", Integer.valueOf(Board.CASTLE_WHITE));
         figuresByFieldNames.put("a0:1", Integer.valueOf(Board.QUEEN_WHITE));
         figuresByFieldNames.put("a1:0", Integer.valueOf(Board.KNIGHT_WHITE));
         figuresByFieldNames.put("b1:0", Integer.valueOf(Board.BISHOP_WHITE));
         figuresByFieldNames.put("c1:0", Integer.valueOf(Board.BISHOP_WHITE));
         figuresByFieldNames.put("d1:0", Integer.valueOf(Board.KNIGHT_WHITE));
         figuresByFieldNames.put("d0:1", Integer.valueOf(Board.KING_WHITE));
         figuresByFieldNames.put("e0:1", Integer.valueOf(Board.CASTLE_WHITE));
         figuresByFieldNames.put("z1:1", Integer.valueOf(Board.PAWN_WHITE));
         figuresByFieldNames.put("a1:1", Integer.valueOf(Board.PAWN_WHITE));
         figuresByFieldNames.put("a2:0", Integer.valueOf(Board.PAWN_WHITE));
         figuresByFieldNames.put("b2:0", Integer.valueOf(Board.PAWN_WHITE));
         figuresByFieldNames.put("c2:0", Integer.valueOf(Board.PAWN_WHITE));
         figuresByFieldNames.put("d2:0", Integer.valueOf(Board.PAWN_WHITE));
         figuresByFieldNames.put("d1:1", Integer.valueOf(Board.PAWN_WHITE));
         figuresByFieldNames.put("e1:1", Integer.valueOf(Board.PAWN_WHITE));
      }
      return figuresByFieldNames;
   }

   @Override
   public void refreshFiguresStates() {
      rangeCheck.refreshFiguresStates();
   }

   @Override
   protected void highlightFieldsForSelectedField(Field f) {
      Figure fig = f.getFigure();
      if (fig == null)
         return;
      fig.highlightFields();
   }

   @Override
   public String get2DName(int[] xy) {
      if (xy[0] < 0 || xy[0] >= columnNames.length || xy[1] < 0 || xy[1] >= rowNames.length)
         return "";

      return columnNames[xy[0]] + rowNames[xy[1]];
   }

   @Override
   public String get3DName(int[] xyz) {
      if (xyz[0] < 0 || xyz[0] >= columnNames.length || xyz[1] < 0 || xyz[1] >= rowNames.length || xyz[2] < 0
            || xyz[2] > 5)
         return "";

      return columnNames[xyz[0]] + rowNames[xyz[1]] + ":" + xyz[2];
   }

   //
   // TODO: Clean up e.getPoint() mess! The whole drag and drop code is
   // full of getPoint() calls, which return event source relative coordinates.
   // However, all calculations rely on screen coordinates. The getPoint()
   // misery is also the reason why dragging figures out of play or back to
   // play doesn't really work yet.
   // Any help on this is appreciated!
   //

   @Override
   public void processMouseMotion(MouseEvent e) {
      if (e.getSource() == getView()) {
         // find field under mouse cursor
         int count = getFieldsList().size();
         for (int i = 0; i < count; i++) {
            Field f = getFieldsList().get(i);
            FieldView fv = f.getView();
            if (fv.contains(e.getPoint())) {
               // is mouse over another field?
               if (f != getLastFieldUnderMouse()) {
                  // selected different field: clear fields' shades
                  int max = getFieldsList().size();
                  for (int m = 0; m < max; m++) {
                     getFieldsList().get(m).getView().setShade(FieldView.NO_SHADE);
                  }
                  // select current field on all z levels
                  setFieldShade(f, FieldView.SELECTED_SHADE);
                  highlightFieldsForSelectedField(f);
                  // have changes repainted
                  setLastFieldUnderMouse(f);
                  setLastPinUnderMouse(null);
                  getView().redraw();
               }
               return;
            }
         }
         // find pin under mouse cursor
         count = getPinsList().size();
         for (int i = 0; i < count; i++) {
            Pin p = getPinsList().get(i);
            PinView pv = p.getView();
            if (pv.contains(e.getPoint())) {
               if (p != getLastPinUnderMouse()) {
                  int max = getPinsList().size();
                  // clear all shades
                  for (int m = 0; m < max; m++) {
                     getPinsList().get(m).getView().setShade(PinView.NO_SHADE);
                  }

                  pv.setShade(PinView.SELECTED_SHADE);
                  shadeAllPins(p);
                  setLastFieldUnderMouse(null);
                  setLastPinUnderMouse(p);
                  getAttackedFiguresView().setDraggedFigure(null);
                  getAttackedFiguresView().setDraggedFigureLocation(null);
                  getAttackedFiguresView().redraw();
                  getView().redraw();
               }
               return;
            }
         }
      } else {
         // mouse motion not over board's view
         if (getGame().isFreeMoveMode()) {
            // check if mouse is over a figure that's out of play
            Figure fig = getAttackedFiguresView().getFigureForPoint(e.getPoint());
            if (fig != null) {
               getAttackedFiguresView().setDraggedFigure(fig);
               getAttackedFiguresView().redraw();
               return;
            }

            // mouse is not over a figure that's out of play
            setLastFieldUnderMouse(null);
            setLastPinUnderMouse(null);
            getAttackedFiguresView().setDraggedFigure(null);
            getAttackedFiguresView().setDraggedFigureLocation(null);
            getAttackedFiguresView().redraw();
            getView().redraw();
         }
      }
      // mouse is not over a board's field
      // if it was before, do a repaint with cleared shades
      if (getLastFieldUnderMouse() != null || getLastPinUnderMouse() != null) {
         // clear all shades
         int max = getFieldsList().size();
         for (int m = 0; m < max; m++) {
            getFieldsList().get(m).getView().setShade(FieldView.NO_SHADE);
         }
         max = getPinsList().size();
         for (int m = 0; m < max; m++) {
            getPinsList().get(m).getView().setShade(PinView.NO_SHADE);
         }
         setLastFieldUnderMouse(null);
         setLastPinUnderMouse(null);
         getView().redraw();
      }
   }

   /**
    * Does setFieldUnderMouse on success!
    * 
    * @param p
    *           Point
    * @return true, on success.
    */
   private boolean fieldForPoint(Point p) {
      int count = getFieldsList().size();
      boolean hit = false;
      for (int i = 0; i < count; i++) {
         Field f = getFieldsList().get(i);
         FieldView fv = f.getView();
         if (fv.contains(p) && f != getFieldUnderMouseDrag()) {
            setFieldUnderMouseDrag(f);
            hit = true;
            break;
         }
      }
      return hit;
   }

   /**
    * Does setPinUnderMouse on success!
    * 
    * @param p
    *           Point
    * @return true, on success.
    */
   private boolean pinForPoint(Point point) {
      int count = getPinsList().size();
      boolean isFreeMoveMode = getGame().isFreeMoveMode();
      boolean hit = false;
      for (int i = 0; i < count; i++) {
         Pin pin = getPinsList().get(i);
         PinView pinView = pin.getView();
         if (pinView.containsForAll(point) && (isFreeMoveMode || PinView.FORBIDDEN_SHADE != pinView.getShade())) {
            setPinUnderMouseDrag(pin);
            pin.getView().setShade(PinView.ALLOWED_SHADE);
            hit = true;
            break;
         }
      }
      return hit;
   }

   @Override
   public void processMouseDragStart(MouseEvent e) {
      // no dragging without a running game
      if (!getGame().isRunning())
         return;
      if (e.getSource() == getView()) {
         Field field = getLastFieldUnderMouse();
         // need to drag a figure?
         if (field != null) {
            if (field.getFigure() != null) {
               if (field.getFigure().isWhite() == getGame().isWhitePlayingParty() || getGame().isFreeMoveMode()) {
                  setFieldUnderMouseDrag(field);
                  setMouseDragPosition(e.getPoint());
                  getView().redraw();
               }
            }
            e.consume();
            return;
         }
         Pin pin = getLastPinUnderMouse();
         // need to drag a pin?
         if (pin != null) {
            if (pin.canMove() && pin.isWhiteCurrentOwner() == getGame().isWhitePlayingParty()
                  || getGame().isFreeMoveMode()) {
               setPinUnderMouseDrag(pin);
               setMouseDragPosition(e.getPoint());
               getView().setHintText("hold CTRL while dropping rotates 180°");
               getView().redraw();
               e.consume();
               return;
            }
         }
      } else {
         // source of event is figures out of play view
         if (getGame().isFreeMoveMode()) {
            Figure fig = getAttackedFiguresView().getDraggedFigure();
            // need to drag a figure that's out of play?
            if (fig != null) {
               getAttackedFiguresView().setDraggedFigureLocation(e.getPoint());
               setFieldUnderMouseDrag(null);
               setMouseDragPosition(e.getPoint());
               getView().redraw();
               getAttackedFiguresView().redraw();
               e.consume();
               return;
            }
         }
      }
   }

   @Override
   public void processMouseDrag(MouseEvent e) {
      // no dragging without a running game
      if (!getGame().isRunning())
         return;
      boolean isFreeMoveMode = getGame().isFreeMoveMode();
      if (e.getSource() == getView()) {
         Field field = getLastFieldUnderMouse();
         // need to drag a figure?
         if (field != null) {
            if (field.getFigure() != null) {
               if (field.getFigure().isWhite() == getGame().isWhitePlayingParty() || isFreeMoveMode) {
                  // find field under mouse cursor
                  boolean hit = fieldForPoint(e.getPoint()); // calls setFieldUnderMouseDrag()
                  if (!hit && isFreeMoveMode) {
                     // dragging over figures out of play view?
                     Rectangle r = new Rectangle(getAttackedFiguresView().getLocationOnScreen(),
                           getAttackedFiguresView().getSize());
                     if (r.contains(e.getPoint())) {
                        // need to continue dragging a figure out of play
                        setFieldUnderMouseDrag(null);
                        setMouseDragPosition(e.getPoint());
                        getAttackedFiguresView().setDraggedFigureLocation(e.getPoint());
                        getView().redraw();
                        getAttackedFiguresView().redraw();
                        e.consume();
                        return;
                     }
                  }
                  setMouseDragPosition(e.getPoint());
                  getView().redraw();
               }
            }
            e.consume();
            return;
         }
         Pin pin = getLastPinUnderMouse();
         // need to drag a pin?
         if (pin != null) {
            if (pin.canMove() && pin.isWhiteCurrentOwner() == getGame().isWhitePlayingParty()
                  || isFreeMoveMode) {
               shadeAllPins(pin); // reset shading
               // find pin under mouse cursor
               boolean hit = pinForPoint(e.getPoint()); // calls setPinUnderMouseDrag()
               if (!hit) {
                  setPinUnderMouseDrag(null);
               }
               setMouseDragPosition(e.getPoint());
               getView().redraw();
               e.consume();
               return;
            }
         }
      } else {
         // source of mouse event is not the board's view
         if (isFreeMoveMode) {
            Figure fig = getAttackedFiguresView().getDraggedFigure();
            // need to drag a figure that's out of play?
            if (fig != null) {
               setMouseDragPosition(e.getPoint());
               boolean hit = fieldForPoint(e.getPoint()); // calls setFieldUnderMouseDrag()
               System.out.println("over board's field? " + hit);
               getAttackedFiguresView().setDraggedFigureLocation(e.getPoint());
               getView().redraw();
               getAttackedFiguresView().redraw();
               e.consume();
               return;
            }
         }
      }
   }

   @Override
   public void processMouseDragEnd(MouseEvent e) {
      // no dragging without a running game
      if (!getGame().isRunning())
         return;
      processMouseDrag(e);
      boolean isFreeMoveMode = getGame().isFreeMoveMode();
      if (e.getSource() == getView()) {
         Field field = getLastFieldUnderMouse();
         // need to drop a figure?
         if (field != null) {
            if (field.getFigure() != null && getFieldUnderMouseDrag() != null) {
               if (field.getFigure().isWhite() == getGame().isWhitePlayingParty() || isFreeMoveMode) {
                  int s = getFieldUnderMouseDrag().getView().getShade();
                  if (s == FieldView.ALLOWED_SHADE || s == FieldView.ATTACK_SHADE || isFreeMoveMode) {
                     Figure attacked = getFieldUnderMouseDrag().getFigure();
                     // never remove kings from board
                     if (attacked == null || (attacked != null && attacked.getType() != Figure.KING)) {
                        // can drop here
                        Move move = new Move(field.getFigure(), getFieldUnderMouseDrag(), attacked);
                        move.carryOutMove();
                     }
                  }
               }

               setFieldUnderMouseDrag(null);
               setMouseDragPosition(null);
               getAttackedFiguresView().setDraggedFigure(null);
               getAttackedFiguresView().setDraggedFigureLocation(null);
               getAttackedFiguresView().redraw();
               getView().redraw();
            } else if (getFieldUnderMouseDrag() == null && field.getFigure() != null) {
               // check if figure needs to be put out of play
               Rectangle r = new Rectangle(getAttackedFiguresView().getLocationOnScreen(),
                     getAttackedFiguresView().getSize());
               if (r.contains(e.getPoint())) {
                  // put figure out of play
                  placeFigureOutOfGame(field.getFigure());
                  if (field.getFigure().isWhite() && !getWhiteAttackedFiguresList().contains(field.getFigure())) {
                     getWhiteAttackedFiguresList().add(field.getFigure());
                  } else if (!field.getFigure().isWhite()
                        && !getBlackAttackedFiguresList().contains(field.getFigure())) {
                     getBlackAttackedFiguresList().add(field.getFigure());
                  }

                  getAttackedFiguresView().setDraggedFigure(null);
                  getAttackedFiguresView().setDraggedFigureLocation(null);
                  setFieldUnderMouseDrag(null);
                  setMouseDragPosition(null);
                  getView().redraw();
                  getAttackedFiguresView().redraw();
               }
            }
            e.consume();
            return;
         }
         Pin pin = getLastPinUnderMouse();
         // need to drop a pin?
         if (pin != null) {
            if (pin.canMove() && pin.isWhiteCurrentOwner() == getGame().isWhitePlayingParty() || isFreeMoveMode) {
               Pin pinUnderMouse = getPinUnderMouseDrag();
               int shadeOfPinUnderMouse = pinUnderMouse != null ? pinUnderMouse.getView().getShade() : 0;
               if (PinView.ALLOWED_SHADE == shadeOfPinUnderMouse || isFreeMoveMode) {
                  // can drop here
                  boolean withCTRL = ((e.getModifiersEx() & MouseEvent.CTRL_DOWN_MASK) == MouseEvent.CTRL_DOWN_MASK);
                  Move move = new Move(pin, getPinUnderMouseDrag(), withCTRL);
                  move.carryOutMove();
               }
            }
            setPinUnderMouseDrag(null);
            setMouseDragPosition(null);
            getView().redraw();
            e.consume();
            return;
         }
      } else {
         // source of event is figures out of play view
         if (isFreeMoveMode) {
            Figure fig = getAttackedFiguresView().getDraggedFigure();
            // need to drop a figure that's out of play?
            if (fig != null) {
               // get figure back to play?
               Rectangle r = new Rectangle(getView().getLocationOnScreen(), getView().getSize());
               if (r.contains(e.getPoint()) && getFieldUnderMouseDrag() != null) {
                  // need to drop formerly attacked figure back on board
                  Figure attacked = getFieldUnderMouseDrag().getFigure();
                  // never remove kings from board
                  if (attacked == null || (attacked != null && attacked.getType() != Figure.KING)) {
                     // can drop here
                     fig.setInGame(true);
                     Move move = new Move(fig, getFieldUnderMouseDrag(), attacked);
                     getAttackedFiguresView().removeFigure(fig);
                     if (fig.isWhite()) {
                        getWhiteAttackedFiguresList().remove(fig);
                        getWhiteFiguresList().add(fig);
                     } else {
                        getBlackAttackedFiguresList().remove(fig);
                        getBlackFiguresList().add(fig);
                     }
                     move.carryOutMove();
                  }
               }
               // can't drop here
               getAttackedFiguresView().setDraggedFigure(null);
               getAttackedFiguresView().setDraggedFigureLocation(null);
               setFieldUnderMouseDrag(null);
               setMouseDragPosition(null);
               getView().redraw();
               getAttackedFiguresView().redraw();
               e.consume();
               return;
            }
         }
      }
   }

   /**
    * Resets all highlighting states of all pins and calculates new states based
    * on selected Pin.
    * 
    * @param selected
    *           Pin
    */
   protected void shadeAllPins(Pin selected) {
      if (selected.getFields() == null) {
         System.err.println("Warning: Ghost pin was selected!");
         return;
      }
      // pins can only be moved if at most one own pawn is on them
      // or if pin is owned by playing party
      if (!selected.canMove() && !getGame().isFreeMoveMode())
         return;
      if (!getGame().isFreeMoveMode() && selected.isWhiteCurrentOwner() != getGame().isWhitePlayingParty())
         return;

      int count = getPinsList().size();
      for (int i = 0; i < count; i++) {
         Pin p = getPinsList().get(i);
         if (p != selected) {
            if (p.getFields() != null) {
               // never can move attack board to a position another
               // attack board occupies already
               p.getView().setShade(PinView.FORBIDDEN_SHADE);
            } else {
               if (selected.canTarget(p.getSource())) {
                  p.getView().setShade(PinView.ALLOWED_SHADE);
               } else {
                  p.getView().setShade(PinView.FORBIDDEN_SHADE);
               }
            }
         }
      }
   }

   @Override
   protected void setFieldShade(Field field, int shade) {
      if (field == null)
         return;
      String name = field.getName();
      int count = getFieldsList().size();
      // clear all shades
      for (int i = 0; i < count; i++) {
         Field f = getFieldsList().get(i);
         if (name.equals(f.getName())) {
            f.getView().setShade(shade);
         }
      }
   }

   /**
    * Applies a shade to a field.
    * 
    * @param field
    * @param shade
    */
   protected void setFieldShade3D(Field field, int shade) {
      if (field == null)
         return;
      FieldView fv = field.getView();
      int s = fv.getShade();
      if (s != FieldView.NO_SHADE && s != FieldView.SELECTED_SHADE && s != shade) {
         System.err.println("Warning: Already applied different shade than " + fv.getShadeName(shade)
               + " to the field! " + field.toString());
      }
      fv.setShade(shade);
   }

   @Override
   public void displayHelpMessageBox(JFrame frame) {
      HelpScreen.displayHelpScreen(frame);
   }

}
