/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.model;

import de.rowlo.tridchess.gui.FieldView;
import de.rowlo.tridchess.util.BoardUtils;

/**
 * Field description.
 * 
 * @author Robert Wloch
 * 
 */
public class Field {
   /** The Field's view. */
   protected FieldView view;
   /** The chess Board. */
   protected Board     board;
   /** The name of the Field. */
   protected String    name;
   /** The Figure on the field or null. */
   protected Figure    figure;

   /**
    * @param n
    *           String; not null
    * @param f
    *           FieldView; not null
    * @param b
    *           Board; not null
    */
   public Field( String n, FieldView f, Board b ) {
      setName( n );
      setView( f );
      setBoard( b );
   }

   /**
    * @return Board
    */
   public Board getBoard() {
      return board;
   }

   /**
    * @param board
    *           Board
    */
   public void setBoard( Board board ) {
      this.board = board;
   }

   /**
    * @return Name
    */
   public String getName() {
      return this.name;
   }

   /**
    * @return Name
    */
   public String get3DName() {
      return this.name + ":" + view.getBoardPos()[2];
   }

   /**
    * @param n
    *           String
    */
   public void setName( String n ) {
      this.name = n;
   }

   /**
    * @return FieldView
    */
   public FieldView getView() {
      return view;
   }

   /**
    * @param view
    *           FieldView
    */
   public void setView( FieldView view ) {
      this.view = view;
   }

   /**
    * @return figure
    */
   public Figure getFigure() {
      return figure;
   }

   /**
    * Sets the figure of this field with logging the move. If the field
    * contained a figure it will be placed out of game.
    * 
    * @param figure
    *           Figure
    * @param logTheMove
    *           true, to log the move
    */
   public void setFigure( Field fromField, Figure figure, boolean logTheMove ) {
      setFigure( figure );
   }

   /**
    * Sets the figure of the field without logging.
    * 
    * @param figure
    *           Figure
    * @return the attacked Figure, if the Field was occupied, else null.
    */
   public Figure setFigure( Figure figure ) {
      Figure attackedFigure = null;
      if ( figure != null ) {
         if ( this.figure != null ) { // attack
            board.placeFigureOutOfGame( this.figure );
            attackedFigure = this.figure;
         }
      }
      this.figure = figure;
      return attackedFigure;
   }

   @Override
   public String toString() {
      StringBuffer sb = new StringBuffer();
      sb = sb.append( "Field " + getName() + " (" );
      sb = sb
            .append( "b=" + BoardUtils.arrayToString( getView().getBoardPos() ) );
      if ( figure != null ) {
         sb = sb.append( ", " + figure.getFigureName() );
      }
      sb = sb.append( ", " + getView().getShadeName() );
      sb = sb.append( ")" );
      return sb.toString();
   }

}
