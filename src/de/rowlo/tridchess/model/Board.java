/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.model;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import de.rowlo.tridchess.event.BoardMouseListener;
import de.rowlo.tridchess.gui.BoardView;
import de.rowlo.tridchess.gui.ChessUi;
import de.rowlo.tridchess.gui.FiguresListView;
import de.rowlo.tridchess.gui.LogView;
import de.rowlo.tridchess.model.trid.Game;
import de.rowlo.tridchess.model.trid.Pin;

/**
 * Abstract base class for chess boards. Can also be implemented by normal chess
 * boards.
 * 
 * @author Robert Wloch
 * 
 */
public abstract class Board {
   /** Figure ID. */
   public static final int      PAWN_WHITE           = 0;
   /** Figure ID. */
   public static final int      CASTLE_WHITE         = 1;
   /** Figure ID. */
   public static final int      KNIGHT_WHITE         = 2;
   /** Figure ID. */
   public static final int      BISHOP_WHITE         = 3;
   /** Figure ID. */
   public static final int      QUEEN_WHITE          = 4;
   /** Figure ID. */
   public static final int      KING_WHITE           = 5;
   /** Figure ID. */
   public static final int      PAWN_BLACK           = 6;
   /** Figure ID. */
   public static final int      CASTLE_BLACK         = 7;
   /** Figure ID. */
   public static final int      KNIGHT_BLACK         = 8;
   /** Figure ID. */
   public static final int      BISHOP_BLACK         = 9;
   /** Figure ID. */
   public static final int      QUEEN_BLACK          = 10;
   /** Figure ID. */
   public static final int      KING_BLACK           = 11;

   /** Figure types as array. */
   public static final int[]    figureTypes          = { Figure.PAWN,
         Figure.CASTLE, Figure.KNIGHT, Figure.BISHOP, Figure.QUEEN,
         Figure.KING, Figure.PAWN, Figure.CASTLE, Figure.KNIGHT, Figure.BISHOP,
         Figure.QUEEN, Figure.KING                  };

   /** Figure types' image file names. */
   public static final String[] figureImageFilenames = { "pawn_w.png",
         "castle_w.png", "knight_w.png", "bishop_w.png", "queen_w.png",
         "king_w.png", "pawn_b.png", "castle_b.png", "knight_b.png",
         "bishop_b.png", "queen_b.png", "king_b.png" };

   private BoardView            view;
   private LogView              logView;
   private FiguresListView      attackedFiguresView;
   private ArrayList<Field>     fieldsList           = new ArrayList<Field>();
   private Map<String, Field>   fieldsByName         = new HashMap<String, Field>();
   private ArrayList<Pin>       pinsList             = new ArrayList<Pin>();
   private ArrayList<Figure>    blackFiguresList     = new ArrayList<Figure>();
   private ArrayList<Figure>    whiteFiguresList     = new ArrayList<Figure>();
   private Field[][]            gridFields;
   private Field                lastFieldUnderMouse  = null;
   private Pin                  lastPinUnderMouse    = null;
   private Field                fieldUnderMouseDrag  = null;
   private Pin                  pinUnderMouseDrag    = null;
   private Point                mouseDragPosition    = null;
   private boolean              dragging;
   private Figure               whiteKing, blackKing;
   private Game                 game                 = null;

   /**
    * Must be invoked after the board's view received it's parent.
    */
   public void generalInit() {
      BoardMouseListener l = new BoardMouseListener( this );
      getView().addMouseMotionListener( l );
      getView().addMouseListener( l );
   }

   /**
    * Loads the figure image files from the images folder.
    */
   protected void loadImages() {
      int count = figureImageFilenames.length;
      String imagePath = "de/rowlo/tridchess/images";
      try {
         for ( int i = 0; i < count; i++ ) {
            URL url = ClassLoader.getSystemResource( imagePath + "/"
                  + figureImageFilenames[i] );
            BufferedImage bi = ImageIO.read( url );
            view.addFigureImage( figureImageFilenames[i], bi );
         }
      }
      catch ( IOException e ) {
         e.printStackTrace();
      }
   }

   /**
    * Creates the LogView.
    * 
    * @param size
    *           Dimension
    * @param ui
    *           ChessUi
    */
   protected void createLogView( Dimension size, ChessUi ui ) {
      this.logView = new LogView( size, ui );
   }

   /**
    * Creates the FigureListViews for black and white figures out of play.
    * 
    * @param size
    *           Dimension
    * @param ui
    *           ChessUi
    */
   protected void createFigureListViews( Dimension size, ChessUi ui ) {
      this.attackedFiguresView = new FiguresListView( size, ui );
   }

   /**
    * @return logView
    */
   public LogView getLogView() {
      return logView;
   }

   /**
    * @return a JComponent containing the logView
    */
   public JComponent getLogJComponent() {
      JScrollPane pane = new JScrollPane( logView );
      pane.setBorder( null );
      pane.setViewportBorder( null );
      return pane;
   }

   /**
    * @return the attackedFiguresView
    */
   public FiguresListView getAttackedFiguresView() {
      return attackedFiguresView;
   }

   /**
    * Creates the fields of a chess board. Must set the size of the board's
    * view.
    */
   protected abstract void createFields();

   /**
    * <p>
    * Creates a mapping of field names to figures for initial figure placement
    * on normal chess boards. The extention ":n" specifies the vertical position
    * of a field, which is not the same for all fields on Tri-D chess boards but
    * is equal for all Fields on normal boards.
    * </p>
    * <p>
    * The map contains also mappings from "attackboard.[white|black].[1|2]" to
    * Pin names. Pin names are "QP1"-"QP6" and "KP1"-"KP6".
    * </p>
    * <p>
    * Therefor the value Object may be an Integer or a String.
    * </p>
    * 
    * @return Map<String, Object>, e.g., "a8:0"-> CASTLE_BLACK or "z9:5"->
    *         CASTLE_BLACK
    */
   public abstract Map<String, Object> createBoardFigures();

   /**
    * Returns a 3D Field name for the given board position.
    * 
    * @param xyz
    * @return String, e.g. "a9:5" on a Tri-D board or "a8:0" on a regular board
    */
   public abstract String get3DName( int[] xyz );

   /**
    * Returns a Field name for the given board position.
    * 
    * @param xy
    * @return String, e.g. "a9" on a Tri-D board or "a8" on a regular board
    */
   public abstract String get2DName( int[] xy );

   /**
    * Clears all figure lists and fields from figures.
    */
   public void clearEverything() {
      int count = getFieldsList().size();
      for ( int i = 0; i < count; i++ ) {
         Field f = getFieldsList().get( i );
         if ( f.getFigure() != null ) {
            f.setFigure( null );
         }
      }
      blackKing = null;
      whiteKing = null;
      blackFiguresList.clear();
      whiteFiguresList.clear();
      attackedFiguresView.getBlackFiguresList().clear();
      attackedFiguresView.getWhiteFiguresList().clear();
   }

   /**
    * Recreate fields by name mapping. Clears the map and adds every field found
    * in the field map again with it's current 3D name. Calling this method can
    * be necessary after the position of the attack board fields have changed.
    */
   public void recreateFieldsByName() {
      Map<String, Field> map = new HashMap<String, Field>();
      for ( Field f : getFieldsList() ) {
         map.put( f.get3DName(), f );
      }
      fieldsByName = map;
   }

   /**
    * Check figure on selected field and highlight other fields to indicate
    * range or coverage.
    * 
    * @param f
    *           selected Field
    */
   protected abstract void highlightFieldsForSelectedField( Field f );

   /**
    * Process a mouse motion event on the BoardView. Triggers highlighting of
    * fields and pins if the mouse cursor hovers over fields and pins.
    * 
    * @param e
    *           MouseEvent
    */
   public abstract void processMouseMotion( MouseEvent e );

   /**
    * Process a mouse drag event on the BoardView. Triggers movement of figures
    * and pins.
    * 
    * @param e
    *           MouseEvent
    */
   public abstract void processMouseDrag( MouseEvent e );

   /**
    * Process start of a mouse drag event on the BoardView. Triggers movement of
    * figures and pins.
    * 
    * @param e
    *           MouseEvent
    */
   public abstract void processMouseDragStart( MouseEvent e );

   /**
    * Process end of a mouse drag event on the BoardView. Triggers movement of
    * figures and pins.
    * 
    * @param e
    *           MouseEvent
    */
   public abstract void processMouseDragEnd( MouseEvent e );

   /**
    * Applies a shade to all fields with the same name. On normal boards the
    * shade should be set to the field only.
    * 
    * @param field
    * @param shade
    */
   protected abstract void setFieldShade( Field field, int shade );

   /**
    * @return dragging
    */
   public boolean isDragging() {
      return dragging;
   }

   /**
    * @param dragging
    *           Festzulegender dragging
    */
   public void setDragging( boolean dragging ) {
      this.dragging = dragging;
   }

   /**
    * @return view
    */
   public BoardView getView() {
      return view;
   }

   /**
    * @param view
    *           BoardView
    */
   protected void setView( BoardView view ) {
      this.view = view;
   }

   /**
    * @return fieldsList
    */
   public ArrayList<Field> getFieldsList() {
      return fieldsList;
   }

   /**
    * @return fieldsByName
    */
   public Map<String, Field> getFieldsByName() {
      return fieldsByName;
   }

   /**
    * @return blackFiguresList ArrayList<Figure>
    */
   public ArrayList<Figure> getBlackFiguresList() {
      return blackFiguresList;
   }

   /**
    * Creates a new grid of Fields (Field[maxX][maxY]).
    * 
    * @param maxX
    * @param maxY
    */
   protected void createGridFields( int maxX, int maxY ) {
      gridFields = new Field[maxX][maxY];
   }

   /**
    * Places a Field on a screen grid position.
    * 
    * @param x
    *           x coordinate
    * @param y
    *           y coordinate
    * @param f
    *           Field
    */
   protected void setGridField( int x, int y, Field f ) {
      if ( f == null ) {
         System.err.println( "grid field [" + x + ":" + y + "] was set null!" );
      }
      gridFields[x][y] = f;
   }

   /**
    * @return gridFields
    */
   public Field[][] getGridFields() {
      return gridFields;
   }

   /**
    * @return pinsList ArrayList<Pin>
    */
   public ArrayList<Pin> getPinsList() {
      return pinsList;
   }

   /**
    * @return whiteFiguresList ArrayList<Figure>
    */
   public ArrayList<Figure> getWhiteFiguresList() {
      return whiteFiguresList;
   }

   /**
    * @return lastFieldUnderMouse
    */
   public Field getLastFieldUnderMouse() {
      return lastFieldUnderMouse;
   }

   /**
    * @param lastFieldUnderMouse
    *           Field
    */
   public void setLastFieldUnderMouse( Field lastFieldUnderMouse ) {
      this.lastFieldUnderMouse = lastFieldUnderMouse;
   }

   /**
    * @return lastPinUnderMouse
    */
   public Pin getLastPinUnderMouse() {
      return lastPinUnderMouse;
   }

   /**
    * @param lastPinUnderMouse
    *           Pin
    */
   public void setLastPinUnderMouse( Pin lastPinUnderMouse ) {
      this.lastPinUnderMouse = lastPinUnderMouse;
   }

   /**
    * @return fieldUnderMouseDrag
    */
   public Field getFieldUnderMouseDrag() {
      return fieldUnderMouseDrag;
   }

   /**
    * @param fieldUnderMouseDrag
    *           Field
    */
   public void setFieldUnderMouseDrag( Field fieldUnderMouseDrag ) {
      this.fieldUnderMouseDrag = fieldUnderMouseDrag;
   }

   /**
    * @return pinUnderMouseDrag
    */
   public Pin getPinUnderMouseDrag() {
      return pinUnderMouseDrag;
   }

   /**
    * @param pinUnderMouseDrag
    *           Pin
    */
   public void setPinUnderMouseDrag( Pin pinUnderMouseDrag ) {
      this.pinUnderMouseDrag = pinUnderMouseDrag;
   }

   /**
    * @return mouseDragPosition
    */
   public Point getMouseDragPosition() {
      return mouseDragPosition;
   }

   /**
    * @param mouseDragPosition
    *           Point
    */
   public void setMouseDragPosition( Point mouseDragPosition ) {
      this.mouseDragPosition = mouseDragPosition;
   }

   /**
    * @return blackAttackedFiguresList
    */
   public ArrayList<Figure> getBlackAttackedFiguresList() {
      return attackedFiguresView.getBlackFiguresList();
   }

   /**
    * @return whiteAttackedFiguresList
    */
   public ArrayList<Figure> getWhiteAttackedFiguresList() {
      return attackedFiguresView.getWhiteFiguresList();
   }

   /**
    * Marks a figure as out of game. It will not be shown on the board anymore.
    * 
    * @param fig
    *           Figure
    */
   public void placeFigureOutOfGame( Figure fig ) {
      if ( fig == null ) return;
      fig.setInGame( false );
      if ( fig.isWhite() ) {
         whiteFiguresList.remove( fig );
         attackedFiguresView.getWhiteFiguresList().add( fig );
         attackedFiguresView.redraw();
      }
      else {
         blackFiguresList.remove( fig );
         attackedFiguresView.getBlackFiguresList().add( fig );
         attackedFiguresView.redraw();
      }
   }

   /**
    * @return the blackKing Figure
    */
   public Figure getBlackKing() {
      return blackKing;
   }

   /**
    * @return the whiteKing Figure
    */
   public Figure getWhiteKing() {
      return whiteKing;
   }

   /**
    * @param fig
    *           the blackKing Figure
    */
   public void setBlackKing( Figure fig ) {
      blackKing = fig;
   }

   /**
    * @param fig
    *           the whiteKing Figure
    */
   public void setWhiteKing( Figure fig ) {
      whiteKing = fig;
   }

   /**
    * Display a message box or something similar to provide help information.
    * 
    * @param frame
    *           parent JFrame
    */
   public abstract void displayHelpMessageBox( JFrame frame );

   /**
    * For all figures needs to find the fields they can move onto, figures they
    * can attack, and figures they cover. Also collects fields where regular
    * movement is blocked by other figures.<br>
    * Marks kings being check or mate.
    */
   public abstract void refreshFiguresStates();

   /**
    * @return the figureImageFilenames
    */
   public static String[] getFigureImageFilenames() {
      return figureImageFilenames;
   }

   /**
    * @return the figureTypes
    */
   public static int[] getFigureTypes() {
      return figureTypes;
   }

   /**
    * @return the game
    */
   public Game getGame() {
      return game;
   }

   /**
    * @param game
    *           the game to set
    */
   protected void setGame( Game game ) {
      this.game = game;
   }

}
