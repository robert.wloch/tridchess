/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import de.rowlo.tridchess.model.Figure;

/**
 * Paints a party's figures that are out of play.
 * 
 * @author Robert Wloch
 */
public class FiguresListView extends JComponent {
   private static final long serialVersionUID = 3499604114670816673L;

   /** Orientation of the figures list. */
   public static final int        TOP_LEFT              = 1;
   /** Orientation of the figures list. */
   public static final int        TOP_RIGHT             = 2;
   /** Orientation of the figures list. */
   public static final int        BOTTOM_LEFT           = 3;
   /** Orientation of the figures list. */
   public static final int        BOTTOM_RIGHT          = 4;

   private RepaintRunnable        repainter;
   private ChessUi                cui;
   private int                    sx, sy, signX, signY;
   private int                    fieldWidth            = 35;
   private int                    fieldHeight           = 35;
   private ArrayList<Figure>      blackFiguresList      = new ArrayList<Figure>();
   private ArrayList<Figure>      whiteFiguresList      = new ArrayList<Figure>();
   private Map<Rectangle, Figure> figureLocations       = new HashMap<Rectangle, Figure>();
   private Point                  draggedFigureLocation = null;
   private Figure                 draggedFigure         = null;

   /**
    * Creates a view of a list of Figures.
    * 
    * @param size
    *           the size of the view
    * @param ui
    *           user interface configuration
    */
   public FiguresListView( Dimension size, ChessUi ui ) {
      super();
      this.repainter = new RepaintRunnable( this );
      setChessUi( ui );
      Dimension d = new Dimension( 40 + 4 * fieldWidth, size.height );
      setSize( d );
      setMinimumSize( d );
      setMaximumSize( d );
      setPreferredSize( d );
   }

   /**
    * Sets the list's orientation.
    * 
    * @param o
    *           the orientation; defaults to TOP_LEFT.
    */
   protected void setOrientation( int o ) {
      switch ( o ) {
         case TOP_RIGHT:
            sx = 3;
            sy = 0;
            signX = -1;
            signY = 1;
            break;
         case BOTTOM_LEFT:
            sx = 0;
            sy = 3;
            signX = 1;
            signY = -1;
            break;
         case BOTTOM_RIGHT:
            sx = 3;
            sy = 3;
            signX = -1;
            signY = -1;
            break;
         case TOP_LEFT:
         default:
            sx = 0;
            sy = 0;
            signX = 1;
            signY = 1;
      }
   }

   /**
    * @return the figuresList
    */
   public ArrayList<Figure> getBlackFiguresList() {
      return blackFiguresList;
   }

   /**
    * @return the figuresList
    */
   public ArrayList<Figure> getWhiteFiguresList() {
      return whiteFiguresList;
   }

   /**
    * @return fieldHeight
    */
   public int getFieldHeight() {
      return fieldHeight;
   }

   /**
    * @param fieldHeight
    */
   public void setFieldHeight( int fieldHeight ) {
      this.fieldHeight = fieldHeight;
   }

   /**
    * @return fieldWidth
    */
   public int getFieldWidth() {
      return fieldWidth;
   }

   /**
    * @param fieldWidth
    */
   public void setFieldWidth( int fieldWidth ) {
      this.fieldWidth = fieldWidth;
      Dimension d = new Dimension( 40 + 4 * fieldWidth, getHeight() );
      setSize( d );
      setMinimumSize( d );
      setMaximumSize( d );
      setPreferredSize( d );
   }

   /**
    * @return ChessUi
    */
   public ChessUi getChessUi() {
      if ( cui == null ) setChessUi( new ChessUi() );
      return cui;
   }

   /**
    * Sets the ChessUi to be used.
    * 
    * @param cui
    *           ChessUi
    */
   public void setChessUi( ChessUi cui ) {
      this.cui = cui;
   }

   /**
    * @return the draggedFigure
    */
   public Figure getDraggedFigure() {
      return draggedFigure;
   }

   /**
    * @param draggedFigure
    *           the draggedFigure to set
    */
   public void setDraggedFigure( Figure draggedFigure ) {
      this.draggedFigure = draggedFigure;
   }

   /**
    * @return the draggedFigureLocation
    */
   public Point getDraggedFigureLocation() {
      return draggedFigureLocation;
   }

   /**
    * @param draggedFigureLocation
    *           the draggedFigureLocation to set
    */
   public void setDraggedFigureLocation( Point draggedFigureLocation ) {
      this.draggedFigureLocation = draggedFigureLocation;
   }

   /**
    * @return Figure under point or null.
    */
   public synchronized Figure getFigureForPoint( Point p ) {
      Iterator<Rectangle> it = figureLocations.keySet().iterator();
      try {
         while ( it.hasNext() ) {
            Rectangle r = it.next();
            if ( r.contains( p ) ) {
               Figure figure = figureLocations.get( r );
               return figure;
            }
         }
      }
      catch ( ConcurrentModificationException cme ) {
      }
      return null;
   }

   /**
    * Removes a figure from the black or white figures list.
    */
   public void removeFigure( Figure fig ) {
      synchronized ( blackFiguresList ) {
         if ( blackFiguresList.contains( fig ) ) {
            blackFiguresList.remove( fig );
         }
      }
      synchronized ( whiteFiguresList ) {
         if ( whiteFiguresList.contains( fig ) ) {
            whiteFiguresList.remove( fig );
         }
      }
   }

   /**
    * Invokes a Runnable to repaint the board at the end of the current event
    * thread's run.
    */
   public void redraw() {
      SwingUtilities.invokeLater( repainter );
   }

   @Override
   public void paint( Graphics g ) {
      Graphics2D g2 = (Graphics2D) g;

      // paint background
      g2.setPaint( cui.getUiProperty( ChessUi.BACKGROUND_COLOR ) );
      g2.fillRect( 0, 0, getWidth(), getHeight() );

      // paint figures
      setOrientation( TOP_LEFT );
      int topLeftY = getFieldHeight();
      if ( signY < 0 ) {
         topLeftY = getHeight() - ( 40 + 5 * getFieldHeight() );
      }
      figureLocations.clear();
      boolean couldDrag = draggedFigureLocation != null
            && draggedFigure != null;
      synchronized ( blackFiguresList ) {
         int count = blackFiguresList.size();
         for ( int y = 0; y < 4; y++ ) {
            for ( int x = 0; x < 4; x++ ) {
               int i = sx + signX * x + ( sy + signY * y ) * 4;
               if ( i < count ) {
                  Figure fig = blackFiguresList.get( i );
                  int screenX = 20 + x * getFieldWidth();
                  int screenY = 20 + topLeftY + y * getFieldHeight();
                  if ( fig == draggedFigure ) {
                     g2.setPaint( cui
                           .getUiProperty( ChessUi.SELECTED_FIELD_COLOR ) );
                     g2.fillRect( screenX, screenY, getFieldWidth(),
                           getFieldHeight() );
                  }
                  figureLocations.put( new Rectangle( screenX, screenY,
                        getFieldWidth(), getFieldHeight() ), fig );
                  g2.drawImage( fig.getImage(), screenX, screenY,
                        getFieldWidth(), getFieldHeight(), this );
                  if ( couldDrag && fig == draggedFigure ) {
                     g2.drawImage( fig.getImage(), draggedFigureLocation.x,
                           draggedFigureLocation.y, getFieldWidth(),
                           getFieldHeight(), this );
                  }
               }
            }
         }
      }
      setOrientation( BOTTOM_LEFT );
      topLeftY = getFieldHeight();
      if ( signY < 0 ) {
         topLeftY = 13 * getFieldHeight();
      }
      synchronized ( whiteFiguresList ) {
         int count = whiteFiguresList.size();
         for ( int y = 0; y < 4; y++ ) {
            for ( int x = 0; x < 4; x++ ) {
               int i = sx + signX * x + ( sy + signY * y ) * 4;
               if ( i < count ) {
                  Figure fig = whiteFiguresList.get( i );
                  int screenX = 20 + x * getFieldWidth();
                  int screenY = 20 + topLeftY + y * getFieldHeight();
                  if ( fig == draggedFigure ) {
                     g2.setPaint( cui
                           .getUiProperty( ChessUi.SELECTED_FIELD_COLOR ) );
                     g2.fillRect( screenX, screenY, getFieldWidth(),
                           getFieldHeight() );
                  }
                  figureLocations.put( new Rectangle( screenX, screenY,
                        getFieldWidth(), getFieldHeight() ), fig );
                  g2.drawImage( fig.getImage(), screenX, screenY,
                        getFieldWidth(), getFieldHeight(), this );
                  if ( couldDrag && fig == draggedFigure ) {
                     g2.drawImage( fig.getImage(), draggedFigureLocation.x,
                           draggedFigureLocation.y, getFieldWidth(),
                           getFieldHeight(), this );
                  }
               }
            }
         }
      }
   }

   class RepaintRunnable implements Runnable {
      FiguresListView subject = null;

      /**
       * Constructs a repaint thread for a view.
       * 
       * @param v
       *           repaints this view
       */
      public RepaintRunnable( FiguresListView v ) {
         subject = v;
      }

      public void run() {
         subject.repaint();
      }
   }
}
