/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.gui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import de.rowlo.tridchess.model.trid.Move;

/**
 * Paints a chess board's move log.
 * 
 * @author Robert Wloch
 */
public class LogView extends JComponent {
   private static final long serialVersionUID = 3939915126835550974L;

   private RepaintRunnable repainter;
   private ChessUi         cui;
   private ArrayList<Move> moveLog = new ArrayList<Move>();

   /**
    * Constructs a game log's view.
    * 
    * @param size
    *           the size of the log view.
    * @param ui
    *           the user interface configuration.
    */
   public LogView( Dimension size, ChessUi ui ) {
      super();
      this.repainter = new RepaintRunnable( this );
      setChessUi( ui );
      setSize( size );
      setMinimumSize( size );
      setPreferredSize( size );
   }

   /**
    * @return ChessUi
    */
   public ChessUi getChessUi() {
      if ( cui == null ) setChessUi( new ChessUi() );
      return cui;
   }

   /**
    * Sets the ChessUi to be used.
    * 
    * @param cui
    *           ChessUi
    */
   public void setChessUi( ChessUi cui ) {
      this.cui = cui;
   }

   /**
    * @return moveLog
    */
   public ArrayList<Move> getMoveLog() {
      return moveLog;
   }

   /**
    * Adds a move to the log entry.
    * 
    * @param entry
    *           Move String
    */
   public void addLogEntry( Move entry ) {
      getMoveLog().add( entry );

      String indent = "";
      if ( getMoveLog().size() < 10 ) indent = "  ";
      if ( getMoveLog().size() > 9 && getMoveLog().size() < 100 ) indent = " ";
      FontMetrics fm = getFontMetrics( new Font( "Courier New", Font.PLAIN, 12 ) );
      int hs = fm.getHeight() / 2;
      int maxHeight = 10 + hs + 14 * getMoveLog().size();
      if ( getHeight() < maxHeight ) {
         Dimension size = new Dimension( getWidth(), maxHeight );
         setSize( size );
         setMinimumSize( size );
         setPreferredSize( size );
         invalidate();
      }
      redraw();
      System.out.println( indent + getMoveLog().size() + ": " + entry );
   }

   /**
    * Saves the move log to a file.
    */
   public void saveMoveLog() {
      SimpleDateFormat dfm = (SimpleDateFormat) SimpleDateFormat
            .getDateTimeInstance();
      dfm.applyPattern( "yyyy-MM-dd_HH-mm" );
      String dateTime = dfm.format( new Date() );
      String path = "logs/";
      String filePath = path + "TriDMatchMoves_" + dateTime + ".log";
      try {
         File f = new File( path );
         f.mkdirs();
         BufferedOutputStream bos = new BufferedOutputStream(
               new FileOutputStream( filePath ) );
         int count = getMoveLog().size();
         for ( int i = 1; i <= count; i++ ) {
            String s = "";
            if ( i <= 99 ) s = " ";
            if ( i <= 9 ) s = "  ";
            s = s + i + ": " + getMoveLog().get( i - 1 ) + "\n";
            bos.write( s.getBytes() );
         }
         bos.close();
      }
      catch ( FileNotFoundException e ) {
         e.printStackTrace();
         System.err.println( "ERROR: Cannot save move log: " + filePath );
      }
      catch ( IOException e ) {
         e.printStackTrace();
         System.err.println( "ERROR: Cannot save move log: " + filePath );
      }
      catch ( NullPointerException e ) {
         e.printStackTrace();
         System.err.println( "ERROR: Cannot save move log: " + filePath );
      }
   }

   /**
    * Invokes a Runnable to repaint the board at the end of the current event
    * thread's run.
    */
   public void redraw() {
      SwingUtilities.invokeLater( repainter );
   }

   @Override
   public void paint( Graphics g ) {
      Graphics2D g2 = (Graphics2D) g;

      // paint background
      g2.setPaint( cui.getUiProperty( ChessUi.BACKGROUND_COLOR ) );
      g2.fillRect( 0, 0, getWidth(), getHeight() );

      // paint labels
      g2.setPaint( cui.getUiProperty( ChessUi.LABEL_COLOR ) );
      Font fnt = new Font( "Courier New", Font.PLAIN, 12 );
      g2.setFont( fnt );
      FontMetrics fm = getFontMetrics( fnt );
      int hs = fm.getHeight() / 2;

      int count = getMoveLog().size();
      for ( int i = 1; i <= count; i++ ) {
         String s = "";
         if ( i <= 99 ) s = " ";
         if ( i <= 9 ) s = "  ";
         s = s + i + ": " + getMoveLog().get( i - 1 );
         g2.drawString( s, 20, 10 + hs + 14 * i );
      }
   }

   class RepaintRunnable implements Runnable {
      LogView subject = null;

      /**
       * Creates a repaint thread for a view.
       * 
       * @param v
       *           the view to be repainted.
       */
      public RepaintRunnable( LogView v ) {
         subject = v;
      }

      public void run() {
         subject.repaint();
      }
   }
}
