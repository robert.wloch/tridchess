/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.gui;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

/**
 * <p>
 * A FieldView is a Canvas Component that shows a field of a chess board.
 * </p>
 * 
 * @author Robert Wloch
 * 
 */
public class FieldView extends Canvas {
   private static final long serialVersionUID = -4455037249497540734L;

   /** Highlighting used for a field. */
   public static final int NO_SHADE        = 0;
   /** Highlighting used for a field. */
   public static final int SELECTED_SHADE  = 1;
   /** Highlighting used for a field. */
   public static final int ALLOWED_SHADE   = 2;
   /** Highlighting used for a field. */
   public static final int ATTACK_SHADE    = 3;
   /** Highlighting used for a field. */
   public static final int GUARDED_SHADE   = 4;
   /** Highlighting used for a field. */
   public static final int FORBIDDEN_SHADE = 5;
   /** Highlighting used for a field. */
   public static final int ATTACKER_SHADE  = 6;

   /** Reference to the user interface configuration. */
   protected ChessUi       ui;
   /** The field's position on the board. */
   protected int[]         b;
   /** The field's position on the screen grid. */
   protected Point         g;
   /** The board's position on the screen. */
   protected Point         ref;
   /** The board's x position on the screen. */
   protected int           screenX;
   /** The board's y position on the screen. */
   protected int           screenY;
   /** Flag indication a field to belong to black or white party. */
   protected boolean       isWhite;
   /** The highlight type currently set. */
   protected int           shade;

   /**
    * Construction of black field with position. The position is not screen
    * position, but indices of the field grid.
    * 
    * @param posB
    *           position on the board
    * @param posG
    *           position on the grid
    * @param ref
    *           top left coordinate of chess board on graphics device.
    */
   public FieldView( int[] posB, Point posG, Point ref ) {
      this( posB, posG, ref, false );
   }

   /**
    * Construction of white or black field with position. The position is not
    * screen position, but indices of the field grid.
    * 
    * @param posB
    *           position on the board
    * @param posG
    *           position on the grid
    * @param ref
    *           top left coordinate of chess board on graphics device.
    * @param isWhite
    *           true, if it's white party's field, false if black
    */
   public FieldView( int[] posB, Point posG, Point ref, boolean isWhite ) {
      this.screenX = 0;
      this.screenY = 0;
      this.isWhite = isWhite;
      setShade( NO_SHADE );
      setBoardPos( posB );
      setGridPos( posG );
      setRefPos( ref );
   }

   /**
    * Sets a new position of the field. This may happen, if attack boards
    * changed location on a Tri-D chess board.
    */
   public void setBoardPos( int[] pos ) {
      this.b = pos;
   }

   /**
    * Returns the field's board position.
    */
   public int[] getBoardPos() {
      return this.b;
   }

   /**
    * Sets a new position of the field. This may happen, if attack boards
    * changed location on a Tri-D chess board.
    */
   public void setGridPos( Point pos ) {
      this.g = pos;
      calculateScreenPos();
   }

   /**
    * Returns the field's grid position.
    */
   public Point getGridPos() {
      return this.g;
   }

   /**
    * Sets a new position of the board on graphics device.
    */
   public void setRefPos( Point ref ) {
      this.ref = ref;
      calculateScreenPos();
   }

   /**
    * Returns the board's screen position.
    */
   public Point getRefPos() {
      return this.ref;
   }

   /**
    * Sets the ChessUi class for providing the current color scheme.
    * 
    * @param ui
    */
   public void setUi( ChessUi ui ) {
      this.ui = ui;
   }

   /**
    * Sets the shade of the field.
    * 
    * @param shade
    */
   public void setShade( int shade ) {
      switch ( shade ) {
         case SELECTED_SHADE:
            this.shade = SELECTED_SHADE;
            break;
         case ALLOWED_SHADE:
            this.shade = ALLOWED_SHADE;
            break;
         case ATTACK_SHADE:
            this.shade = ATTACK_SHADE;
            break;
         case GUARDED_SHADE:
            this.shade = GUARDED_SHADE;
            break;
         case FORBIDDEN_SHADE:
            this.shade = FORBIDDEN_SHADE;
            break;
         case ATTACKER_SHADE:
            this.shade = ATTACKER_SHADE;
            break;
         default:
            this.shade = NO_SHADE;
            break;
      }
   }

   /**
    * Returns the shade of the field.
    * 
    * @return shade
    */
   public int getShade() {
      return this.shade;
   }

   /**
    * Returns the name of the shade of the field.
    * 
    * @return shade
    */
   public String getShadeName() {
      return getShadeName( shade );
   }

   /**
    * Returns the name of the shade of the field.
    * 
    * @param s
    *           shade value
    * @return shade
    */
   public String getShadeName( int s ) {
      StringBuffer sb = new StringBuffer();
      switch ( s ) {
         case FieldView.NO_SHADE:
            sb = sb.append( "NO_SHADE" );
            break;
         case FieldView.SELECTED_SHADE:
            sb = sb.append( "SELECTED_SHADE" );
            break;
         case FieldView.ALLOWED_SHADE:
            sb = sb.append( "ALLOWED_SHADE" );
            break;
         case FieldView.ATTACK_SHADE:
            sb = sb.append( "ATTACK_SHADE" );
            break;
         case FieldView.GUARDED_SHADE:
            sb = sb.append( "GUARDED_SHADE" );
            break;
         case FieldView.FORBIDDEN_SHADE:
            sb = sb.append( "FORBIDDEN_SHADE" );
            break;
         case FieldView.ATTACKER_SHADE:
            sb = sb.append( "ATTACKER_SHADE" );
            break;
         default:
            sb = sb.append( "shade=" + s );
            break;
      }
      return sb.toString();
   }

   /**
    * Calculates new position on the paint device for paint method's Graphics
    * object. Called after setting new reference or grid position.
    */
   protected void calculateScreenPos() {
      if ( ref == null || g == null ) {
         this.screenX = 0;
         this.screenY = 0;
         return;
      }
      this.screenX = ref.x + getWidth() * g.x;
      this.screenY = ref.y + getHeight() * g.y;
   }

   /**
    * @return screenX
    */
   public int getScreenX() {
      return screenX;
   }

   /**
    * @return screenY
    */
   public int getScreenY() {
      return screenY;
   }

   @Override
   public void setSize( Dimension d ) {
      super.setSize( d.width, d.height );
      calculateScreenPos();
   }

   @Override
   public boolean contains( Point p ) {
      if ( p == null ) return false;
      if ( p.x < screenX || p.x > screenX + getWidth() || p.y < screenY
            || p.y > screenY + getHeight() ) return false;
      return true;
   }

   @Override
   public void paint( Graphics g ) {
      Graphics2D g2 = (Graphics2D) g;
      g2.setPaint( ui
            .getUiProperty( ( isWhite ) ? ChessUi.WHITE_FIELD_EDGE_COLOR
                  : ChessUi.BLACK_FIELD_EDGE_COLOR ) );
      g2.drawRect( this.screenX, this.screenY, getWidth() - 1, getHeight() - 1 );
      g2.setPaint( ui.getUiProperty( ( isWhite ) ? ChessUi.WHITE_FIELD_COLOR
            : ChessUi.BLACK_FIELD_COLOR ) );
      g2.fillRect( this.screenX + 1, this.screenY + 1, getWidth() - 2,
            getHeight() - 2 );
      if ( shade > NO_SHADE ) {
         String prop = null;
         switch ( shade ) {
            case SELECTED_SHADE:
               prop = ChessUi.SELECTED_FIELD_COLOR;
               break;
            case ALLOWED_SHADE:
               prop = ChessUi.ALLOWED_FIELD_COLOR;
               break;
            case ATTACK_SHADE:
               prop = ChessUi.ATTACK_FIELD_COLOR;
               break;
            case GUARDED_SHADE:
               prop = ChessUi.GUARDED_FIELD_COLOR;
               break;
            case FORBIDDEN_SHADE:
               prop = ChessUi.FORBIDDEN_FIELD_COLOR;
               break;
            case ATTACKER_SHADE:
               prop = ChessUi.ATTACKER_FIELD_COLOR;
               break;
         }
         if ( prop != null ) {
            g2.setPaint( ui.getUiProperty( prop ) );
            g2.fillRect( this.screenX + 1, this.screenY + 1, getWidth() - 2,
                  getHeight() - 2 );
         }
      }
   }
}
