/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.gui.trid;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

import de.rowlo.tridchess.gui.ChessUi;

/**
 * <p>
 * A PinView is a Canvas Component that shows a pin of a Tri-D chess board.
 * </p>
 * 
 * @author Robert Wloch
 * 
 */
public class PinView extends Canvas {
   private static final long serialVersionUID = -7250171510855827090L;

   /** Highlighting used for a pin. */
   public static final int NO_SHADE        = 0;
   /** Highlighting used for a pin. */
   public static final int SELECTED_SHADE  = 1;
   /** Highlighting used for a pin. */
   public static final int ALLOWED_SHADE   = 2;
   /** Highlighting used for a pin. */
   public static final int FORBIDDEN_SHADE = 3;

   /** Reference to the user interface configuration. */
   protected ChessUi       ui;
   /** The pin's position on the board. */
   protected int[]         b;
   /** The pin's position on the screen grid. */
   protected Point         g;
   /** The board's position on the screen. */
   protected Point         ref;
   /** The board's x position on the screen. */
   protected int           screenX;
   /** The board's y position on the screen. */
   protected int           screenY;
   /** The highlight type currently set. */
   protected int           shade;
   /** Flag if a pin is active or not. */
   protected boolean       isGhost;

   /**
    * Construction of a permanently visible pin with position. The position is
    * not screen position, but indices of the field grid.
    * 
    * @param posB
    *           position on the board
    * @param posG
    *           position on the grid
    * @param ref
    *           top left coordinate of chess board on graphics device.
    */
   public PinView( int[] posB, Point posG, Point ref ) {
      this( posB, posG, ref, false );
   }

   /**
    * Construction of pin with position. The position is not screen position,
    * but indices of the field grid. If ghost is true, the pin will be invisible
    * as long as its shade is NO_SHADE. Ghost pins are no real pins, but serve
    * visualization purposes.
    * 
    * @param posB
    *           position on the board
    * @param posG
    *           position on the grid
    * @param ref
    *           top left coordinate of chess board on graphics device.
    * @param ghost
    *           true, if pin is without fields.
    */
   public PinView( int[] posB, Point posG, Point ref, boolean ghost ) {
      this.screenX = 0;
      this.screenY = 0;
      this.isGhost = ghost;
      setShade( NO_SHADE );
      setBoardPos( posB );
      setGridPos( posG );
      setRefPos( ref );
   }

   /**
    * Sets a new position of the pin. This may happen, if attack boards changed
    * location on a Tri-D chess board.
    */
   public void setBoardPos( int[] pos ) {
      this.b = pos;
   }

   /**
    * Returns the pin's board position.
    */
   public int[] getBoardPos() {
      return this.b;
   }

   /**
    * Sets a new position of the pin. This may happen, if attack boards changed
    * location on a Tri-D chess board.
    */
   public void setGridPos( Point pos ) {
      this.g = pos;
      calculateScreenPos();
   }

   /**
    * Returns the pin's grid position.
    */
   public Point getGridPos() {
      return this.g;
   }

   /**
    * Sets a new position of the board on graphics device.
    */
   public void setRefPos( Point ref ) {
      this.ref = ref;
      calculateScreenPos();
   }

   /**
    * Returns the board's screen position.
    */
   public Point getRefPos() {
      return this.ref;
   }

   /**
    * Sets the ChessUi class for providing the current color scheme.
    * 
    * @param ui
    */
   public void setUi( ChessUi ui ) {
      this.ui = ui;
   }

   /**
    * @return shade
    */
   public int getShade() {
      return shade;
   }

   /**
    * Sets the current highlight type.
    * 
    * @param shade
    */
   public void setShade( int shade ) {
      switch ( shade ) {
         case SELECTED_SHADE:
            this.shade = SELECTED_SHADE;
            break;
         case ALLOWED_SHADE:
            this.shade = ALLOWED_SHADE;
            break;
         case FORBIDDEN_SHADE:
            this.shade = FORBIDDEN_SHADE;
            break;
         default:
            this.shade = NO_SHADE;
            break;
      }
   }

   /**
    * @return isGhost
    */
   public boolean isGhost() {
      return isGhost;
   }

   /**
    * @param isGhost
    *           boolean
    */
   public void setGhost( boolean isGhost ) {
      this.isGhost = isGhost;
   }

   /**
    * Calculates new position on the paint device for paint method's Graphics
    * object. Called after setting new reference or grid position.
    */
   protected void calculateScreenPos() {
      if ( ref == null || g == null ) {
         this.screenX = 0;
         this.screenY = 0;
         return;
      }
      this.screenX = ref.x + g.x * getWidth();
      // pin view is 2 fields high, but pin is only 6 pixels wide around
      // horizontal center line
      int halfHeight = getHeight() / 2;
      // screenY is lowest y being painted on
      this.screenY = ref.y + g.y * halfHeight + halfHeight - 3;
   }

   /**
    * @return screenX
    */
   public int getScreenX() {
      return screenX;
   }

   /**
    * @return screenY
    */
   public int getScreenY() {
      return screenY;
   }

   @Override
   public void setSize( Dimension d ) {
      super.setSize( d.width, d.height );
      calculateScreenPos();
   }

   /**
    * Works only on none ghost pins.
    */
   @Override
   public boolean contains( Point p ) {
      if ( p == null || isGhost ) return false;
      boolean excludesPoint = p.x < screenX || p.x > screenX + getWidth() || p.y < screenY
            || p.y > screenY + 6;
      if ( excludesPoint ) { return false; }
      return true;
   }

   /**
    * Does also work for ghost pins.
    * 
    * @param p
    *           Point
    * @return true, if Point is part of the pin
    */
   public boolean containsForAll( Point p ) {
      if ( p == null ) return false;
      boolean excludesPoint = p.x < screenX || p.x > screenX + getWidth() || p.y < screenY
              || p.y > screenY + 6;
        if ( excludesPoint ) { return false; }
      return true;
   }

   @Override
   public void paint( Graphics g ) {
      Graphics2D g2 = (Graphics2D) g;
      // paint pin only if not ghost pin or ghost pin with shade
      if ( !isGhost || ( isGhost && shade > NO_SHADE ) ) {
         g2.setPaint( new GradientPaint( screenX, screenY, ui
               .getUiProperty( ( isGhost ) ? ChessUi.PIN_GHOST_BRIGHT_COLOR
                     : ChessUi.PIN_BRIGHT_COLOR ), screenX, screenY + 6, ui
               .getUiProperty( ( isGhost ) ? ChessUi.PIN_GHOST_DARK_COLOR
                     : ChessUi.PIN_DARK_COLOR ) ) );
         g2.fillRect( screenX, screenY, getWidth(), 6 );
      }
      if ( shade > NO_SHADE ) {
         String prop = null;
         switch ( shade ) {
            case SELECTED_SHADE:
               prop = ChessUi.SELECTED_PIN_COLOR;
               break;
            case ALLOWED_SHADE:
               prop = ChessUi.ALLOWED_PIN_COLOR;
               break;
            case FORBIDDEN_SHADE:
               prop = ChessUi.FORBIDDEN_PIN_COLOR;
               break;
         }
         if ( prop != null ) {
            g2.setPaint( ui.getUiProperty( prop ) );
            g2.fillRect( screenX, screenY, getWidth(), 6 );
         }
      }
   }
}
