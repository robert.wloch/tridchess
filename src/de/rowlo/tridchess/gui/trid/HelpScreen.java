/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.gui.trid;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.HyperlinkEvent.EventType;

/**
 * The in game help screen.
 * 
 * @author Robert Wloch
 * 
 */
public class HelpScreen {
   private static JDialog dialog   = null;
   private static JButton okButton = null;

   /**
    * @return the help dialog.
    */
   public static JDialog getDialog() {
      return dialog;
   }

   /**
    * Shows the help dialog.
    * 
    * @param frame
    *           parent frame
    */
   public static void displayHelpScreen( JFrame frame ) {
      if ( dialog == null ) {
         URL url = ClassLoader.getSystemResource( "help/Tri-D.html" );

         JEditorPane editorContent = new JEditorPane();
         final JEditorPane editor = editorContent;
         editorContent.addHyperlinkListener( new HyperlinkListener() {
            public void hyperlinkUpdate( HyperlinkEvent e ) {
               editor.setToolTipText( "" );
               if ( e.getEventType().equals( EventType.ENTERED ) ) {
                  editor.setToolTipText( e.getURL().toString() );
               }
            }
         } );
         editorContent.setEditable( false );
         if ( url != null ) {
            try {
               editorContent.setPage( url );
            }
            catch ( IOException e ) {
               System.err.println( "Attempted to read a bad URL: " + url );
            }
         }
         else {
            System.err.println( "Couldn't find file: help/Tri-D.html" );
         }
         // Put the editor pane in a scroll pane.
         JScrollPane content = new JScrollPane( editorContent );
         content
               .setVerticalScrollBarPolicy( JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED );
         Dimension d = new Dimension( 860, 720 );
         content.setPreferredSize( d );
         content.setMinimumSize( d );

         dialog = new JDialog( frame, "Help - Tri-D Chess", true );
         dialog.setUndecorated( true );

         JPanel panel = new JPanel( new GridBagLayout() );
         GridBagConstraints gc = new GridBagConstraints();
         gc.weightx = 1.0;
         gc.weighty = 1.0;
         gc.fill = GridBagConstraints.BOTH;
         gc.insets = new Insets( 2, 2, 2, 2 );
         gc.gridx = 0;
         gc.gridy = 0;

         panel.add( content, gc );
         gc.weighty = 0.0;
         gc.fill = GridBagConstraints.HORIZONTAL;
         gc.gridy = 1;
         okButton = new JButton( "Ok" );
         okButton.addActionListener( new ActionListener() {
            public void actionPerformed( ActionEvent e ) {
               closeHelpScreen();
            }
         } );
         okButton.addKeyListener( new KeyAdapter() {
            @Override
            public void keyPressed( KeyEvent e ) {
               if ( e == null ) return;
               if ( e.getKeyCode() == KeyEvent.VK_ENTER ) {
                  closeHelpScreen();
                  e.consume();
               }
            }

         } );
         panel.add( okButton, gc );

         dialog.setContentPane( panel );
         dialog.pack();
         dialog.setResizable( false );

         // center dialog
         int w = dialog.getWidth();
         int h = dialog.getHeight();
         int x = GraphicsEnvironment.getLocalGraphicsEnvironment()
               .getDefaultScreenDevice().getDisplayMode().getWidth() / 2;
         int y = GraphicsEnvironment.getLocalGraphicsEnvironment()
               .getDefaultScreenDevice().getDisplayMode().getHeight() / 2;
         x = x - w / 2;
         if ( x < 0 ) x = 0;
         y = y - h / 2;
         if ( y < 0 ) y = 0;
         dialog.setBounds( x, y, w, h );

         okButton.requestFocusInWindow();
         dialog.setVisible( true );
      }
      else {
         okButton.requestFocusInWindow();
         dialog.setVisible( true );
      }
   }

   /**
    * Hides the help dialog.
    */
   public static void closeHelpScreen() {
      dialog.setVisible( false );
      // dialog = null;
   }
}
