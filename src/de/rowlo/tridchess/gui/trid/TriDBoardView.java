/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.gui.trid;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import de.rowlo.tridchess.gui.BoardView;
import de.rowlo.tridchess.gui.ChessUi;
import de.rowlo.tridchess.gui.FieldView;
import de.rowlo.tridchess.model.Field;
import de.rowlo.tridchess.model.Figure;
import de.rowlo.tridchess.model.trid.Pin;
import de.rowlo.tridchess.model.trid.TriDBoard;

/**
 * Paints the Tri-D chess board.
 * 
 * @author Robert Wloch
 * 
 */
public class TriDBoardView extends BoardView {
   private static final long serialVersionUID = 6226760625724825215L;

   private TriDBoard board;

   /**
    * Constructs the view.
    * 
    * @param colNames
    *           array of column names
    * @param rowNames
    *           array of row names
    * @param board
    *           the TriDBoard
    */
   public TriDBoardView( String[] colNames, String[] rowNames, TriDBoard board ) {
      super( colNames, rowNames );
      this.board = board;
   }

   /**
    * @return the board
    */
   protected TriDBoard getBoard() {
      return board;
   }

   @Override
   public Dimension getPreferredSize() {

      return super.getPreferredSize();
   }

   /**
    * Highlights the attacked king's field and the attackers' fields.
    * 
    * @param king
    *           the attacked king
    * @param g2
    *           the Graphics2D
    */
   protected void shadeMateFields( Figure king, Graphics2D g2 ) {
      FieldView fv = king.getField().getView();
      g2.setPaint( getChessUi().getUiProperty( ChessUi.ATTACK_FIELD_COLOR ) );
      g2.fillRect( fv.getScreenX() + 1, fv.getScreenY() + 1, fv.getWidth() - 2,
            fv.getHeight() - 2 );
      ArrayList<Field> a = king.getAttackersList();
      int count = a.size();
      for ( int i = 0; i < count; i++ ) {
         fv = a.get( i ).getView();
         g2
               .setPaint( getChessUi().getUiProperty(
                     ChessUi.ATTACKER_FIELD_COLOR ) );
         g2.fillRect( fv.getScreenX() + 1, fv.getScreenY() + 1,
               fv.getWidth() - 2, fv.getHeight() - 2 );
      }
   }

   @Override
   public void paint( Graphics g ) {
      super.paint( g );

      Graphics2D g2 = (Graphics2D) g;

      // label font and color
      g2.setPaint( getChessUi().getUiProperty( ChessUi.LABEL_COLOR ) );
      Font fnt = new Font( "Arial", Font.PLAIN, 12 );
      g2.setFont( fnt );
      FontMetrics fm = getFontMetrics( fnt );
      int hs = fm.getHeight() / 2;

      // paint board labels
      g2.setPaint( getChessUi().getUiProperty( ChessUi.LABEL_COLOR ) );
      Field f = getGridFields()[6][3];
      if ( f != null ) {
         int x = f.getView().getScreenX() + getFieldWidth() + 3;
         int y = f.getView().getScreenY() + hs - fm.getDescent();
         g2.drawString( "B", x, y );
         g2.drawString( "N", x, y + getFieldHeight() * 6 );
         g2.drawString( "W", x, y + getFieldHeight() * 12 );
      }

      // paint all chess pins
      int count = getBoard().getPinsList().size();
      int halfWidth = getFieldWidth() / 2;
      for ( int i = 0; i < count; i++ ) {
         Pin p = getBoard().getPinsList().get( i );
         // paint pin
         p.getView().paint( g2 );
         // paint pin name below
         if ( !p.getView().isGhost()
               || p.getView().getShade() != PinView.NO_SHADE ) {
            g2.setPaint( getChessUi().getUiProperty( ChessUi.LABEL_COLOR ) );
            String s = p.getName();
            if ( s.length() > 0 ) {
               int ls = fm.stringWidth( s ) / 2;
               int x = p.getView().getScreenX() + halfWidth - ls;
               int y = p.getView().getScreenY() + 6 + fm.getHeight();
               g2.drawString( s, x, y );
            }
         }
      }

      // don't draw and access things requiring a running game
      if ( !getBoard().getGame().isRunning() ) return;

      // paint hint at grid 5:6
      int sx = 20 + 5 * getFieldWidth();
      int sy = 20 + 6 * getFieldHeight();
      g2.setPaint( getChessUi().getUiProperty( ChessUi.HINT_COLOR ) );
      Font hfnt = new Font( "Arial Black", Font.PLAIN, 16 );
      g2.setFont( hfnt );
      FontMetrics hfm = getFontMetrics( hfnt );
      int hhs = hfm.getHeight() / 2;
      g2.drawString( getHintText(), sx - hfm.stringWidth( getHintText() ) / 2,
            sy + hhs - hfm.getDescent() );

      // paint check label at top of black's board or below white's
      Figure whiteKing = getBoard().getWhiteKing();
      Figure blackKing = getBoard().getBlackKing();
      if ( whiteKing != null && whiteKing.isCheck() ) {
         String msg = "check";
         if ( whiteKing.isMate() ) msg = "check mate";
         sy = 20 + 17 * getFieldHeight() + getFieldHeight() / 2;
         g2.drawString( msg, sx - hfm.stringWidth( msg ) / 2, sy + hhs
               - hfm.getDescent() );
      }
      else if ( blackKing != null && blackKing.isCheck() ) {
         String msg = "check";
         if ( blackKing.isMate() ) msg = "check mate";
         sy = 20 + getFieldHeight() / 2;
         g2.drawString( msg, sx - hfm.stringWidth( msg ) / 2, sy + hhs
               - hfm.getDescent() );
      }
      if ( whiteKing != null && whiteKing.isMate() ) {
         shadeMateFields( whiteKing, g2 );
      }
      if ( blackKing != null && blackKing.isMate() ) {
         shadeMateFields( blackKing, g2 );
      }

      // paint black figures
      synchronized ( getBoard().getBlackFiguresList() ) {
         for ( int i = 0; i < getBoard().getBlackFiguresList().size(); i++ ) {
            Figure fig = getBoard().getBlackFiguresList().get( i );
            FieldView fv = fig.getField().getView();
            g2.drawImage( getImage( fig.getName() ), fv.getScreenX(), fv
                  .getScreenY(), getFieldWidth(), getFieldHeight(), this );
         }
      }
      // paint white figures
      synchronized ( getBoard().getWhiteFiguresList() ) {
         for ( int i = 0; i < getBoard().getWhiteFiguresList().size(); i++ ) {
            Figure fig = getBoard().getWhiteFiguresList().get( i );
            FieldView fv = fig.getField().getView();
            g2.drawImage( getImage( fig.getName() ), fv.getScreenX(), fv
                  .getScreenY(), getFieldWidth(), getFieldHeight(), this );
         }
      }

      // paint tool tip of selected field under mouse cursor
      if ( getBoard().getLastFieldUnderMouse() != null ) {
         g2.setFont( fnt );
         String s = getBoard().getLastFieldUnderMouse().get3DName();
         int ls = fm.stringWidth( s ) + 6;
         int x = getBoard().getLastFieldUnderMouse().getView().getScreenX()
               + ( getFieldWidth() - ls ) / 2;
         int y = getBoard().getLastFieldUnderMouse().getView().getScreenY() - 2;
         g2.setPaint( getChessUi().getUiProperty( ChessUi.TOOLTIP_COLOR ) );
         g2.fillRect( x, y - fm.getHeight(), ls + 1, fm.getHeight() + 1 );
         g2.setPaint( getChessUi().getUiProperty( ChessUi.TOOLTIP_EDGE_COLOR ) );
         g2
               .drawRect( x - 1, y - fm.getHeight() - 1, ls + 2,
                     fm.getHeight() + 2 );
         g2.setPaint( getChessUi().getUiProperty( ChessUi.TOOLTIP_TEXT_COLOR ) );
         g2.drawString( s, x + 3, y - fm.getDescent() );
      }

      // paint tool tip of selected pin under mouse cursor
      if ( getBoard().getLastPinUnderMouse() != null ) {
         g2.setFont( fnt );
         String s = getBoard().getLastPinUnderMouse().getName();
         int ls = fm.stringWidth( s ) + 6;
         int x = getBoard().getLastPinUnderMouse().getView().getScreenX()
               + ( getFieldWidth() - ls ) / 2;
         int y = getBoard().getLastPinUnderMouse().getView().getScreenY() - 2;
         g2.setPaint( getChessUi().getUiProperty( ChessUi.TOOLTIP_COLOR ) );
         g2.fillRect( x, y - fm.getHeight(), ls + 1, fm.getHeight() + 1 );
         g2.setPaint( getChessUi().getUiProperty( ChessUi.TOOLTIP_EDGE_COLOR ) );
         g2
               .drawRect( x - 1, y - fm.getHeight() - 1, ls + 2,
                     fm.getHeight() + 2 );
         g2.setPaint( getChessUi().getUiProperty( ChessUi.TOOLTIP_TEXT_COLOR ) );
         g2.drawString( s, x + 3, y - fm.getDescent() );
      }

      // paint dragged figure or pin
      if ( getBoard().isDragging() ) {
         Field field = getBoard().getLastFieldUnderMouse();
         Pin pin = getBoard().getLastPinUnderMouse();
         // need to drag a figure?
         if ( field != null ) {
            Figure fig = field.getFigure();
            Point p = getBoard().getMouseDragPosition();
            Field dField = getBoard().getFieldUnderMouseDrag();
            if ( dField != null ) {
               int s = getBoard().getFieldUnderMouseDrag().getView().getShade();
               boolean canDrop = false;
               if ( s == FieldView.ALLOWED_SHADE || s == FieldView.ATTACK_SHADE
                     || getBoard().getGame().isFreeMoveMode() ) {
                  canDrop = true;
               }
               if ( fig != null && p != null && canDrop ) {
                  BufferedImage bi = getImage( fig.getName() );
                  g2.drawImage( bi, p.x, p.y, getFieldWidth(),
                        getFieldHeight(), this );
               }
            } // if: dField != null
         } // if: drag figure
         // need to drag a pin?
         else if ( pin != null ) {
            Point p = new Point( getBoard().getMouseDragPosition() );
            Pin dPin = getBoard().getPinUnderMouseDrag();
            if ( dPin != null ) {
               int s = dPin.getView().getShade();
               boolean canDrop = false;
               if ( s == PinView.ALLOWED_SHADE
                     || getBoard().getGame().isFreeMoveMode() ) {
                  canDrop = true;
               }
               if ( pin != null && p != null && canDrop ) {
                  p.setLocation( p.x, p.y + getFieldHeight() - 3 );
                  g2.setPaint( new GradientPaint( p.x, p.y, getChessUi()
                        .getUiProperty( ChessUi.PIN_BRIGHT_COLOR ), p.x,
                        p.y + 6, getChessUi().getUiProperty(
                              ChessUi.PIN_DARK_COLOR ) ) );
                  g2.fillRect( p.x, p.y, getFieldWidth(), 6 );
               }
            } // if: dPin != null
         } // else: drag pin
      } // if: dragging
   }
}
