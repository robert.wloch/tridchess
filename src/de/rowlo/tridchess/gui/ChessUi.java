/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.gui;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

/**
 * Provides color settings for the chess ui components. Attempts to read
 * de.rowlo.tridchess.config/ChessUi.properties or another special properties file. If none are
 * found, default values will be taken.
 * 
 * @author Robert Wloch
 * 
 */
public class ChessUi {

   /** Property key name for animated move sequence replay. */
   public final static String     REPLAY_DELAY           = "replay.delay";
   /** Property key name for components' backfround color. */
   public final static String     BACKGROUND_COLOR       = "background.color";
   /** Property key name for white field's color. */
   public final static String     WHITE_FIELD_COLOR      = "white.field.color";
   /** Property key name for white field's edge color. */
   public final static String     WHITE_FIELD_EDGE_COLOR = "white.field.edge.color";
   /** Property key name for black field's color. */
   public final static String     BLACK_FIELD_COLOR      = "black.field.color";
   /** Property key name for black field's edge color. */
   public final static String     BLACK_FIELD_EDGE_COLOR = "black.field.edge.color";
   /** Property key name for selected field's color. */
   public final static String     SELECTED_FIELD_COLOR   = "selected.field.color";
   /** Property key name for allowed field's color. */
   public final static String     ALLOWED_FIELD_COLOR    = "allowed.field.color";
   /** Property key name for attack field's color. */
   public final static String     ATTACK_FIELD_COLOR     = "attack.field.color";
   /** Property key name for guarded field's color. */
   public final static String     GUARDED_FIELD_COLOR    = "guarded.field.color";
   /** Property key name for forbidden field's color. */
   public final static String     FORBIDDEN_FIELD_COLOR  = "forbidden.field.color";
   /** Property key name for attacker field's color frame. */
   public final static String     ATTACKER_FIELD_COLOR   = "attacker.field.color";
   /** Property key name for label color. */
   public final static String     LABEL_COLOR            = "label.color";
   /** Property key name for hint color. */
   public final static String     HINT_COLOR             = "hint.color";
   /** Property key name for tool tip text color. */
   public final static String     TOOLTIP_TEXT_COLOR     = "tooltip.text.color";
   /** Property key name for tool tip color. */
   public final static String     TOOLTIP_COLOR          = "tooltip.color";
   /** Property key name for tool tip edge color. */
   public final static String     TOOLTIP_EDGE_COLOR     = "tooltip.edge.color";
   /** Property key name for pin's bright gradient color. */
   public final static String     PIN_BRIGHT_COLOR       = "pin.bright.color";
   /** Property key name for pin's dark gradient color. */
   public final static String     PIN_DARK_COLOR         = "pin.dark.color";
   /** Property key name for ghost pin's bright gradient color. */
   public final static String     PIN_GHOST_BRIGHT_COLOR = "pin.ghost.bright.color";
   /** Property key name for ghost pin's dark gradient color. */
   public final static String     PIN_GHOST_DARK_COLOR   = "pin.ghost.dark.color";
   /** Property key name for selected pin's color. */
   public final static String     SELECTED_PIN_COLOR     = "selected.pin.color";
   /** Property key name for allowed pin's color. */
   public final static String     ALLOWED_PIN_COLOR      = "allowed.pin.color";
   /** Property key name for forbidden pin's color. */
   public final static String     FORBIDDEN_PIN_COLOR    = "forbidden.pin.color";
   /** Array of all default values. */
   public final static String[]   uiProperties           = { REPLAY_DELAY,
         BACKGROUND_COLOR, WHITE_FIELD_COLOR, WHITE_FIELD_EDGE_COLOR,
         BLACK_FIELD_COLOR, BLACK_FIELD_EDGE_COLOR, LABEL_COLOR, HINT_COLOR,
         SELECTED_FIELD_COLOR, ALLOWED_FIELD_COLOR, ATTACK_FIELD_COLOR,
         GUARDED_FIELD_COLOR, FORBIDDEN_FIELD_COLOR, ATTACKER_FIELD_COLOR,
         TOOLTIP_TEXT_COLOR, TOOLTIP_COLOR, TOOLTIP_EDGE_COLOR,
         PIN_BRIGHT_COLOR, PIN_DARK_COLOR, PIN_GHOST_BRIGHT_COLOR,
         PIN_GHOST_DARK_COLOR, SELECTED_PIN_COLOR, ALLOWED_PIN_COLOR,
         FORBIDDEN_PIN_COLOR                            };

   private String                 resourceFile;
   private Properties             properties;
   private HashMap<String, Color> colors;
   private Set<String>            noColor                = new HashSet<String>();

   /**
    * Default constructor. Tries to load de.rowlo.tridchess.config/ChessUI.properties.
    */
   public ChessUi() {
      this( "de/rowlo/tridchess/config/ChessUI.properties" );
   }

   /**
    * Constructs a ChessUi for a specific resource file.
    * 
    * @param resourceFile
    *           name of the resource file located in config folder.
    */
   public ChessUi( String resourceFile ) {
      noColor.add( REPLAY_DELAY );
      initialize( resourceFile );
   }

   /**
    * Loads UI properties from a given resource file. Old properties will be
    * discarded.
    * 
    * @param resourceFile
    *           name of the resource file located in config folder.
    */
   protected void initialize( String resourceFile ) {
      this.resourceFile = resourceFile;
      URL url = ClassLoader.getSystemResource( resourceFile );
      if ( url == null ) {
         File f = new File( resourceFile );
         try {
            url = f.toURI().toURL();
         }
         catch ( MalformedURLException e ) {
            System.err.println( "ERROR: Cannot create URL from URI: "
                  + f.toURI() );
            url = null;
         }
      }

      if ( properties == null ) {
         properties = new Properties();
      }
      else {
         properties.clear();
      }

      try {
         properties.load( url.openStream() );
      }
      catch ( FileNotFoundException e ) {
         System.out
               .println( "No chess UI configuration found. Using defaults." );
         setupDefaults();
      }
      catch ( IOException e ) {
         System.out
               .println( "No chess UI configuration found. Using defaults." );
         setupDefaults();
      }
      catch ( NullPointerException e ) {
         System.out
               .println( "No chess UI configuration found. Using defaults." );
         setupDefaults();
      }

      if ( colors == null ) {
         colors = new HashMap<String, Color>();
      }
      else {
         colors.clear();
      }
      createColors();
   }

   /**
    * @return the Color of a property or null, if property name unknown
    */
   public Color getUiProperty( String property ) {
      if ( !Arrays.asList( uiProperties ).contains( property ) ) return null;
      return colors.get( property );
   }

   /**
    * @return the String value of a property or null, if property name unknown
    */
   public String getUiPropertyString( String property ) {
      if ( !Arrays.asList( uiProperties ).contains( property ) ) return null;
      return properties.getProperty( property );
   }

   /**
    * Sets up default colors if no properties files have been found.
    */
   protected void setupDefaults() {
      properties.put( REPLAY_DELAY, "500" );
      properties.put( BACKGROUND_COLOR, "  0,  0,  0,172" );
      properties.put( WHITE_FIELD_COLOR, "223,223,223, 96" );
      properties.put( WHITE_FIELD_EDGE_COLOR, "255,255,255,180" );
      properties.put( BLACK_FIELD_COLOR, "  0, 86,223, 96" );
      properties.put( BLACK_FIELD_EDGE_COLOR, "  0,118,255,180" );
      properties.put( LABEL_COLOR, "255,255,255,255" );
      properties.put( HINT_COLOR, "255,255,128,128" );
      properties.put( SELECTED_FIELD_COLOR, "255,255,255,128" );
      properties.put( ALLOWED_FIELD_COLOR, " 22,227, 95,128" );
      properties.put( ATTACK_FIELD_COLOR, "255,230, 40,128" );
      properties.put( GUARDED_FIELD_COLOR, "255,115, 40,128" );
      properties.put( FORBIDDEN_FIELD_COLOR, "242, 24, 71,128" );
      properties.put( ATTACKER_FIELD_COLOR, "242, 24, 71,180" );
      properties.put( TOOLTIP_TEXT_COLOR, "  0,  0,  0,255" );
      properties.put( TOOLTIP_COLOR, "255,241,123,255" );
      properties.put( TOOLTIP_EDGE_COLOR, "215,158, 45,255" );
      properties.put( PIN_BRIGHT_COLOR, "232,232,232,255" );
      properties.put( PIN_DARK_COLOR, "105,105,105,255" );
      properties.put( PIN_GHOST_BRIGHT_COLOR, "232,232,232, 32" );
      properties.put( PIN_GHOST_DARK_COLOR, "105,105,105, 32" );
      properties.put( SELECTED_PIN_COLOR, "255,255,255,128" );
      properties.put( ALLOWED_PIN_COLOR, " 22,227, 95,128" );
      properties.put( FORBIDDEN_PIN_COLOR, "242, 24, 71, 64" );
   }

   /**
    * Creates a Color mapping for the properties keys.
    */
   protected void createColors() {
      int count = uiProperties.length;
      for ( int i = 0; i < count; i++ ) {
         // ignore non-color properties
         if ( noColor.contains( uiProperties[i] ) ) continue;
         colors.put( uiProperties[i], toColor( (String) properties
               .get( uiProperties[i] ) ) );
      }
   }

   /**
    * Creates a Color object from an rgba representation with the format
    * "r,g,b,a".
    */
   protected static Color toColor( String rgba ) {
      int r = 0;
      int g = 0;
      int b = 0;
      int a = 0;
      String[] sInts = rgba.replaceAll( " ", "" ).split( "," );
      if ( sInts.length >= 3 ) {
         try {
            r = Integer.valueOf( sInts[0] );
            g = Integer.valueOf( sInts[1] );
            b = Integer.valueOf( sInts[2] );
            if ( sInts.length == 4 ) a = Integer.valueOf( sInts[3] );
         }
         catch ( NumberFormatException e ) {
            System.err.println( "Invalid color specification: " + rgba );
            r = g = b = a = 0;
         }
      }
      return new Color( r, g, b, a );
   }

   /**
    * @return the resourceFile
    */
   public String getResourceFile() {
      return resourceFile;
   }

   /**
    * @param resourceFile
    *           the resourceFile to set
    */
   public void setResourceFile( String resourceFile ) {
      initialize( resourceFile );
   }
}
