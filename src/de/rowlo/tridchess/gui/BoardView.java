/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.gui;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;

import de.rowlo.tridchess.model.Field;

/**
 * Paints a chess board and it's fields.
 * 
 * @author Robert Wloch
 */
public class BoardView extends JComponent {
   private static final long serialVersionUID = 5917562444447286531L;

   private RepaintRunnable            repainter;
   private String[]                   colNames;
   private String[]                   rowNames;
   private ChessUi                    cui;
   private int                        fieldWidth  = 35;
   private int                        fieldHeight = 35;
   private Map<String, BufferedImage> images      = new HashMap<String, BufferedImage>();

   private Field[][]                  gridFields;
   private String                     hintText    = "";

   /**
    * Creates the Board's view.
    * 
    * @param colNames
    *           array of column names.
    * @param rowNames
    *           array of row names.
    */
   public BoardView( String[] colNames, String[] rowNames ) {
      super();
      this.repainter = new RepaintRunnable( this );
      this.colNames = colNames;
      this.rowNames = rowNames;
   }

   /**
    * @param name
    *           a Figure's name
    * @return the BufferedImage of the Figure.
    */
   public BufferedImage getImage( String name ) {
      return images.get( name );
   }

   /**
    * @return gridFields
    */
   public Field[][] getGridFields() {
      return gridFields;
   }

   /**
    * @param gridFields
    *           Field[][]
    */
   public void setGridFields( Field[][] gridFields ) {
      this.gridFields = gridFields;
   }

   /**
    * @return hintText
    */
   public String getHintText() {
      return hintText;
   }

   /**
    * @param hintText
    *           Festzulegender hintText
    */
   public void setHintText( String hintText ) {
      this.hintText = hintText;
   }

   /**
    * @return fieldHeight
    */
   public int getFieldHeight() {
      return fieldHeight;
   }

   /**
    * @param fieldHeight
    */
   public void setFieldHeight( int fieldHeight ) {
      this.fieldHeight = fieldHeight;
   }

   /**
    * @return fieldWidth
    */
   public int getFieldWidth() {
      return fieldWidth;
   }

   /**
    * @param fieldWidth
    */
   public void setFieldWidth( int fieldWidth ) {
      this.fieldWidth = fieldWidth;
   }

   /**
    * @return colNames String[]
    */
   public String[] getColNames() {
      return colNames;
   }

   /**
    * @return rowNames String[]
    */
   public String[] getRowNames() {
      return rowNames;
   }

   /**
    * @return ChessUi
    */
   public ChessUi getChessUi() {
      if ( cui == null ) setChessUi( new ChessUi() );
      return cui;
   }

   /**
    * Sets the ChessUi to be used.
    * 
    * @param cui
    *           ChessUi
    */
   public void setChessUi( ChessUi cui ) {
      this.cui = cui;
   }

   /**
    * Addes an image for a figure to the image map.
    * 
    * @param key
    * @param value
    */
   public void addFigureImage( String key, BufferedImage value ) {
      images.put( key, value );
   }

   /**
    * Invokes a Runnable to repaint the board at the end of the current event
    * thread's run.
    */
   public void redraw() {
      SwingUtilities.invokeLater( repainter );
   }

   @Override
   public void paint( Graphics g ) {
      Graphics2D g2 = (Graphics2D) g;

      // paint background
      g2.setPaint( cui.getUiProperty( ChessUi.BACKGROUND_COLOR ) );
      g2.fillRect( 0, 0, getWidth(), getHeight() );

      // paint labels
      g2.setPaint( cui.getUiProperty( ChessUi.LABEL_COLOR ) );
      Font fnt = new Font( "Arial", Font.PLAIN, 12 );
      g2.setFont( fnt );
      FontMetrics fm = getFontMetrics( fnt );
      int hs = fm.getHeight() / 2;
      int maxX = colNames.length;
      int maxY = rowNames.length;
      // col labels
      int offsetX = 20 + fieldWidth / 2;
      for ( int x = 0; x < maxX; x++ ) {
         String s = colNames[x];
         int ls = fm.stringWidth( s ) / 2;
         g2.drawString( s, offsetX - ls + fieldWidth * x, 10 + hs );
         g2.drawString( s, offsetX - ls + fieldWidth * x, 30 + hs + maxY
               * fieldHeight );
      }
      // row labels
      int offsetY = 20 + fieldHeight / 2 + hs;
      for ( int y = 0; y < maxY; y++ ) {
         String s = rowNames[y];
         int ls = fm.stringWidth( s ) / 2;
         g2.drawString( s, 10 - ls, offsetY + fieldHeight * y );
         g2.drawString( s, 30 - ls + maxX * fieldWidth, offsetY + fieldHeight
               * y );
      }

      // paint all chess fields
      if ( gridFields == null || gridFields.length < 1 ) return;
      maxX = gridFields.length;
      maxY = gridFields[0].length;
      for ( int y = 0; y < maxY; y++ ) {
         for ( int x = 0; x < maxX; x++ ) {
            Field f = gridFields[x][y];
            if ( f == null ) continue; // grid position is empty
            f.getView().paint( g2 );
         }
      }

   }

   class RepaintRunnable implements Runnable {
      BoardView subject = null;

      /**
       * Creates a repaint thread for a view.
       * 
       * @param v
       *           the view to be repainted.
       */
      public RepaintRunnable( BoardView v ) {
         subject = v;
      }

      public void run() {
         subject.repaint();
      }
   }
}
