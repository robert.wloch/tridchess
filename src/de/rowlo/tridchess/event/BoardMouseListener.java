/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.event;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import de.rowlo.tridchess.model.Board;

/**
 * MouseListener and MouseMotionListener for a chess Board. Calls the Board's
 * mouse dragging methods.
 * 
 * @author Robert Wloch
 * 
 */
public class BoardMouseListener implements MouseMotionListener, MouseListener {
   /**
    * The chess Board to be called back.
    */
   protected Board board;

   /**
    * Creates the mouse listener.
    * 
    * @param b
    *           the Board to be called back.
    */
   public BoardMouseListener( Board b ) {
      board = b;
      board.setDragging( false );
   }

   public void mouseMoved( MouseEvent e ) {
      Propagator p = new Propagator( Propagator.MOVE, board, e );
      p.start();
   }

   public void mouseDragged( MouseEvent e ) {
      if ( board.isDragging() ) {
         Propagator p = new Propagator( Propagator.DRAG, board, e );
         p.start();
      }
      else {
         Propagator p = new Propagator( Propagator.DRAG_START, board, e );
         p.start();
      }
   }

   public void mouseReleased( MouseEvent e ) {
      if ( board.isDragging() ) {
         Propagator p = new Propagator( Propagator.DRAG_END, board, e );
         p.start();
      }
   }

   public void mouseClicked( MouseEvent e ) {
   }

   public void mouseEntered( MouseEvent e ) {
   }

   public void mouseExited( MouseEvent e ) {
   }

   public void mousePressed( MouseEvent e ) {
   }

   class Propagator extends Thread {
      /** Mouse move event. */
      public static final int MOVE       = 1;
      /** Mouse drag start event. */
      public static final int DRAG_START = 2;
      /** Mouse drag event. */
      public static final int DRAG       = 3;
      /** Mouse drag end event. */
      public static final int DRAG_END   = 4;
      private MouseEvent      e;
      private Board           b;
      private int             t;

      /**
       * Creates a thread that will forward the event to specific Board's
       * methods.
       * 
       * @param type
       *           mouse event type
       * @param b
       *           the Board
       * @param e
       *           the MouseEvent
       */
      public Propagator( int type, Board b, MouseEvent e ) {
         this.t = type;
         this.b = b;
         this.e = e;
      }

      @Override
      public void run() {
         switch ( t ) {
            case MOVE:
               b.processMouseMotion( e );
               break;
            case DRAG_START:
               b.setDragging( true );
               b.processMouseDragStart( e );
               break;
            case DRAG:
               b.processMouseDrag( e );
               break;
            case DRAG_END:
               b.setDragging( false );
               b.processMouseDragEnd( e );
               break;
            default:
         }
      }
   }
}
