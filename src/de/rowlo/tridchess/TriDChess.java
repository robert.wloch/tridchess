package de.rowlo.tridchess;
/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;

import de.rowlo.tridchess.model.trid.Game;

/**
 * <p>
 * Tri-D chess is a game allowing two players to play on a special type of chess
 * board: the Tri-D board. Originally presented in the early Star Trek (TM)
 * series the board consists of 3 4-by-4 levels and 4 smaller 2-by-2 levels.
 * </p>
 * <p>
 * This computer version provides a bird view perspective with all boards being
 * aside of each other. On the real board the board parts are located half way
 * above each other. Hence this PC game provides a flat bird view perspective
 * with fields lighten up in different colors indicating possible moves,
 * threads, and attacks.
 * </p>
 * 
 * @author Robert Wloch
 * 
 */
public class TriDChess {

   /**
    * Creates a Tri-D chess board.
    */
   public TriDChess() {
      final JFrame frame;
      final Game g = new Game();
      // create main frame
      frame = new JFrame();
      frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

      frame.getContentPane().removeAll();
      frame.getContentPane().setLayout( new GridBagLayout() );
      GridBagConstraints gc = new GridBagConstraints();
      gc.fill = GridBagConstraints.BOTH;
      gc.gridx = 0;
      gc.gridy = 0;
      gc.weightx = 1.0;
      gc.weighty = 1.0;
      frame.getContentPane().add( g.getBoard().getLogJComponent(), gc );
      gc.fill = GridBagConstraints.VERTICAL;
      gc.gridx = 1;
      gc.weightx = 0.0;
      frame.getContentPane().add( g.getBoard().getView(), gc );
      gc.fill = GridBagConstraints.VERTICAL;
      gc.gridx = 2;
      frame.getContentPane().add( g.getBoard().getAttackedFiguresView(), gc );
      g.getBoard().generalInit();

      // show main frame
      frame.pack();
      // center frame
      int w = frame.getWidth();
      int h = frame.getHeight();
      int x = GraphicsEnvironment.getLocalGraphicsEnvironment()
            .getDefaultScreenDevice().getDisplayMode().getWidth() / 2;
      int y = GraphicsEnvironment.getLocalGraphicsEnvironment()
            .getDefaultScreenDevice().getDisplayMode().getHeight() / 2;
      x = x - w / 2;
      if ( x < 0 ) x = 0;
      y = y - h / 2;
      if ( y < 0 ) y = 0;
      frame.setBounds( x, y, w, h );
      frame.setVisible( true );

      frame.addKeyListener( new KeyAdapter() {
         @Override
         public void keyPressed( KeyEvent e ) {
            switch ( e.getKeyCode() ) {
               case KeyEvent.VK_F1:
                  g.getBoard().displayHelpMessageBox( frame );
                  e.consume();
                  break;
               case KeyEvent.VK_F2:
                  if ( g.isRunning() ) {
                     g.saveFormation();
                  }
                  e.consume();
                  break;
               case KeyEvent.VK_F3:
                  g.loadFormation();
                  e.consume();
                  break;
               case KeyEvent.VK_F4:
                  if ( g.isRunning() ) {
                     g.saveGame();
                  }
                  e.consume();
                  break;
               case KeyEvent.VK_F5:
                  g.loadGame();
                  e.consume();
                  break;
               case KeyEvent.VK_F6:
                  g.startGame();
                  e.consume();
                  break;
               case KeyEvent.VK_F7:
                  g.toggleFreeMoveMode();
                  e.consume();
                  break;
               default:
            }
         }
      } );

      g.getBoard().displayHelpMessageBox( frame );
   }

   /**
    * Main method creating the game.
    * 
    * @param args
    *           not used so far.
    */
   public static void main( String[] args ) {
      new TriDChess();
   }
}
