/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.util;

import java.util.ArrayList;

import de.rowlo.tridchess.model.Field;

/**
 * Figure and Field list specific utilities.
 * 
 * @author Robert Wloch
 * 
 */
public class ArrayUtils {

   /**
    * Returns a new ArrayList containing only these elements that are part of
    * list and add. Starts at end and stops on first mismatch.
    * 
    * @param list
    * @param add
    * @return never null
    */
   public static ArrayList<Field> joinMatch( ArrayList<Field> list,
         ArrayList<Field> add ) {
      ArrayList<Field> a = new ArrayList<Field>();
      if ( list == null || add == null ) return a;
      int count = list.size();
      for ( int i = count - 1; i >= 0; i-- ) {
         Field f = list.get( i );
         if ( add.contains( f ) ) a.add( f );
      }
      return ArrayUtils.reverse( a );
   }

   /**
    * Returns a new ArrayList containing only these elements that are part of
    * list and and not part of add.
    * 
    * @param list
    * @param add
    * @return never null
    */
   public static ArrayList<Field> joinExclude( ArrayList<Field> list,
         ArrayList<Field> add ) {
      ArrayList<Field> a = new ArrayList<Field>();
      if ( list == null || add == null ) return a;
      int count = list.size();
      for ( int i = 0; i < count; i++ ) {
         Field f = list.get( i );
         if ( !add.contains( f ) ) a.add( f );
      }
      return a;
   }

   /**
    * Returns a new ArrayList containing all elements that are part of list and
    * add once.
    * 
    * @param list
    * @param add
    * @return never null
    */
   public static ArrayList<Field> joinUnique( ArrayList<Field> list,
         ArrayList<Field> add ) {
      ArrayList<Field> a = list;
      if ( list == null || add == null ) return a;
      int count = add.size();
      for ( int i = 0; i < count; i++ ) {
         Field f = add.get( i );
         if ( !a.contains( f ) ) a.add( f );
      }
      return a;
   }

   /**
    * Returns a new ArrayList containing every element of list exactly once.
    * 
    * @param list
    * @return never null
    */
   public static ArrayList<Field> removeDuplicates( ArrayList<Field> list ) {
      ArrayList<Field> a = new ArrayList<Field>();
      if ( list == null ) return a;
      int count = list.size();
      for ( int i = 0; i < count; i++ ) {
         Field f = list.get( i );
         if ( !a.contains( f ) ) a.add( f );
      }
      return a;
   }

   /**
    * Returns a new ArrayList containing the same elements as list.
    * 
    * @param list
    * @return never null
    */
   @SuppressWarnings({ "unchecked", "rawtypes" })
   public static ArrayList copy( ArrayList list ) {
      ArrayList a = new ArrayList();
      if ( list == null ) return a;
      int count = list.size();
      for ( int i = 0; i < count; i++ ) {
         a.add( list.get( i ) );
      }
      return a;
   }

   /**
    * Returns a new ArrayList containing the same elements as list in reverse
    * order.
    * 
    * @param list
    * @return never null
    */
   public static ArrayList<Field> reverse( ArrayList<Field> list ) {
      ArrayList<Field> a = new ArrayList<Field>();
      if ( list == null ) return a;
      int count = list.size();
      for ( int i = count - 1; i >= 0; i-- ) {
         a.add( list.get( i ) );
      }
      return a;
   }
}
