/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.util;

/**
 * Special String functions.
 * 
 * @author Robert Wloch
 * 
 */
public class StringUtils {
   /**
    * Takes the shorter of s1 and s2 as pattern matching the beginning of the
    * longer one. Cuts of the matching start of the longer String and returns
    * the rest. E.g., be s1="abcdefoobar" and s2="abcde" or vice versa, the
    * result will be "foobar".
    * 
    * @param s1
    *           String, not null
    * @param s2
    *           String, not null
    * @return the longest String without matching shorter String at the
    *         beginning, or null if at least one argument was null
    */
   public static String skipMatch( String s1, String s2 ) {
      if ( s1 == null || s2 == null ) return null;
      String shorter = ( s1.length() <= s2.length() ) ? s1 : s2;
      String longer = ( s1.length() > s2.length() ) ? s1 : s2;
      if ( longer.startsWith( shorter ) ) { return longer.substring( shorter
            .length() ); }
      return longer;
   }
}
