/*
 * Tri-D Chess application for playing real 3D chess.
 * 
 * Copyright (C) 2007-2021 Robert Wloch
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 */
package de.rowlo.tridchess.util;

/**
 * Board specific utilities.
 * 
 * @author Robert Wloch
 * 
 */
public class BoardUtils {

   /**
    * Returns an int[3] array.
    * 
    * @param x
    * @param y
    * @param z
    * @return [x,y,z]
    */
   public static int[] make3DPointArray( int x, int y, int z ) {
      int[] p3d = new int[3];
      p3d[0] = x;
      p3d[1] = y;
      p3d[2] = z;
      return p3d;
   }

   /**
    * 
    * @param a
    * @return [a[0]:a[1]..:a[n]]
    */
   public static String arrayToString( int[] a ) {
      StringBuffer sb = new StringBuffer();
      int count = a.length;
      sb = sb.append( '[' );
      for ( int i = 0; i < count; i++ ) {
         sb = sb.append( a[i] );
         if ( i < count - 1 ) sb = sb.append( ':' );
      }
      sb = sb.append( ']' );
      return sb.toString();
   }

   /**
    * Returns an int[3] array.
    * 
    * @param x
    * @param y
    * @param z
    * @return [x,y,z]
    */
   public static String make3DString( int x, int y, int z ) {
      int[] p3d = new int[3];
      p3d[0] = x;
      p3d[1] = y;
      p3d[2] = z;
      return arrayToString( p3d );
   }

}
