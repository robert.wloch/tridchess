# tridchess
## Latest Release
Built on 2021-08-29: [tridchess.jar](./archive/tridchess.jar "Tri-D Chess JAR file")

## About tridchess
Tri-D chess is a game allowing two players to play on a special type of chess
board: the Tri-D board. Originally presented in the early Star Trek (TM)
series the board consists of 3 4-by-4 levels and 4 smaller 2-by-2 levels.

This computer version provides a bird view perspective with all boards being
aside of each other. On the real board the board parts are located half way
above each other. Hence this PC game provides a flat bird view perspective
with fields lighten up in different colors indicating possible moves,
threads, and attacks.

<img style="float: right;" src="src/help/tridboard-overviews-bright.png" width="410" height="615" 
	 align="left" vspace="2" hspace="2" alt="Tri-D Chess Board Perspectives" />

## Tri-D Chess Board Help
### Essential Keys
*   `[`<span style="color: red">F1</span>`]`: That Help Section
*   `[`<span style="color: red">F2</span>`]`: Save Formation
*   `[`<span style="color: red">F3</span>`]`: Load Formation
*   `[`<span style="color: red">F4</span>`]`: Save Game
*   `[`<span style="color: red">F5</span>`]`: Load Game
*   `[`<span style="color: red">F6</span>`]`: Start/Restart Game
*   `[`<span style="color: red">F7</span>`]`: Toggle Free Move Mode

### Quickstart Guide
Start or restart a game with [<font color="red">F6</font>]. Use drag and drop to
move figures and pins. Naturally, white party has the first move.

The 4 small attack boards can only be moved if there is no more
than one pawn on it. Moving an attack board counts as a regular
move. While an attack board is moved it may be turned by 180
degrees. To do so hold [<font color="red">CTRL</font>] down while dropping it onto an
available green pin.

Press [<font color="red">F7</font>] to toggle free movement mode of figures and pins.

### About the Tri-D Chess Board
The displayed board is a stretched out bird view of a 3D board
with 3 fixed levels: White board (*W*), Neutral Board (*N*), and Black
Board (*B*). There are 3 additional levels created by the presence
of the four small attack boards. The image on the right shows the
Tri-D board from two perspectives.

For more information have a look at [www.3dschach.de](http://www.3dschach.de "Link to German 3D chess website") although
their rules differ a bit from mine. Especially attack board movement and sneaking figures
over- or underneath by the opponent is kept as stated in the original rule book to increase
fun and strategic thinking.

## Copyright and License
Tri-D Chess version 2021-08-29, Copyright (C) 2007-2021 Robert Wloch

Tri-D Chess comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions. For details read the [GPL](gpl.txt "GNU General Public License") shipped along with this software.<br>

Many thanks to the GNU chess project, as I use their figure images.