# Copyright and License
Tri-D Chess version 2021-08-29, Copyright (C) 2007-2021 Robert Wloch

Tri-D Chess comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions. For details read the [GPL](gpl.txt "GNU General Public License") shipped along with this software.<br>

Many thanks to the GNU chess project, as I use their figure images.